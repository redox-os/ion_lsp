## Standard of rust code 

There is no CI for ensuring standard of the rust code base and the visual code extension.

However pull requests should adhere to certain standards nonetheless. 
One can run the program `just` to run the command `check_all` to ensure those standards are met.
Before I approve a pull request, I will use this command to verify standards.

## Needed Tools

You need a number of tools to make [bash file][shell_check] work and to contribute to this project.

- Rust tool chain, cargo and the rust compiler. 
You can install those components via the rustup by following this [instruction][rustup]
- [Typos][typos]: Spellchecking tool. Install via 
```sh
cargo install typos-cli
```
- [insta][insta]: Snapshot testing. 
Install via
```sh
cargo install cargo-insta
```
- [just][just]: Used command runner 
```sh
cargo install just
```
- [cargo-llvm-cov][cargo-llvm-cov]: Used check for code coverage
```sh
cargo +stable install cargo-llvm-cov --locked 
```
- [Visual studio code][visual_studio_code]: To run the visual code extension during development.
- [Npm][npm] Node package manager used for the development of visual code extension.j


  
## Testing

This crate heavily relies on snapshot testing. This approach removes the work of creating complicated
expected output by hand. The functionality of the parsing and semantic analyzer is verified by 
asserting their output. This output is a sequence of structs value with a decent number of fields.
It would be hell to specify all these fields by hand.

If a new test is written, you need to verify whether the output is the correct one.
The snapshot testing is accomplished by the crate [insta][insta] and its [CLI tool][insta_cli_tool].

[rustup]: https://rustup.rs/
[visual_studio_code]: https://code.visualstudio.com/
[shell_check]: check.sh
[insta_cli_tool]: https://insta.rs/docs/cli/
[insta]: https://docs.rs/insta/latest/insta/
[typos]: https://github.com/crate-ci/typos
[npm]: https://docs.npmjs.com/about-npm
[cargo-llvm-cov]: https://github.com/taiki-e/cargo-llvm-cov
[just]: https://just.systems/
