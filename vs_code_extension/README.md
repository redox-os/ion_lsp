# Visual Stuio Code extension for ion shell

## Functionality

This extension provides the following features for scripting language of [ion_shell]: 

- Syntax highlighting .
- LSP-Features provided by the language sever for ion shell [List of current features][ion_lsp_readme].
  The LSP server for ion shell has to be installed locally.

## How to use get working

1. Install the LSP server for Ion shell locally. See this [Readme][ion_lsp_readme] for How to.
2. Install this extension. See the next **section** below for how to.
3. Make sure this extension knows where to execute the LSP server. That described further down.

## How to install the visual code extension locally

The extension is not yet published and therefore is to be installed locally by
running the following commands.

```sh
npm install
npm run install_locally
```

## How does this extension start the LSP server

The extension assumes that the language server executable is named "ion_lsp" and is 
within PATH environmental variable aka you could just invoke "ion_lsp" in the terminal.

If it is somewhere else you need to provide the path to LSP server via the settings
of this extension. 
In the settings you need to adjust the value called "ion_lsp_extension.executable_path" then.

## Contributing

See this [document][contributing]

## License 

This project, all workspace members and the visual code extension licensed under [MIT License][LICENSE]

[License]: LICENSE
[ion_lsp_readme]: https://gitlab.redox-os.org/redox-os/ion_lsp/-/blob/main/ion_shell_lsp_server/README.md
[contributing]: CONTRIBUTING.md 
[grammar_file]: grammar_syntax_highlight.json
[ion_shell]: https://gitlab.redox-os.org/redox-os/ion
[lsp_server]: ../ion_lsp
