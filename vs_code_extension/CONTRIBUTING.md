# Specifics about contributing to the visual studio code extension

## Running the Extension during development

- Run `npm install` in this folder. This installs all necessary npm modules in both the client.
- Open VS Code on this folder.
- Press Ctrl+Shift+B to start compiling the client, extension and server.
- Switch to the Run and Debug View in the Sidebar (Ctrl+Shift+D).
- Select `Launch Client` from the drop down (if it is not already).
- Press ▷ to run the launch configuration (F5).

## Syntax Highlighting

This [grammar file][grammar_file] is used to define the Syntax Highlighting. 

## General Contributing Guide lines and set ups

See the following [guide lines][contributing] 

[contributing]: ../CONTRIBUTING.md 
[grammar_file]: grammar_syntax_highlight.json
