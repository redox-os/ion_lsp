import { ExtensionContext, ExtensionMode, workspace } from "vscode";

import {
  Executable,
  LanguageClient,
  LanguageClientOptions,
  ServerOptions,
} from "vscode-languageclient/node";

let client: LanguageClient;

export function activate(context: ExtensionContext) {
  const executable: Executable = getPathToLspServer(context);
  // If the extension is launched in debug mode then the debug server options are used
  //sdf Otherwise the run options are used
  const serverOptions: ServerOptions = {
    run: executable,
    debug: executable,
  };

  // Options to control the language client
  const clientOptions: LanguageClientOptions = {
    // Register the server for plain text documents
    documentSelector: [{ scheme: "file", language: "ion" }],
  };

  // Create the language client and start the client.
  client = new LanguageClient(
    "Ion shell language extension",
    "Ion shell language client",
    serverOptions,
    clientOptions,
  );

  // Start the client. This will also launch the server
  client.start();
}

function getPathToLspServer(context: ExtensionContext): Executable {
  if (isIsInProduction(context)) {
    const config = workspace.getConfiguration();

    const key = "ion_lsp_extension.executable_path";
    const pathToLspServer = config.get<string>(key);

    if (pathToLspServer === null) {
      throw new Error(
        `Configuration value for path to Ion LSP Server is not set: (${key})`,
      );
    } else {
      return { command: pathToLspServer };
    }
  } else {
    const pathToLspServer = process.env.__ION_LSP_SERVER_DEBUG;
    return { command: pathToLspServer };
  }
}

function isIsInProduction(context: ExtensionContext): boolean {
  const mode: ExtensionMode = context.extensionMode;
  return mode === ExtensionMode.Production;
}

export function deactivate(): Thenable<void> | undefined {
  if (!client) {
    return undefined;
  }
  return client.stop();
}
