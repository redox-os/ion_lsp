# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

## [0.2.0] - 2024-04-07 

### Added 

- Support for the following LSP features
  - Diagnostics: Shows errors in the syntax of a ion shell script. Semantic errors like referencing 
    a not defined variable are not supported right now.
  - Go to definition: 
    - Variable Reference: Jumps from a reference to a variable to its declaration in the same file.
    - Function Reference: Jumps invocation to its declaration in the same file.
  - Find References:
    - Variable Reference: List all references to and 
      redeclaration of the variable name within current and 
      all children scopes within the same file
    - Function Reference: List all invocation of the declared function
  - Document Symbols: Shows all declared variables and functions within a script
  - Hover information: Shows the following declaration information 
    on hovering over a function call or variable reference. 
    - Type of declaration (variable of function) 
    - Name of declaration (variable or function name)
    - Line of declaration
    - All comments 
       - without blank lines in between 
       - coming before the declaration 
       - Not being part of another declaration.
    - Inline comment, right from the declaration up to end of line
    - Specific to functions 
      - Arguments with optional type annotation.
      - Doc strings if declaration is for a function
    - Specific to variables
      - Type annotation if variable name has a type annotation.
      
[unreleased]: https://gitlab.redox-os.org/redox-os/ion_lsp/-/commits/main?ref_type=heads
[0.2.0]: https://gitlab.redox-os.org/redox-os/ion_lsp/-/tags/0.2.0v

