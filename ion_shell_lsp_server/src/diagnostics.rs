use std::borrow::Cow;

use ion_shell_parser::{
    parsing::{language_items::HasWarnings, HasLocation},
    semantic_analysis::ValidationError,
};
use lsp_types::Diagnostic;

use crate::{LspRange, ParsedIonShellFile, ToConvert};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DiagnosticModel {
    Pull,
    Push,
}

pub fn produce_diagnostics(parsed: &ParsedIonShellFile) -> Vec<Diagnostic> {
    let line_columns = parsed.line_columns();
    let content = parsed.text();
    let mut to_diagnostics: Vec<Cow<ValidationError>> = Vec::with_capacity(parsed.items().len());
    for next in parsed.items() {
        match next {
            Ok(might_have_warnings) => {
                might_have_warnings.all_warnings(&mut |warning| {
                    let validation_error = ValidationError::Syntax(warning.clone());
                    to_diagnostics.push(Cow::Owned(validation_error));
                });
            }
            Err(error) => to_diagnostics.push(Cow::Borrowed(error)),
        }
    }
    to_diagnostics
        .into_iter()
        .map(|error| {
            let location = error.location();
            let (start, end) = (
                line_columns
                    .get_line_column(content, location.start_pos())
                    .unwrap(),
                line_columns
                    .get_line_column(content, location.end_pos())
                    .unwrap(),
            );

            let range: LspRange = ToConvert((start, end)).into();
            let message = error.message(content).into_owned();
            Diagnostic {
                range,
                message,
                severity: Some(ToConvert(error.severity()).into()),
                ..Default::default()
            }
        })
        .collect()
}
