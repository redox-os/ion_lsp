use std::{collections::HashMap, usize};

use ion_shell_parser::{
    parsing::{
        language_items::{FunctionDeclaration, OwnedFoundVariableDeclaration},
        HasLocation,
    },
    Range,
};
use once_cell::sync::Lazy;

use crate::{
    finding_symbols::{self, FoundDefinitionKind},
    ion_shell_file_parse_state::CommentEntry,
    LspPosition, ParsedIonShellFile,
};

const MARKUP_SEPARATOR: &str = "---";
const MARKUP_NEWLINE: &str = "\n\n";
static MARKUP_SEPARATOR_LINE: Lazy<Box<str>> =
    Lazy::new(|| format!("{0}{1}{0}", MARKUP_NEWLINE, MARKUP_SEPARATOR).into_boxed_str());

pub fn hover_information(parsed: &ParsedIonShellFile, wanted: LspPosition) -> Option<String> {
    fn added_comment(
        comments: &HashMap<usize, CommentEntry>,
        indexes: &mut Vec<Range>,
        line: u32,
        is_first: bool,
    ) -> bool {
        if let Some(comment) = comments.get(&(line as usize)) {
            if !is_first && comment.is_inline() {
                false
            } else {
                indexes.push(comment.inner());
                true
            }
        } else {
            false
        }
    }

    fn loop_until_gab_in_comments(line: u32, parsed: &ParsedIonShellFile) -> Vec<Range> {
        let comments = parsed.comments();
        let mut indexes: Vec<Range> = Vec::default();
        let _ = added_comment(comments, &mut indexes, line, true);
        let mut current_line = line;

        while let (new, false) = current_line.overflowing_sub(1) {
            current_line = new;
            if !added_comment(comments, &mut indexes, current_line, false) {
                return indexes;
            }
        }

        indexes
    }

    fn func_dec_to_string(
        name: &str,
        line: u32,
        func: &FunctionDeclaration,
        parsed: &ParsedIonShellFile,
    ) -> String {
        let mut output = format!(
            "Function: ({}) defined at line ({})",
            name,
            line.saturating_add(1)
        );

        let has_docstr = if let Some(docstring) = func.docstring() {
            let inner = docstring.value().inner().location();
            let text = ion_shell_parser::get_string_from_input(parsed.text(), inner);
            output += &format!("{0} \\-\\-{1}{0}", *MARKUP_SEPARATOR_LINE, text);
            true
        } else {
            false
        };

        let has_args = !func.arguments().is_empty();
        if has_args && !has_docstr {
            output += &MARKUP_SEPARATOR_LINE;
        }

        for next_param in func.arguments() {
            let text = ion_shell_parser::get_string_from_input(
                parsed.text(),
                next_param.name().location(),
            );
            let type_string = next_param
                .known_type()
                .map(|some_type| format!(" `{}`", some_type.kind()))
                .unwrap_or_default();

            output += &format!("{}- {}{}", MARKUP_NEWLINE, text, type_string);
        }

        output
    }

    fn var_dec_to_string(var: &OwnedFoundVariableDeclaration, name: &str, line: u32) -> String {
        let type_string = var
            .try_get_type()
            .map(|some_type| format!(" `{}`", some_type))
            .unwrap_or_default();
        format!(
            "Variable: ({}){} defined at line ({})",
            name,
            type_string,
            line.saturating_add(1)
        )
    }

    let position = finding_symbols::go_to_definition(parsed, wanted)?;

    let name = position.item().name(parsed.text());
    let location = position.position();
    let line = location.start.line;
    let indexes = loop_until_gab_in_comments(line, parsed);

    let indexes = {
        let mut indexes: Vec<&str> = indexes
            .into_iter()
            .map(|e| ion_shell_parser::get_string_from_input(parsed.text(), e))
            .collect();
        indexes.reverse();
        indexes
    };

    let output = match position.item() {
        FoundDefinitionKind::Function(func) => func_dec_to_string(name, line, func, parsed),
        FoundDefinitionKind::VariableDeclaration(var) => var_dec_to_string(var, name, line),
    };

    if indexes.is_empty() {
        Some(output)
    } else {
        let output_comment = indexes.join(MARKUP_NEWLINE);
        Some(output + &MARKUP_SEPARATOR_LINE + &output_comment)
    }
}

#[cfg(test)]
mod testing {
    use super::*;

    #[test]
    fn hover_information() {
        fn assert_case(parsed: &ParsedIonShellFile, case_name: &str, position: LspPosition) {
            let actual = super::hover_information(parsed, position).unwrap();
            insta::assert_display_snapshot!(case_name, actual);
        }
        const INPUT: &str = include_str!("test_input_files/hover.txt");
        let parsed = crate::parse_whole_file(INPUT);
        assert_case(
            &parsed,
            "var_without_type",
            lsp_types::Position {
                line: 7,
                character: 12,
            },
        );
        assert_case(
            &parsed,
            "var_with_type",
            lsp_types::Position {
                line: 10,
                character: 12,
            },
        );
        assert_case(
            &parsed,
            "left_variable_with_int_annotation",
            lsp_types::Position {
                line: 8,
                character: 8,
            },
        );
        assert_case(
            &parsed,
            "right_variable_without_annotation",
            lsp_types::Position {
                line: 9,
                character: 9,
            },
        );
        assert_case(
            &parsed,
            "with_two_comments",
            lsp_types::Position {
                line: 11,
                character: 10,
            },
        );
        assert_case(
            &parsed,
            "with_one_inline_comment",
            lsp_types::Position {
                line: 12,
                character: 10,
            },
        );
        assert_case(
            &parsed,
            "func_with_args",
            lsp_types::Position {
                line: 19,
                character: 0,
            },
        );
        assert_case(
            &parsed,
            "without_any_args",
            lsp_types::Position {
                line: 24,
                character: 5,
            },
        )
    }
}
