use std::path::{Path, PathBuf};

use anyhow::anyhow;
use flexi_logger::{FileSpec, FlexiLoggerError, Logger, LoggerHandle, WriteMode};

use crate::{AppResult, CliArgs};

pub fn set_up_logger(cli: &CliArgs) -> AppResult<LoggerHandle> {
    fn set_up(path: &Path) -> Result<LoggerHandle, FlexiLoggerError> {
        let specs = FileSpec::default().directory(path).suppress_timestamp();
        Logger::try_with_str("info")?
            .log_to_file(specs)
            .write_mode(WriteMode::BufferAndFlush)
            .start()
    }

    let handler = if let Some(path) = cli.log_path() {
        set_up(path)
    } else if cfg!(debug_assertions) {
        set_up(&PathBuf::from(env!("CARGO_MANIFEST_DIR")))
    } else if let Some(path) = dirs::data_dir() {
        let app_folder = path.join(env!("CARGO_PKG_NAME"));
        std::fs::create_dir_all(&app_folder)?;
        set_up(&app_folder)
    } else if let Some(path) = dirs::template_dir() {
        set_up(&path)
    } else {
        Err(anyhow!("No directory found to log messages !"))?
    }?;

    Ok(handler)
}
