use ion_shell_parser::{
    parsing::{
        language_items::{FunctionDeclaration, OwnedFoundVariableDeclaration},
        HasLocation,
    },
    Range,
};

use crate::LspRange;

#[derive(Debug)]
pub struct FoundDefinition {
    position: LspRange,
    item: FoundDefinitionKind,
}

impl HasLocation for FoundDefinition {
    fn location(&self) -> ion_shell_parser::prelude::Range {
        match self.item() {
            FoundDefinitionKind::Function(ctx) => ctx.location(),
            FoundDefinitionKind::VariableDeclaration(ctx) => ctx.location(),
        }
    }
}

impl FoundDefinition {
    pub fn new(position: LspRange, item: FoundDefinitionKind) -> Self {
        Self { position, item }
    }

    pub fn position(&self) -> LspRange {
        self.position
    }

    pub fn item(&self) -> &FoundDefinitionKind {
        &self.item
    }
}

#[derive(Debug, Clone)]
pub enum FoundDefinitionKind {
    Function(FunctionDeclaration),
    VariableDeclaration(OwnedFoundVariableDeclaration),
}

impl FoundDefinitionKind {
    pub fn name<'a>(&self, input: &'a str) -> &'a str {
        let var_name = self.name_location();
        ion_shell_parser::get_string_from_input(input, var_name)
    }

    pub fn name_location(&self) -> Range {
        match self {
            FoundDefinitionKind::Function(func) => func.name().location(),
            FoundDefinitionKind::VariableDeclaration(var) => var.var_name().location(),
        }
    }
}
