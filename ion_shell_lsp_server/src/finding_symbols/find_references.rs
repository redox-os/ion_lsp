use std::collections::HashSet;

use ion_shell_parser::parsing::{
    language_items::{
        item_scopes::EnterScope, HasCommandInvocation, HasVariableDeclaration,
        HasVariableReference, LanguageItem, LanguageItemContext,
    },
    HasLocation, LocalScope,
};

use crate::{LspPosition, LspRange, ParsedIonShellFile};

use super::{filter_errors_out, FoundDefinition, FoundDefinitionKind};

pub fn find_references(
    parsed: &ParsedIonShellFile,
    wanted: LspPosition,
    include_declaration: bool,
) -> Option<Vec<LspRange>> {
    fn add_to_vec_if_same_name(
        parsed: &ParsedIonShellFile,
        var: &impl HasLocation,
        name: &str,
        found: &mut Vec<LspRange>,
    ) {
        let range = var.location();
        let text = ion_shell_parser::get_string_from_input(parsed.text(), range);
        if text == name {
            let range: LspRange = parsed.byte_range_to_lsp_range(range).unwrap();
            found.push(range);
        }
    }

    let found_definition = super::go_to_definition(parsed, wanted)?;

    let input = parsed.text();
    let needle = found_definition.position();

    let (anker, all_after) =
        find_all_after(parsed, parsed.lsp_position_to_byte(needle.start).unwrap())?;
    let scoped = filter_out_upper_scopes(parsed, anker, all_after, &found_definition);

    let mut found: Vec<LspRange> = Vec::default();
    let name = found_definition.item().name(input);

    if include_declaration {
        add_to_vec_if_same_name(
            parsed,
            &found_definition.item().name_location(),
            name,
            &mut found,
        );
    }
    match found_definition.item() {
        FoundDefinitionKind::Function(_) => {
            for next in scoped {
                next.command_invocation(&mut |invocation| {
                    add_to_vec_if_same_name(parsed, &invocation, name, &mut found);
                });
                if let LanguageItem::Enter(EnterScope::Function(func)) = next.item() {
                    add_to_vec_if_same_name(parsed, func.name(), name, &mut found);
                }
            }
        }
        FoundDefinitionKind::VariableDeclaration(_) => {
            for next in scoped {
                next.var_declaration(&mut |var| {
                    add_to_vec_if_same_name(parsed, &var, name, &mut found);
                });
                next.var_references(&mut |var| {
                    add_to_vec_if_same_name(parsed, var, name, &mut found);
                });
            }
        }
    }
    Some(found)
}

fn filter_out_upper_scopes<'a>(
    parsed: &ParsedIonShellFile,
    anker: &LanguageItemContext,
    to_filter: impl Iterator<Item = &'a LanguageItemContext>,
    found_definition: &FoundDefinition,
) -> impl Iterator<Item = &'a LanguageItemContext> {
    let scopes = parsed.scopes();
    let mut path: HashSet<LocalScope> = HashSet::new();

    let from_func = matches!(found_definition.item(), FoundDefinitionKind::Function(_));
    let anker = anker.scope();
    let anker = if from_func {
        scopes
            .get(&anker)
            .map(|inner| inner.parent().unwrap_or(anker))
            .unwrap_or(anker)
    } else {
        anker
    };
    path.insert(anker);
    let mut next_children: Vec<LocalScope> = vec![anker];

    while let Some(next) = next_children.pop() {
        path.insert(next);
        let relation = scopes.get(&next).unwrap();
        for child in relation.children() {
            if !path.contains(child) {
                next_children.push(*child);
            }
        }
    }
    to_filter.filter(move |next| path.contains(&next.scope()))
}

fn find_all_after(
    parsed: &ParsedIonShellFile,
    needle: usize,
) -> Option<(
    &LanguageItemContext,
    impl Iterator<Item = &LanguageItemContext>,
)> {
    let mut source = filter_errors_out(parsed);
    while let Some(element) = source.next() {
        if element.location().is_within(needle) {
            return Some((element, source));
        }
    }

    None
}

#[cfg(test)]
mod testing {
    use super::*;

    #[test]
    fn find_references() {
        fn assert_case(snapshot_name: &str, pos: LspPosition, parsed: &ParsedIonShellFile) {
            let actual: Vec<(LspRange, &str)> = super::find_references(parsed, pos, true)
                .map(|seq| {
                    seq.into_iter()
                        .map(|element| (element, parsed.lsp_range_to_text(element).unwrap()))
                        .collect()
                })
                .unwrap();

            // 8 => 3
            insta::assert_debug_snapshot!(snapshot_name, actual);
        }

        const INPUT: &str = include_str!("find_references.txt");
        let parsed = crate::parse_whole_file(INPUT);
        assert_case(
            "find_references_function_called_once",
            LspPosition::new(6, 1),
            &parsed,
        );
        assert_case(
            "find_references_function_in_command_expression",
            LspPosition::new(11, 10),
            &parsed,
        );
        assert_case(
            "find_references_function_on_name_parameter",
            LspPosition::new(1, 14),
            &parsed,
        );
    }
}
