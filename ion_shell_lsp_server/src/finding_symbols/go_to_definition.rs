use std::collections::HashSet;

use ion_shell_parser::{
    parsing::{
        language_items::{
            item_scopes::EnterScope, variables::VariableReference, FunctionDeclaration,
            HasCommandInvocation, HasVariableDeclaration, HasVariableReference, LanguageItem,
            LanguageItemContext, OwnedFoundVariableDeclaration,
        },
        HasLocation, LocalScope,
    },
    Range,
};

use crate::{
    finding_symbols::FoundDefinitionKind, LspPosition, LspRange, ParsedIonShellFile, ToConvert,
};

use super::{filter_errors_out, FoundDefinition};

pub fn go_to_definition(
    parsed: &ParsedIonShellFile,
    wanted: LspPosition,
) -> Option<FoundDefinition> {
    fn might_find_declaration_up_to_root<'a>(
        parsed: &'a ParsedIonShellFile,
        found: &LanguageItemContext,
        rest: impl Iterator<Item = &'a LanguageItemContext>,
        reference: &'a VariableReference,
    ) -> Option<FoundDefinition> {
        let mut filtered = filter_out_all_non_parent_scopes(parsed, found, rest);
        let wanted_name = {
            let to_str = reference.name();
            ion_shell_parser::get_string_from_input(parsed.text(), to_str.location()).to_string()
        };
        filtered.find_map(|element| {
            let mut found = None;
            element.var_declaration(&mut |declaration| {
                if found.is_none() {
                    let found_name = declaration.var_name();
                    let location = found_name.location();
                    let found_name =
                        ion_shell_parser::get_string_from_input(parsed.text(), location);
                    if wanted_name == found_name {
                        let lsp_range: LspRange =
                            ToConvert((parsed.line_columns(), parsed.text(), location)).into();
                        found = Some(FoundDefinition::new(
                            lsp_range,
                            FoundDefinitionKind::VariableDeclaration(declaration.into()),
                        ));
                    }
                }
            });
            found
        })
    }

    fn return_declaration<T>(
        parsed: &ParsedIonShellFile,
        declaration: T,
        on_found_definition_kind: impl Fn(T) -> FoundDefinitionKind,
    ) -> FoundDefinition
    where
        T: HasLocation,
    {
        let lsp_range: LspRange =
            ToConvert((parsed.line_columns(), parsed.text(), declaration.location())).into();
        FoundDefinition::new(lsp_range, on_found_definition_kind(declaration))
    }

    let needle = parsed
        .line_columns()
        .get_byte(parsed.text(), ToConvert(wanted).into())
        .unwrap();
    let (found, rest) = find_all_before_item(parsed, needle)?;

    match determine_go_to_definition_from(found, needle) {
        GoToDefinitionFrom::None => None,
        GoToDefinitionFrom::VariableDeclaration(declaration) => {
            Some(return_declaration(parsed, declaration, |d| {
                FoundDefinitionKind::VariableDeclaration(d)
            }))
        }
        GoToDefinitionFrom::FunctionDeclaration(declaration) => {
            Some(return_declaration(parsed, declaration, |d| {
                FoundDefinitionKind::Function(d)
            }))
        }
        GoToDefinitionFrom::MightBeFunctionName(function) => {
            let mut filtered = filter_out_unreachable_funcs(parsed, found, rest);
            let wanted_name = { ion_shell_parser::get_string_from_input(parsed.text(), function) };
            filtered.find_map(|next| {
                if let LanguageItem::Enter(EnterScope::Function(declaration)) = next.item() {
                    let function_name_loc = declaration.name().location();
                    let function_name =
                        ion_shell_parser::get_string_from_input(parsed.text(), function_name_loc);
                    if function_name == wanted_name {
                        let lsp_range: LspRange =
                            ToConvert((parsed.line_columns(), parsed.text(), function_name_loc))
                                .into();
                        let declaration = FoundDefinition::new(
                            lsp_range,
                            FoundDefinitionKind::Function(declaration.clone()),
                        );
                        Some(declaration)
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
        }
        GoToDefinitionFrom::VariableReference(reference) => {
            might_find_declaration_up_to_root(parsed, found, rest, &reference)
        }
    }
}

pub fn find_all_before_item(
    parsed: &ParsedIonShellFile,
    needle: usize,
) -> Option<(
    &LanguageItemContext,
    impl Iterator<Item = &LanguageItemContext>,
)> {
    let mut source = filter_errors_out(parsed).rev();
    while let Some(element) = source.next() {
        if element.location().is_within(needle) {
            return Some((element, source));
        }
    }

    None
}

fn filter_out_unreachable_funcs<'a>(
    parsed: &ParsedIonShellFile,
    anker: &LanguageItemContext,
    to_filter: impl Iterator<Item = &'a LanguageItemContext>,
) -> impl Iterator<Item = &'a LanguageItemContext> {
    let scopes = parsed.scopes();
    let mut path: HashSet<LocalScope> = HashSet::new();

    let mut anker = Some(anker.scope());
    while let Some(next) = anker {
        path.insert(next);
        let next_node = scopes.get(&next).unwrap();
        anker = next_node.parent();
        for to_insert in next_node.children() {
            path.insert(*to_insert);
        }
    }
    to_filter.filter(move |next| path.contains(&next.scope()))
}

fn filter_out_all_non_parent_scopes<'a>(
    parsed: &ParsedIonShellFile,
    anker: &LanguageItemContext,
    to_filter: impl Iterator<Item = &'a LanguageItemContext>,
) -> impl Iterator<Item = &'a LanguageItemContext> {
    let scopes = parsed.scopes();
    let mut path: HashSet<LocalScope> = HashSet::new();

    let mut anker = anker.scope();
    path.insert(anker);
    while let Some(next) = scopes
        .get(&anker)
        .expect(
            "Anker comes from scope map as a key or as the start item.\n\
            It must be within the scope map",
        )
        .parent()
    {
        path.insert(next);
        anker = next;
    }
    to_filter.filter(move |next| path.contains(&next.scope()))
}

fn determine_go_to_definition_from(
    item: &LanguageItemContext,
    wanted: usize,
) -> GoToDefinitionFrom {
    let mut found_as_reference = None;

    if let LanguageItem::Enter(EnterScope::Function(func)) = item.item() {
        if func.name().location().is_within(wanted) {
            return GoToDefinitionFrom::FunctionDeclaration(func.to_owned());
        }
    }

    item.command_invocation(&mut |invocation| {
        if found_as_reference.is_none() && invocation.is_within(wanted) {
            found_as_reference = Some(GoToDefinitionFrom::MightBeFunctionName(invocation));
        }
    });

    if let Some(found) = found_as_reference {
        return found;
    }

    item.var_references(&mut |var| {
        if found_as_reference.is_none() && var.location().is_within(wanted) {
            found_as_reference = Some(GoToDefinitionFrom::VariableReference(var.clone()));
        }
    });

    if let Some(found) = found_as_reference {
        return found;
    }

    item.var_declaration(&mut |var| {
        if found_as_reference.is_none() && var.location().is_within(wanted) {
            found_as_reference = Some(GoToDefinitionFrom::VariableDeclaration(var.into()));
        }
    });

    if let Some(found) = found_as_reference {
        return found;
    }

    GoToDefinitionFrom::None
}

#[derive(Debug)]
enum GoToDefinitionFrom {
    None,
    VariableReference(VariableReference),
    VariableDeclaration(OwnedFoundVariableDeclaration),
    FunctionDeclaration(FunctionDeclaration),
    MightBeFunctionName(Range),
}
