pub use find_references::find_references;
pub use found_definition::{FoundDefinition, FoundDefinitionKind};

mod find_references;
mod found_definition;
mod go_to_definition;

pub use go_to_definition::go_to_definition;
use ion_shell_parser::parsing::language_items::LanguageItemContext;

use crate::ParsedIonShellFile;

pub fn filter_errors_out(
    parsed: &ParsedIonShellFile,
) -> impl DoubleEndedIterator<Item = &LanguageItemContext> {
    parsed.items().iter().filter_map(|e| e.as_ref().ok())
}

#[cfg(test)]
mod testing {

    use ion_shell_parser::parsing::{language_items::LanguageItemContext, HasLocation};
    use testing::go_to_definition::find_all_before_item;

    use crate::{LspPosition, LspRange, ToConvert};

    use super::*;

    type LanguageItemInTest<'a> = (LspRange, String, &'a LanguageItemContext);

    fn expected_range(
        start_line: u32,
        start_character: u32,
        end_line: u32,
        end_character: u32,
    ) -> LspRange {
        LspRange::new(
            LspPosition::new(start_line, start_character),
            LspPosition::new(end_line, end_character),
        )
    }

    fn prepare_item_for_test_review<'a>(
        parsed: &ParsedIonShellFile,
        item: &'a LanguageItemContext,
        input: &str,
    ) -> LanguageItemInTest<'a> {
        let location = item.location();
        let lsp_location = parsed
            .line_columns()
            .range_line_column(parsed.text(), location)
            .unwrap();
        let lsp_range: LspRange = ToConvert(lsp_location).into();
        let text = ion_shell_parser::get_string_from_input(input, item.location()).to_owned();
        (lsp_range, text, item)
    }

    #[test]
    fn get_some_on_go_to_definition() {
        fn assert_case(pos: LspPosition, expected: LspRange, input: &str) {
            let parsed = crate::parse_whole_file(input);

            let actual = super::go_to_definition(&parsed, pos).unwrap();

            assert_eq!(expected, actual.position());
        }
        assert_case(
            LspPosition::new(1, 6),
            expected_range(0, 4, 0, 6),
            "let aa = 2\necho $aa",
        );
        assert_case(
            LspPosition::new(5, 8),
            expected_range(0, 4, 0, 5),
            include_str!("test_input_files/go_to_def_if.txt"),
        );
        assert_case(
            LspPosition::new(5, 1),
            expected_range(0, 3, 0, 9),
            include_str!("test_input_files/go_to_func.txt"),
        );
    }

    #[test]
    fn all_before_item() {
        const INPUT: &str = "let a = 2\nlet b = $sep(2)\nlet c = '2'\necho $b";
        let parsed = crate::parse_whole_file(INPUT);
        let pos = LspPosition::new(2, 4);
        let pos = parsed
            .line_columns()
            .get_byte(INPUT, ToConvert(pos).into())
            .unwrap();

        let actual: (LanguageItemInTest, Vec<LanguageItemInTest>) =
            find_all_before_item(&parsed, pos)
                .map(|(found, rest)| {
                    let found = prepare_item_for_test_review(&parsed, found, INPUT);
                    (
                        found,
                        rest.map(|element| prepare_item_for_test_review(&parsed, element, INPUT))
                            .collect(),
                    )
                })
                .unwrap();
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn is_within_range() {
        fn assert_case(needle: LspPosition, hay: LspRange, expected: bool) {
            let actual = crate::is_within_range(hay, needle);
            assert_eq!(expected, actual, "needle: {:#?}, hay: {:#?}", needle, hay);
        }
        assert_case(
            LspPosition::new(1, 2),
            LspRange::new(LspPosition::new(1, 2), LspPosition::new(1, 2)),
            true,
        );
        assert_case(
            LspPosition::new(2, 2),
            LspRange::new(LspPosition::new(1, 2), LspPosition::new(1, 2)),
            false,
        );
        assert_case(
            LspPosition::new(0, 1),
            LspRange::new(LspPosition::new(0, 0), LspPosition::new(0, 5)),
            true,
        );
    }
}
