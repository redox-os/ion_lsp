use clap::Parser;
use log::{error, info};
use lsp_server::Connection;

use ion_shell_lsp_server::{logging, server_state::ServerState, AppResult, CliArgs};

fn main() -> AppResult {
    fn start_main_loop() -> AppResult {
        // Run the server and wait for the two threads to end (typically by trigger LSP Exit event).
        let (connection, io_threads) = Connection::stdio();
        let after_loop = ServerState::new(connection).and_then(|(mut server_state, connection)| {
            ion_shell_lsp_server::main_loop(connection, &mut server_state)
        });
        let joined = io_threads.join();
        after_loop?;
        joined?;
        Ok(())
    }

    let args = CliArgs::parse();

    let _keep_logge_alive = logging::set_up_logger(&args);
    info!("Starting lsp server for ion shell");

    ion_shell_lsp_server::panic_hook::set_app_panic_hook();
    if let Err(error) = start_main_loop() {
        error!("Error: {:#?}", error);
    }

    info!("shutting down server");
    Ok(())
}
