use std::collections::HashMap;

use ion_shell_parser::ImmutableClonableText;
use log::info;
use lsp_server::Connection;
use lsp_types::{
    ClientCapabilities, Diagnostic, DocumentDiagnosticReport, InitializeParams,
    TextDocumentClientCapabilities,
};

use crate::{
    diagnostics::{self, DiagnosticModel},
    server_capabilities, AppResult, IonShellFileParseState, LspFileIdentifier, ParsedIonShellFile,
    ToConvert,
};

#[derive(Debug)]
pub struct ServerState {
    text: HashMap<LspFileIdentifier, IonShellFileParseState>,
    init_parmas: InitializeParams,
    diagnostic_model: DiagnosticModel,
}

impl ServerState {
    pub fn new(connection: Connection) -> AppResult<(Self, Connection)> {
        let server_capabilities = server_capabilities::build_server_capabilities();

        let init_parmas: InitializeParams = {
            let it = connection.initialize(server_capabilities)?;
            serde_json::from_value(it)
                .expect("Client capability should be parseable from lsp_server crate.")
        };

        let diagnostic_model = if let Some(TextDocumentClientCapabilities {
            diagnostic: Some(_),
            ..
        }) = init_parmas.capabilities.text_document
        {
            info!("Client is capable of pulling diagnostics. Lsp server will compute diagnostics on demand of client.");
            DiagnosticModel::Pull
        } else {
            info!("Client is not capable of pulling diagnostics. Lsp server will compute diagnostics whenever a file is saved.");
            DiagnosticModel::Push
        };
        let new_self = Self {
            text: Default::default(),
            init_parmas,
            diagnostic_model,
        };
        Ok((new_self, connection))
    }

    pub fn get_file(&mut self, input: LspFileIdentifier) -> Option<IonShellFileParseState> {
        self.text.get(&input).cloned()
    }
    pub fn remove_file(&mut self, input: LspFileIdentifier) {
        info!("File with id {} was removed", input);
        _ = self.text.remove(&input);
    }

    pub fn insert_or_update_file_parsed(
        &mut self,
        input: LspFileIdentifier,
        content: ParsedIonShellFile,
    ) {
        info!("File with id {} was parsed", input);
        let content = IonShellFileParseState::Parsed(content);
        _ = self.text.insert(input, content);
    }

    pub fn insert_or_update_file_unparsed(
        &mut self,
        input: LspFileIdentifier,
        content: ImmutableClonableText,
    ) {
        info!("File with id {} was updated", input);
        let content = IonShellFileParseState::NotParsed(content);
        _ = self.text.insert(input, content);
    }

    pub fn init_parmas(&self) -> &InitializeParams {
        &self.init_parmas
    }
    pub fn client_caps(&self) -> &ClientCapabilities {
        &self.init_parmas.capabilities
    }

    pub fn diagnostic_model(&self) -> DiagnosticModel {
        self.diagnostic_model
    }

    pub fn parse_file(&mut self, ident: LspFileIdentifier) -> Option<ParsedIonShellFile> {
        let content = self.get_file(ident.clone())?;
        match content {
            IonShellFileParseState::Parsed(content) => Some(content.clone()),
            IonShellFileParseState::NotParsed(content) => {
                let to_insert = crate::parse_whole_file(content);
                self.insert_or_update_file_parsed(ident, to_insert.clone());

                Some(to_insert)
            }
        }
    }

    pub fn parse_file_and_diagnostics(
        &mut self,
        ident: LspFileIdentifier,
    ) -> Option<DocumentDiagnosticReport> {
        let parsed = self.parse_file(ident)?;
        Some(ToConvert(diagnostics::produce_diagnostics(&parsed)).into())
    }
    pub fn parse_file_and_diagnostics_push(
        &mut self,
        ident: LspFileIdentifier,
    ) -> Option<Vec<Diagnostic>> {
        let parsed = self.parse_file(ident)?;
        Some(diagnostics::produce_diagnostics(&parsed))
    }
}
