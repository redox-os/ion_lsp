use std::path::PathBuf;

use clap::Parser;
#[derive(Parser, Debug)]
pub struct CliArgs {
    #[arg(short, long, value_parser = validate_is_valid_file_path, env = "ION_SHELL_LSP_LOG_PATH")]
    /// Where the lsp server logs its events
    log_path: Option<PathBuf>,
}

impl CliArgs {
    pub fn log_path(&self) -> Option<&PathBuf> {
        self.log_path.as_ref()
    }
}

fn validate_is_valid_file_path(input: &str) -> Result<PathBuf, String> {
    let path = PathBuf::from(input);
    match path.parent() {
        Some(should_be_dir) if should_be_dir.is_dir() => (),
        None => (),
        Some(is_not_a_dir) => {
            return Err(format!(
                "Parent of log file is `{:?}` is not a directory",
                is_not_a_dir
            ))
        }
    }

    if path.is_dir() {
        return Err(format!(
            "Path for log file at `{:?}` is taken by a directory",
            path
        ));
    }

    Ok(path)
}
