use log::warn;
use lsp_server::{RequestId, Response, ResponseError};
use lsp_types::{
    request::{
        DocumentDiagnosticRequest, DocumentSymbolRequest, GotoDeclaration, GotoDeclarationParams,
        GotoDefinition, HoverRequest, References, Request,
    },
    DocumentDiagnosticParams, DocumentSymbolParams, GotoDefinitionParams, Hover, HoverContents,
    HoverParams, Location, MarkedString, Position, ReferenceParams, TextDocumentPositionParams,
};

use crate::{
    error_codes, finding_symbols, hover, server_state::ServerState, to_convert::ToConvert,
    AppError, LspFileIdentifier,
};

fn null_success_response(id: RequestId) -> lsp_server::Response {
    lsp_server::Response {
        id,
        result: Some(serde_json::Value::Null),
        error: None,
    }
}

/// # Errors
///
/// If constructing a response to request fails. This can happen because of
///
/// * A value could not be turned into a JSON value.
/// * Parameters from a request could not be extracted.
///
/// : An error response to a request is a Ok(_) return value.
///
pub fn try_return_response_to_request(
    state: &mut ServerState,
    req: lsp_server::Request,
) -> Result<Response, (AppError, RequestId)> {
    fn handle_go_to(
        state: &mut ServerState,
        req_id: RequestId,
        position: TextDocumentPositionParams,
    ) -> Result<Response, (AppError, RequestId)> {
        let (ident, pos): (LspFileIdentifier, Position) =
            (ToConvert(position.text_document).into(), position.position);
        let parsed = state.parse_file(ident.clone()).ok_or_else(|| {
            (
                anyhow::anyhow!("No file found under id {}", ident),
                req_id.clone(),
            )
        })?;
        match finding_symbols::go_to_definition(&parsed, pos) {
            Some(found_range) => {
                let found_range = found_range.position();
                let location = Location::new(ident.as_ref().clone(), found_range);
                let location = serde_json::to_value(location)
                    .map_err(|error| (error.into(), req_id.clone()))?;
                let response = ToConvert((req_id, location)).into();
                Ok(response)
            }
            None => Ok(null_success_response(req_id)),
        }
    }

    fn create_no_found_file_response(req_id: RequestId, ident: LspFileIdentifier) -> Response {
        let error: ResponseError = ToConvert(format!(
            "Content of file with id {} is not provided yet",
            ident
        ))
        .into();
        ToConvert((req_id, error)).into()
    }

    match req.method.as_str() {
        HoverRequest::METHOD => {
            let (req_id, params): (RequestId, HoverParams) =
                req.extract(HoverRequest::METHOD).unwrap();
            let position = params.text_document_position_params.position;
            let ident: LspFileIdentifier = params
                .text_document_position_params
                .text_document
                .uri
                .into();
            match state.parse_file(ident.clone()) {
                None => Ok(create_no_found_file_response(req_id, ident)),
                Some(parsed) => {
                    let result = hover::hover_information(&parsed, position).map(|content| Hover {
                        contents: HoverContents::Scalar(MarkedString::String(content)),
                        range: None,
                    });

                    let value = serde_json::to_value(result).unwrap();
                    let response = ToConvert((req_id, value)).into();
                    Ok(response)
                }
            }
        }
        References::METHOD => {
            let (req_id, params): (RequestId, ReferenceParams) =
                req.extract(References::METHOD).unwrap();
            let text_document_position = params.text_document_position;
            let ident: LspFileIdentifier = text_document_position.text_document.uri.into();

            match state.parse_file(ident.clone()) {
                None => Ok(create_no_found_file_response(req_id, ident)),
                Some(parsed) => {
                    let wanted = text_document_position.position;
                    let include_declaration = params.context.include_declaration;
                    let references: Option<Vec<Location>> =
                        finding_symbols::find_references(&parsed, wanted, include_declaration).map(
                            |without_file| {
                                without_file
                                    .into_iter()
                                    .map(|range| Location {
                                        uri: ident.as_ref().clone(),
                                        range,
                                    })
                                    .collect()
                            },
                        );

                    let json_value = serde_json::to_value(references).expect(
                        "references is Option vector of lsp locations \
                        which are convertible to a json value",
                    );
                    let response = ToConvert((req_id, json_value)).into();
                    Ok(response)
                }
            }
        }
        DocumentSymbolRequest::METHOD => {
            let (req_id, params): (RequestId, DocumentSymbolParams) =
                req.extract(DocumentSymbolRequest::METHOD).unwrap();

            let ident: LspFileIdentifier = params.text_document.uri.into();
            match state.parse_file(ident.clone()) {
                None => Ok(create_no_found_file_response(req_id, ident)),
                Some(parsed) => {
                    let document_symbols = parsed.document_symbols(ident);
                    let document_symbols = serde_json::to_value(document_symbols).unwrap();
                    let response = ToConvert((req_id, document_symbols)).into();
                    Ok(response)
                }
            }
        }
        GotoDeclaration::METHOD => {
            let (req_id, params): (RequestId, GotoDeclarationParams) =
                req.extract(GotoDeclaration::METHOD).unwrap();
            let position = params.text_document_position_params;

            handle_go_to(state, req_id, position)
        }
        GotoDefinition::METHOD => {
            let (req_id, params): (RequestId, GotoDefinitionParams) =
                req.extract(GotoDefinition::METHOD).unwrap();
            let position = params.text_document_position_params;

            handle_go_to(state, req_id, position)
        }
        DocumentDiagnosticRequest::METHOD => {
            let (req_id, params): (RequestId, DocumentDiagnosticParams) =
                req.extract(DocumentDiagnosticRequest::METHOD).unwrap();
            let doc = params.text_document;

            let ident: LspFileIdentifier = doc.uri.into();
            match state.parse_file_and_diagnostics(ident.clone()) {
                None => Ok(create_no_found_file_response(req_id, ident)),
                Some(diagnostics) => {
                    let response = match serde_json::to_value(diagnostics) {
                        Ok(success) => Ok(ToConvert((req_id, success)).into()),
                        Err(error) => Err((error.into(), req_id)),
                    }?;
                    Ok(response)
                }
            }
        }
        not_found_method => {
            let error: ResponseError = ToConvert((
                format!(
                    "The requests with the method: ({}) are not supported",
                    not_found_method
                ),
                error_codes::METHOD_NOT_FOUND,
            ))
            .into();

            warn!(
                "Request with unknown method ({}) was received: {:?}",
                not_found_method, &req
            );

            let req_id = req.id;
            let error_response = ToConvert((req_id, error)).into();
            Ok(error_response)
        }
    }
}

#[cfg(test)]
mod testing {

    use crate::test_utils::{self, client_notification, client_requests, TestLspMemoryFixture};

    const EXAMPLE_FILE: &str = "example.ion";

    #[test]
    fn respond_with_method_not_found() {
        let mut fixture = test_utils::act_lsp_init();

        fixture.send_as_client(
            client_requests::will_no_support_type_hierarchy_subtypes_used_as_unknown_method(
                "Not supported".to_string(),
            ),
        );
        fixture.resv_as_client();

        test_utils::act_lsp_shutdown(&mut fixture);

        insta::assert_debug_snapshot!(fixture.history());
    }
    #[test]
    fn respond_to_request_pull_diagnostics_with_errors() {
        const CONTENT: &str = "let 2 = aa\n\
            let name = \"hello world\"\n\
            let";

        let actual = open_one_document_then_pull_diagnostics(EXAMPLE_FILE, CONTENT);

        insta::assert_debug_snapshot!(actual.history());
    }

    #[test]
    fn respond_to_request_pull_diagnostics_without_errors() {
        const CONTENT: &str = "let a = aa\n\
            let name = \"hello world\"\n\
            echo $name";

        let actual = open_one_document_then_pull_diagnostics(EXAMPLE_FILE, CONTENT);

        insta::assert_debug_snapshot!(actual.history());
    }

    #[test]
    fn respond_to_request_pull_diagnostics_with_changed_file_after_open() {
        const INITIAL_CONTENT: &str = "let 2 = aa\n\
            let name = \"hello world\"\n\
            echo $name";
        // One error more
        const CHANGED_CONTENT: &str = "let 2 = aa\n\
            let name = \n\
            echo $name";
        let request_id = "Example".to_string();

        let mut fixture = init_fixture_to_open_file(EXAMPLE_FILE, INITIAL_CONTENT);

        // After opened file
        fixture.send_as_client(client_requests::pull_diagnostics(
            request_id.clone(),
            EXAMPLE_FILE,
        ));
        fixture.resv_as_client();

        fixture.send_as_client(client_notification::text_doc_changed(
            EXAMPLE_FILE,
            CHANGED_CONTENT,
        ));

        // After changed file
        fixture.send_as_client(client_requests::pull_diagnostics(request_id, EXAMPLE_FILE));
        fixture.resv_as_client();

        test_utils::act_lsp_shutdown(&mut fixture);
        insta::assert_debug_snapshot!(fixture.history());
    }

    fn init_fixture_to_open_file(file: &str, content: &str) -> TestLspMemoryFixture {
        let mut fixture = test_utils::act_lsp_init();

        fixture.send_as_client(client_notification::text_doc_open(file, content));

        fixture
    }

    fn open_one_document_then_pull_diagnostics(file: &str, content: &str) -> TestLspMemoryFixture {
        let mut fixture = test_utils::act_lsp_init();
        let request_id = "Example".to_string();

        fixture.send_as_client(client_notification::text_doc_open(file, content));
        fixture.send_as_client(client_requests::pull_diagnostics(request_id, file));
        fixture.resv_as_client();
        test_utils::act_lsp_shutdown(&mut fixture);
        fixture
    }
}
