//! The crate [lsp_types] does provide all the error codes for LSP Protocol.
//! Here the missing error codes are defined.
//!
//! [Source][error_codes_lsp] for this error codes
//!
//! It seems that all codes defined by the json-rpc are not included by the crate [lsp_types]
//!
//! [lsp_types]: https://docs.rs/lsp-types/0.95.0/lsp_types/index.html
//! [error_codes_lsp]: https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#errorCodes

/// If a request with unknown method to this LSP server was send from the client.
/// This should normally not happen if the client respects the server capabilities
/// and this server declares its capabilities correctly.
pub const METHOD_NOT_FOUND: i32 = -32601;
pub const INTERNAL_ERROR: i32 = -32603;
