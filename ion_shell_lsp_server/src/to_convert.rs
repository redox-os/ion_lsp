const FILE_SCHEME: &str = "file:///";

use ion_shell_parser::{LineColumn, LineColumnMap, ProblemSeverity, Range};
use lsp_server::{RequestId, Response, ResponseError};
use lsp_types::{
    error_codes, Diagnostic, DiagnosticSeverity, DocumentDiagnosticReport,
    FullDocumentDiagnosticReport, RelatedFullDocumentDiagnosticReport, SymbolInformation,
    TextDocumentIdentifier, Url,
};

use crate::{
    document_symbols::LocalDocumentSymbols, LspFileIdentifier, LspJsonValue, LspPosition, LspRange,
};

/// # Purpose
///
/// Provides a wrapper type which allows some conversions between types of external crates.
/// By using this type, one does not need invent new names for conversion functions. The name of
/// those functions would need to contain types names. This is not refactor-friendly.
///
/// ## Note
///
/// It is mainly used to convert types from the crate `ion_parser` to types of the crate
/// `lsp_types`.
pub struct ToConvert<T>(pub T);

impl From<ToConvert<TextDocumentIdentifier>> for LspFileIdentifier {
    fn from(value: ToConvert<TextDocumentIdentifier>) -> Self {
        value.0.uri.into()
    }
}

impl<T> From<ToConvert<T>> for Url
where
    T: AsRef<str>,
{
    fn from(value: ToConvert<T>) -> Self {
        use std::borrow::Cow;
        let str_ref = value.0.as_ref();

        Url::parse(str_ref).unwrap_or_else(|_| {
            let has_file_scheme = str_ref.starts_with(FILE_SCHEME);
            let to_url = if has_file_scheme {
                Cow::Borrowed(str_ref)
            } else {
                Cow::Owned(String::from(FILE_SCHEME) + str_ref)
            };

            Url::parse(&to_url).expect(
                "We ensure the name is prepended with the file prefix and \
            we assume the client will send a valid file names",
            )
        })
    }
}

impl<T> From<ToConvert<T>> for TextDocumentIdentifier
where
    T: AsRef<str>,
{
    fn from(value: ToConvert<T>) -> Self {
        let str_ref = value.0.as_ref();
        let uri = ToConvert(str_ref).into();
        TextDocumentIdentifier { uri }
    }
}

impl From<ToConvert<LineColumn>> for LspPosition {
    fn from(value: ToConvert<LineColumn>) -> Self {
        let value = value.0;
        let (line, character) = (value.line() as u32, value.column() as u32);
        LspPosition { line, character }
    }
}

impl From<ToConvert<LspPosition>> for LineColumn {
    fn from(ToConvert(pos): ToConvert<LspPosition>) -> Self {
        let (line, character) = (pos.line as usize, pos.character as usize);
        LineColumn::new(line, character)
    }
}

impl From<ToConvert<(LineColumn, LineColumn)>> for LspRange {
    fn from(value: ToConvert<(LineColumn, LineColumn)>) -> Self {
        let (start, end) = value.0;
        let (start, end) = (ToConvert(start).into(), ToConvert(end).into());
        LspRange { start, end }
    }
}
impl From<ToConvert<LspRange>> for (LineColumn, LineColumn) {
    fn from(ToConvert(value): ToConvert<LspRange>) -> Self {
        let (start, end) = (value.start, value.end);
        (ToConvert(start).into(), ToConvert(end).into())
    }
}

impl From<ToConvert<(&LineColumnMap, &str, Range)>> for LspRange {
    fn from(ToConvert((map, input, range)): ToConvert<(&LineColumnMap, &str, Range)>) -> Self {
        let (start, end) = map.range_line_column(input, range).unwrap();
        ToConvert((start, end)).into()
    }
}

impl From<ToConvert<(LspPosition, LspPosition)>> for LspRange {
    fn from(value: ToConvert<(LspPosition, LspPosition)>) -> Self {
        let (start, end) = value.0;
        LspRange { start, end }
    }
}

impl From<ToConvert<String>> for ResponseError {
    fn from(value: ToConvert<String>) -> Self {
        let message = value.0;
        // Safe: value of `UNKNOWN_ERROR_CODE` is small enough
        // for safe casting
        ToConvert((message, error_codes::UNKNOWN_ERROR_CODE as i32)).into()
    }
}
impl From<ToConvert<(String, i32)>> for ResponseError {
    fn from(value: ToConvert<(String, i32)>) -> Self {
        let (message, code) = value.0;
        ResponseError {
            code,
            message,
            data: None,
        }
    }
}

impl From<ToConvert<(RequestId, LspJsonValue)>> for Response {
    fn from(value: ToConvert<(RequestId, serde_json::value::Value)>) -> Self {
        let (id, result) = value.0;
        let result = Some(result);
        Response {
            id,
            result,
            error: None,
        }
    }
}

impl From<ToConvert<(RequestId, ResponseError)>> for Response {
    fn from(value: ToConvert<(RequestId, ResponseError)>) -> Self {
        let (id, error) = value.0;
        let error = Some(error);
        Response {
            id,
            result: None,
            error,
        }
    }
}

impl From<ToConvert<ProblemSeverity>> for DiagnosticSeverity {
    fn from(ToConvert(value): ToConvert<ProblemSeverity>) -> Self {
        match value {
            ProblemSeverity::Error => DiagnosticSeverity::ERROR,
            ProblemSeverity::Warning => DiagnosticSeverity::WARNING,
        }
    }
}

impl From<ToConvert<Vec<Diagnostic>>> for DocumentDiagnosticReport {
    fn from(value: ToConvert<Vec<Diagnostic>>) -> Self {
        let items = value.0;
        DocumentDiagnosticReport::Full(RelatedFullDocumentDiagnosticReport {
            related_documents: None,
            full_document_diagnostic_report: FullDocumentDiagnosticReport {
                result_id: None,
                items,
            },
        })
    }
}

impl From<ToConvert<(LocalDocumentSymbols, LspFileIdentifier)>> for SymbolInformation {
    #[allow(deprecated)]
    fn from(
        ToConvert((value, ident)): ToConvert<(LocalDocumentSymbols, LspFileIdentifier)>,
    ) -> Self {
        let (name, kind, range) = value.into();

        let location = lsp_types::Location {
            uri: ident.as_ref().clone(),
            range,
        };
        SymbolInformation {
            name,
            kind,
            tags: None,
            deprecated: None,
            location,
            container_name: None,
        }
    }
}
