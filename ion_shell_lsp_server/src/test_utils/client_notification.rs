use lsp_server::Message;
use lsp_types::{
    notification::{
        DidChangeTextDocument, DidOpenTextDocument, Exit, Initialized, Notification as _,
    },
    DidChangeTextDocumentParams, DidOpenTextDocumentParams, InitializeParams,
    TextDocumentContentChangeEvent, TextDocumentItem, VersionedTextDocumentIdentifier,
};
use serde_json::Value;

use crate::ToConvert;

use super::to_json_value;

fn notification(method: impl ToString, params: Value) -> Message {
    let method = method.to_string();
    Message::Notification(lsp_server::Notification { method, params })
}

pub fn text_doc_open(file_path: impl AsRef<str>, text: impl ToString) -> Message {
    let (uri, text) = (ToConvert(file_path).into(), text.to_string());
    let method = DidOpenTextDocument::METHOD;
    let params = DidOpenTextDocumentParams {
        text_document: TextDocumentItem {
            uri,
            text,
            version: 0,
            language_id: "ion".to_owned(),
        },
    };
    let params = to_json_value(params);
    notification(method, params)
}

pub fn text_doc_changed(file_path: impl AsRef<str>, text: impl ToString) -> Message {
    let (uri, text) = (ToConvert(file_path).into(), text.to_string());
    let method = DidChangeTextDocument::METHOD;
    let params = DidChangeTextDocumentParams {
        text_document: VersionedTextDocumentIdentifier { uri, version: 1 },
        content_changes: vec![TextDocumentContentChangeEvent {
            range: None,
            range_length: None,
            text,
        }],
    };
    let params = to_json_value(params);
    notification(method, params)
}

pub fn init_confirmation_for_server() -> Message {
    let params = to_json_value(InitializeParams::default());
    notification(Initialized::METHOD, params)
}
pub fn exit() -> Message {
    let exit = to_json_value(());
    notification(Exit::METHOD, exit)
}
