use std::thread::JoinHandle;

use lsp_server::{Connection, Message};

use crate::server_state::ServerState;

use super::{ActionLspClient, HistoryLspClient};

/// Used to set up a in memory set up between a client and the ion shell LSP server.
/// In tests we use the client to control the flow of the LSP session.
/// We monitor what the client sends and receives to determine the correctness.
pub struct TestLspMemoryFixture {
    // Needs to be optional so we can join within drop impl
    server_thread: Option<JoinHandle<()>>,
    /// Client which controls the flow of the LSP session
    client_connection: Connection,
    /// This saves everything which will be asserted for correctness.
    history: HistoryLspClient,
}

impl TestLspMemoryFixture {
    pub fn send_as_client(&mut self, msg: Message) {
        self.history.push(ActionLspClient::Send(msg.clone()));
        self.client_connection
            .sender
            .send(msg)
            .unwrap_or_else(|error| {
                panic!(
                    "Message could not be sent from client to the server.\nDetails: {:#?}",
                    error
                )
            })
    }

    pub fn resv_as_client(&mut self) -> Message {
        let received = self
            .client_connection
            .receiver
            .recv()
            .unwrap_or_else(|error| {
                panic!(
                    "Expected to receive something from server.\nDetails: {:#?}",
                    error
                )
            });
        self.history
            .push(ActionLspClient::Received(received.clone()));
        received
    }

    /// Use this to assert whether the LSP session went the way it supposed to.
    pub fn history(&self) -> &[ActionLspClient] {
        &self.history
    }
}

impl Default for TestLspMemoryFixture {
    fn default() -> Self {
        let (server_connection, client_connection) = Connection::memory();
        let history = Default::default();
        let server_thread = std::thread::spawn(move || {
            let (mut context, server_connection) = ServerState::new(server_connection).unwrap();
            crate::main_loop(server_connection, &mut context).unwrap()
        });
        let server_thread = Some(server_thread);
        Self {
            server_thread,
            client_connection,
            history,
        }
    }
}

impl Drop for TestLspMemoryFixture {
    fn drop(&mut self) {
        if let Some(handle) = self.server_thread.take() {
            handle.join().expect("failed to join thread");
        }
    }
}
