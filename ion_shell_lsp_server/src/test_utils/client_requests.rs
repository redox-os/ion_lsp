use lsp_server::{Message, Request, RequestId};
use lsp_types::{
    request::{DocumentDiagnosticRequest, Initialize, Request as _, TypeHierarchySubtypes},
    ClientCapabilities, DiagnosticClientCapabilities, DocumentDiagnosticParams, InitializeParams,
    SymbolKind, TextDocumentClientCapabilities, TypeHierarchyItem, TypeHierarchySupertypesParams,
};
use serde_json::Value;

use crate::ToConvert;

use super::to_json_value;

fn request_message(id: RequestId, method: impl ToString, params: Value) -> Message {
    let method = method.to_string();
    let request = Request { id, method, params };
    Message::Request(request)
}

pub fn will_no_support_type_hierarchy_subtypes_used_as_unknown_method(
    id: impl Into<RequestId>,
) -> Message {
    let method = TypeHierarchySubtypes::METHOD;
    let params = TypeHierarchySupertypesParams {
        item: TypeHierarchyItem {
            name: Default::default(),
            kind: SymbolKind::FILE,
            tags: None,
            detail: None,
            uri: ToConvert("a").into(),
            range: Default::default(),
            selection_range: Default::default(),
            data: None,
        },
        work_done_progress_params: lsp_types::WorkDoneProgressParams::default(),
        partial_result_params: lsp_types::PartialResultParams::default(),
    };
    let params = to_json_value(params);
    let id = id.into();
    request_message(id, method, params)
}

pub fn pull_diagnostics(id: impl Into<RequestId>, file: impl AsRef<str>) -> Message {
    let id = id.into();
    let method = DocumentDiagnosticRequest::METHOD;
    let text_document = ToConvert(file).into();
    let params = DocumentDiagnosticParams {
        text_document,
        identifier: None,
        previous_result_id: None,
        work_done_progress_params: lsp_types::WorkDoneProgressParams::default(),
        partial_result_params: lsp_types::PartialResultParams::default(),
    };
    let params = to_json_value(params);
    request_message(id, method, params)
}

pub fn client_init_request() -> Message {
    let params = {
        let text_document = Some(TextDocumentClientCapabilities {
            diagnostic: Some(DiagnosticClientCapabilities::default()),
            ..Default::default()
        });
        let capabilities = ClientCapabilities {
            text_document,
            ..Default::default()
        };

        serde_json::to_value(InitializeParams {
            capabilities,
            ..Default::default()
        })
        .unwrap()
    };

    request_message("Init".to_string().into(), Initialize::METHOD, params)
}

pub fn shutdown() -> Message {
    request_message(
        "Shutdown".to_string().into(),
        lsp_types::request::Shutdown::METHOD,
        ().into(),
    )
}
