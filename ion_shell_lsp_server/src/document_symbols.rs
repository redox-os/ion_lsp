use ion_shell_parser::parsing::{
    language_items::{item_scopes::EnterScope, HasVariableDeclaration, LanguageItem},
    HasLocation,
};
use lsp_types::SymbolKind;

use crate::{finding_symbols, LspRange, ParsedIonShellFile, ToConvert};

use derive_more::Into;
#[derive(Debug, Into)]
pub struct LocalDocumentSymbols {
    name: String,
    kind: SymbolKind,
    range: LspRange,
}

pub fn get_document_symbols(parsed: &ParsedIonShellFile) -> Vec<LocalDocumentSymbols> {
    let mut symbols = Vec::default();
    finding_symbols::filter_errors_out(parsed).for_each(|without_errors| {
        if let LanguageItem::Enter(EnterScope::Function(func)) = without_errors.item() {
            const KIND: SymbolKind = SymbolKind::FUNCTION;
            let symbol = create_symbol_information(KIND, parsed, func.name());
            symbols.push(symbol);
        }
        without_errors.var_declaration(&mut |var| {
            const KIND: SymbolKind = SymbolKind::VARIABLE;
            let symbol = create_symbol_information(KIND, parsed, &var);
            symbols.push(symbol);
        });
    });
    symbols
}

#[allow(deprecated)]
fn create_symbol_information(
    kind: SymbolKind,
    parsed: &ParsedIonShellFile,
    item: &impl HasLocation,
) -> LocalDocumentSymbols {
    let location = item.location();
    let text = ion_shell_parser::get_string_from_input(parsed.text(), location);
    let range = parsed
        .line_columns()
        .range_line_column(parsed.text(), location)
        .unwrap();
    let range: LspRange = ToConvert(range).into();
    LocalDocumentSymbols {
        name: text.to_string(),
        kind,
        range,
    }
}

#[cfg(test)]
mod testing {

    use super::*;

    #[test]
    fn document_symbol() {
        const INPUT: &str = include_str!("test_input_files/document_symbol.txt");
        let parsed = crate::parse_whole_file(INPUT);
        let actual = get_document_symbols(&parsed);
        insta::assert_debug_snapshot!(actual);
    }
}
