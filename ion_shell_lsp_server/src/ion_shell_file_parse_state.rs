use std::{cell::OnceCell, collections::HashMap};

use ion_shell_parser::{
    parsing::{language_items::LanguageItem, HasLocation, ScopeMap},
    semantic_analysis::SemanticResult,
    LineColumn, LineColumnMap, Range,
};
use lsp_types::SymbolInformation;

use crate::{
    document_symbols, HeapAllocated, LspFileIdentifier, LspPosition, LspRange, ParsedContent,
    ReadOnlyText, ToConvert,
};

#[derive(Debug, Clone)]
pub enum IonShellFileParseState {
    NotParsed(ReadOnlyText),
    Parsed(ParsedIonShellFile),
}

#[derive(Debug, Clone)]
pub struct CommentEntry {
    inner: Range,
    is_inline: bool,
}

impl CommentEntry {
    pub fn new(inner: Range) -> Self {
        Self {
            inner,
            is_inline: false,
        }
    }

    pub fn inner(&self) -> Range {
        self.inner
    }

    pub fn is_inline(&self) -> bool {
        self.is_inline
    }

    pub fn set_to_inline(&mut self) {
        self.is_inline = true;
    }
}

#[derive(Debug, Clone)]
pub struct ParsedIonShellFile {
    text: ReadOnlyText,
    parsed: HeapAllocated<[SemanticResult]>,
    line_columns: HeapAllocated<LineColumnMap>,
    scopes: HeapAllocated<ScopeMap>,
    document_symbols: OnceCell<HeapAllocated<[SymbolInformation]>>,
    comments: OnceCell<HashMap<usize, CommentEntry>>,
}

impl ParsedIonShellFile {
    pub fn new(
        text: ReadOnlyText,
        parsed: ParsedContent,
        line_columns: LineColumnMap,
        scopes: ScopeMap,
    ) -> Self {
        let line_columns = HeapAllocated::new(line_columns);
        let scopes = HeapAllocated::new(scopes);
        Self {
            text,
            parsed,
            line_columns,
            scopes,
            document_symbols: Default::default(),
            comments: Default::default(),
        }
    }

    pub fn lsp_range_to_text(&self, lsp_range: LspRange) -> Option<&str> {
        let start_end: (LineColumn, LineColumn) = ToConvert(lsp_range).into();
        let text = self.text();
        let range = self.line_columns().get_byte_range(text, start_end)?;
        let text = ion_shell_parser::get_string_from_input(text, range);
        Some(text)
    }

    pub fn lsp_position_to_byte(&self, lsp_position: LspPosition) -> Option<usize> {
        let line: LineColumn = ToConvert(lsp_position).into();
        self.line_columns().get_byte(self.text(), line)
    }

    pub fn byte_range_to_lsp_range(&self, range: Range) -> Option<LspRange> {
        let line_column = self.line_columns().range_line_column(self.text(), range)?;
        let lsp_range = ToConvert(line_column).into();
        Some(lsp_range)
    }

    pub fn text(&self) -> &str {
        &self.text
    }

    pub fn items(&self) -> &[SemanticResult] {
        &self.parsed
    }

    pub fn parsed_mut(&mut self) -> &mut HeapAllocated<[SemanticResult]> {
        &mut self.parsed
    }

    pub fn line_columns(&self) -> &LineColumnMap {
        &self.line_columns
    }

    pub fn scopes(&self) -> &ScopeMap {
        &self.scopes
    }

    pub fn document_symbols(&self, key: LspFileIdentifier) -> &[SymbolInformation] {
        self.document_symbols.get_or_init(|| {
            document_symbols::get_document_symbols(self)
                .into_iter()
                .map(|element| ToConvert((element, key.clone())).into())
                .collect()
        })
    }

    pub fn comments(&self) -> &HashMap<usize, CommentEntry> {
        self.comments.get_or_init(|| {
            let mut lines: HashMap<usize, CommentEntry> = Default::default();
            self.items().iter().for_each(|item| {
                if let Ok(item) = item {
                    if let LanguageItem::Comment(comment) = item.item() {
                        let line_column = self
                            .line_columns()
                            .get_line_column(self.text(), comment.inner().start_pos())
                            .unwrap();
                        let line = line_column.line();
                        lines.insert(line, CommentEntry::new(comment.inner()));
                    }
                }
            });
            // Second run of iteration needed.
            // Reason: inline comment come after an non comment item.
            self.items().iter().for_each(|item| {
                if let Ok(item) = item {
                    let is_comment = matches!(item.item(), LanguageItem::Comment(_));
                    if !is_comment {
                        let end = item.location().end_pos();
                        let line_column = self
                            .line_columns()
                            .get_line_column(self.text(), end)
                            .unwrap();
                        let line = line_column.line();
                        if let Some(comment) = lines.get_mut(&line) {
                            comment.set_to_inline();
                        }
                    }
                }
            });
            lines
        })
    }
}
