const BUG_REPORT: &str = "The LSP sever for the scripting language ion shell crashed !\n\
    This is a bug. Please report this crash as an issue under the link below\n\
    Link for Reporting: https://gitlab.redox-os.org/redox-os/ion_lsp";
const SEP: &str = "==============================================";
/// After calling this, the end of every panic message is appended with a notice of how to report a
/// bug as an end user, a link to the issue page is included.
/// In a perfect world, panic messages should not be triggered.
/// If they occurs however then the end user knows where to report this issue.
pub fn set_app_panic_hook() {
    use std::panic;

    panic::set_hook(Box::new(|panic_info| {
        // Reuse default panic by printing "panic_info"
        println!("{panic_info}\n{SEP}\n{}", BUG_REPORT);
    }));
}
