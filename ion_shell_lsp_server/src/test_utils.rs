pub use test_lsp_memory_fixture::TestLspMemoryFixture;

pub mod client_notification;
pub mod client_requests;

mod test_lsp_memory_fixture;

use lsp_server::Message;

#[derive(Debug, Clone)]
pub enum ActionLspClient {
    Send(Message),
    Received(Message),
}

pub type HistoryLspClient = Vec<ActionLspClient>;

pub fn to_json_value<T>(param: T) -> serde_json::Value
where
    T: serde::Serialize,
{
    serde_json::to_value(param).unwrap()
}

pub fn act_lsp_init() -> TestLspMemoryFixture {
    let mut fixture = TestLspMemoryFixture::default();

    fixture.send_as_client(client_requests::client_init_request());
    fixture.resv_as_client();
    fixture.send_as_client(client_notification::init_confirmation_for_server());
    fixture
}

pub fn act_lsp_shutdown(to_shutdown: &mut TestLspMemoryFixture) {
    to_shutdown.send_as_client(client_requests::shutdown());
    to_shutdown.resv_as_client();
    to_shutdown.send_as_client(client_notification::exit());
}
