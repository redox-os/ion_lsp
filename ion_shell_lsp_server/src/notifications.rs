use ion_shell_parser::ImmutableClonableText;
use log::{info, warn};
use lsp_server::Notification;
use lsp_types::{
    notification::{
        DidChangeTextDocument, DidCloseTextDocument, DidOpenTextDocument, DidSaveTextDocument,
        Notification as _, PublishDiagnostics,
    },
    DidOpenTextDocumentParams,
};
use lsp_types::{
    DidChangeTextDocumentParams, DidCloseTextDocumentParams, DidSaveTextDocumentParams,
    PublishDiagnosticsParams,
};

use crate::{diagnostics::DiagnosticModel, server_state::ServerState, LspFileIdentifier};
use crate::{AppResult, ToConvert};

/// A notification is consumed.
/// There is no response to return to the client at time of writing
///
/// # Errors
///
/// * If extracting the params of an incoming notification, parameter `not`.
/// This should not happen.
pub fn handle(state: &mut ServerState, not: Notification) -> AppResult<Option<Notification>> {
    fn publish_diagnostic(
        state: &mut ServerState,
        file_ident: LspFileIdentifier,
    ) -> Option<Notification> {
        state
            .parse_file_and_diagnostics_push(file_ident.clone())
            .map(|diagnostics| {
                let params = PublishDiagnosticsParams {
                    uri: file_ident.as_ref().clone(),
                    diagnostics,
                    version: None,
                };

                Notification::new(PublishDiagnostics::METHOD.to_string(), params)
            })
    }
    info!("got notification: {:?}", &not);
    match not.method.as_str() {
        DidOpenTextDocument::METHOD => {
            let params: DidOpenTextDocumentParams = not.extract(DidOpenTextDocument::METHOD)?;
            let file_ident: LspFileIdentifier = params.text_document.uri.into();
            info!(
                "Got opened text document with identifier: {:?}",
                &file_ident
            );
            let content = params.text_document.text.into();
            state.insert_or_update_file_unparsed(file_ident.clone(), content);
            if state.diagnostic_model() == DiagnosticModel::Push {
                return Ok(publish_diagnostic(state, file_ident));
            }
        }
        DidSaveTextDocument::METHOD => {
            let param: DidSaveTextDocumentParams = not.extract(DidSaveTextDocument::METHOD)?;

            let file_ident: LspFileIdentifier = ToConvert(param.text_document).into();
            info!(
                "Save notification of text document with identifier: {}",
                file_ident
            );
            if let Some(new_content) = param.text {
                let content = new_content.into();
                info!(
                    "Save notification: text document has new content.\n\
                    Identifier: {}",
                    file_ident
                );
                if state.diagnostic_model() == DiagnosticModel::Push {
                    return Ok(publish_diagnostic(state, file_ident));
                } else {
                    state.insert_or_update_file_unparsed(file_ident, content);
                }
            }
        }
        DidChangeTextDocument::METHOD => {
            let param: DidChangeTextDocumentParams = not.extract(DidChangeTextDocument::METHOD)?;

            if let Some(changes) = param.content_changes.first() {
                // According to the the LSP protocol
                // the 2 fields `range` and `range_length` are not
                // there if the field `text` is the whole new text
                // Source: https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentContentChangeEvent
                let has_full_content = changes.range.is_none() && changes.range_length.is_none();
                let file_ident = param.text_document.uri.into();
                if has_full_content {
                    let new_content = ImmutableClonableText::from(changes.text.as_str());
                    info!(
                        "Change notification for text document with new full content and the identifier: {}",
                        file_ident
                    );
                    state.insert_or_update_file_unparsed(file_ident, new_content);
                } else {
                    warn!(
                        "Change notification: This lsp server can only apply changes with the full new content of a changed file. \n\
                        Incremental file changes are not handled !\n\
                        File identifier: {}", file_ident
                    );
                }
            }
        }
        DidCloseTextDocument::METHOD => {
            let param: DidCloseTextDocumentParams = not.extract(DidCloseTextDocument::METHOD)?;
            let file_ident = param.text_document.uri.into();
            info!(
                "Closed notification for text document with identifier: {}",
                file_ident
            );
            state.remove_file(file_ident);
        }
        _ => (),
    }

    Ok(None)
}
