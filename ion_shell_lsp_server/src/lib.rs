#![deny(clippy::dbg_macro, clippy::todo)]

pub mod hover;
pub type LspJsonValue = serde_json::value::Value;
pub type LspRange = lsp_types::Range;
pub type LspPosition = lsp_types::Position;
pub type LspFileIdentifier = HeapAllocated<Url>;
pub type AppError = anyhow::Error;
pub type AppResult<T = ()> = Result<T, AppError>;
pub type ReadOnlyText = HeapAllocated<str>;
pub type ParsedContent = HeapAllocated<[SemanticResult]>;
pub type HeapAllocated<T> = Rc<T>;

pub fn parse_whole_file(input: impl Into<ReadOnlyText>) -> ParsedIonShellFile {
    let input = input.into();
    let tokens = IonTokenizer::new(&input);
    let lines = LineColumnMap::new(&input);
    let mut parser = IonParser::new(tokens, &input);
    let parsed: ParsedContent = IonSemanticAnalysis::new(parser.by_ref()).collect();
    let scopes = parser.into_scope_map();
    ParsedIonShellFile::new(input, parsed, lines, scopes)
}

pub use cli::CliArgs;
pub use ion_shell_file_parse_state::{IonShellFileParseState, ParsedIonShellFile};
use ion_shell_parser::{
    parsing::IonParser,
    semantic_analysis::{IonSemanticAnalysis, SemanticResult},
    IonTokenizer, LineColumnMap,
};
pub use to_convert::ToConvert;

pub mod document_symbols;
pub mod error_codes;
pub mod finding_symbols;
pub mod logging;
pub mod notifications;
pub mod panic_hook;
pub mod requests;
pub mod server_capabilities;
pub mod server_state;

mod cli;
mod diagnostics;
mod ion_shell_file_parse_state;
mod to_convert;

#[cfg(test)]
mod test_utils;

use std::{cmp::Ordering, rc::Rc};

use log::{error, info};
use lsp_server::{Connection, Message, RequestId, Response};
use lsp_types::{
    request::{Request as _, Shutdown},
    Url,
};
use server_state::ServerState;

pub fn is_within_range(hay: LspRange, needle: LspPosition) -> bool {
    match order_lsp_pos(needle, hay.start) {
        Ordering::Equal => true,
        Ordering::Less => false,
        Ordering::Greater => match order_lsp_pos(needle, hay.end) {
            Ordering::Less | Ordering::Equal => true,
            Ordering::Greater => false,
        },
    }
}

pub fn order_lsp_pos(left: LspPosition, right: LspPosition) -> Ordering {
    match left.line.cmp(&right.line) {
        Ordering::Equal => left.character.cmp(&right.character),
        not_equal => not_equal,
    }
}

pub fn order_between_range_start_pos(hay: LspRange, needle: LspPosition) -> Ordering {
    order_lsp_pos(hay.start, needle)
}

pub fn order_between_range_end_pos(hay: LspRange, needle: LspPosition) -> Ordering {
    order_lsp_pos(hay.end, needle)
}

/// # Errors
///
/// * If sending a response to a request fails.
pub fn main_loop(connection: Connection, state: &mut ServerState) -> Result<(), AppError> {
    for msg in &connection.receiver {
        info!("got msg: {:?}", &msg);
        let result: Result<(), AppError> = match msg {
            Message::Request(req) => match req.method.as_str() {
                Shutdown::METHOD => {
                    if connection.handle_shutdown(&req)? {
                        return Ok(());
                    } else {
                        Err(anyhow::anyhow!(
                            "Failed to handle requested shutdown of client"
                        ))
                    }
                }
                _ => {
                    let send_back = requests::try_return_response_to_request(state, req);
                    match send_back {
                        Ok(response) => {
                            info!(
                                "Sending the following response back to the client: {:?}",
                                &response
                            );
                            connection.sender.send(Message::Response(response))?;
                            Ok(())
                        }
                        Err((error, id)) => {
                            let internal_error =
                                create_internal_error_response(id, error.to_string());
                            connection.sender.send(Message::Response(internal_error))?;
                            Ok(())
                        }
                    }
                }
            },
            Message::Response(resp) => {
                info!("got response: {:?}", resp);
                Ok(())
            }
            Message::Notification(not) => {
                if let Some(send_back) = notifications::handle(state, not)? {
                    connection.sender.send(Message::Notification(send_back))?;
                }
                Ok(())
            }
        };

        if let Err(error) = result {
            error!("Unhandled error by the LSP server: {:#?}", error);
        }
    }
    Ok(())
}

pub fn create_internal_error_response(id: RequestId, error_message: String) -> Response {
    let error = ToConvert((error_message, crate::error_codes::INTERNAL_ERROR)).into();
    Response {
        id,
        result: None,
        error: Some(error),
    }
}
