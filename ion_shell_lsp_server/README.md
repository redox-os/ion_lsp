# Language server for the scripting language of ion shell

## Functionality

The following features are available to an editors via this language server
for the scripting language of [ion shell][ion_shell].
These features are provided via the [LSP protocol][lsp_protocol].

This LSP server provides the following language features:

- Diagnostics: Shows errors in the syntax of a ion shell script. Semantic errors like referencing 
  a not defined variable are not supported right now.
- Go to definition: 
  - Variable Reference: Jumps from a reference to a variable to its declaration in the same file.
  - Function Reference: Jumps invocation to its declaration in the same file.
- Find References:
  - Variable Reference: List all references to and 
    redeclaration of the variable name within current and 
    all children scopes within the same file
  - Function Reference: List all invocation of the declared function
- Document Symbols: Shows all declared variables and functions within a script
- Hover information: Shows the following declaration information 
  on hovering over a function call or variable reference. 
  - Type of declaration (variable of function) 
  - Name of declaration (variable or function name)
  - Line of declaration
  - All comments 
     - without blank lines in between 
     - coming before the declaration 
     - Not being part of another declaration.
  - Inline comment, right from the declaration up to end of line
  - Specific to functions 
    - Arguments with optional type annotation.
    - Doc strings if declaration is for a function
  - Specific to variables
    - Type annotation if variable name has a type annotation.

**Note**: This LSP server currently parses every file in isolation without considering 
the sourcing of other files. This fact has the following consequences:
 - Diagnostics are only shown of opened files within the editor.
 - Go to definitions can only jump to declarations within the same file of the selected reference.
 - Find references does only find all references within the same file.

### Consideration of implementing analysis of files sourcing other files fully

These limitation of could be fixed partially. Partially in the sense that only static sourcing could
be used to analyze files sourcing other file fully. A static source uses only paths which are 
resolvable before runtime aka without executing the script. As soon as a dynamic source statement
is encountered, certain semantic errors like using an undefined variable could not be generated without
the risk of a false positive for example. 
Another problem is that files could source each other which results in a cycle.  

These represents the following challenges to overcome:

- Detect if sourcing is static or dynamic.
- How to handle dynamic sourcing. 
  Only provided full functionality if only static sourcing is used 
  or do get sections which are static, more analyzed than dynamic sections.
- How to detect cyclic sourcing and how to deal with it in the LSP analysis.


## How to install the LSP Server

**Over crate.io => recommended for user of LSP server**
```
cargo install ion_shell_lsp_server
```

**Latest changes on main branch => over git**

```
cargo install --git https://gitlab.redox-os.org/redox-os/ion_lsp ion_shell_lsp_server
```

## Nvim

Put this into your init.lua file:

```lua
vim.api.nvim_create_autocmd('FileType', {
  pattern = 'ion',
  callback = function()
    vim.lsp.start({
      name = 'ion_shell_lsp_server',
      cmd = { 'ion_shell_lsp_server' },
    })
  end,
})
```

This examples assumes that the LSP server can be found under PATH variable with name "ion_shell_lsp_server".

## Changelog

See the [changelogs](./CHANGELOG.md)

## Contributing

See the following [guide lines][contributing] 

## License 

This project, all workspace members and the visual code extension are licensed under [MIT License][LICENSE]

[License]: LICENSE
[ion_shell]: https://gitlab.redox-os.org/redox-os/ion
[lsp_protocol]: https://microsoft.github.io/language-server-protocol/
[contributing]: ../CONTRIBUTING.md 
