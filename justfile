vs_folder := "vs_code_extension"
cargo_test_parser := "cargo test --package ion_shell_parser"
cargo_test_server := "cargo test --package ion_shell_lsp_server"
go_to_vs_ext := "cd vs_code_extension &&"

cov_parser: 
  cargo llvm-cov --open --package ion_shell_parser

test: 
  cargo test --all

test_server TEST="": 
  {{cargo_test_server}} {{TEST}}

test_parser TEST="":
  {{cargo_test_parser}} {{TEST}} 

test_ignored:
  cargo test --all -- --ignored

test_debug_parser TEST:
  {{cargo_test_parser}} --features ion_shell_parser_trace_parse_phase -- {{TEST}}

rs_check_format $RUSTFLAGS='-D warnings': 
  cargo fmt --check --all

ts_check_format:
  {{go_to_vs_ext}} npm run format:check

check_format: rs_check_format ts_check_format

check_spelling: 
  typos

ts_lint: 
  {{go_to_vs_ext}} npm run lint

vs_build:
  {{go_to_vs_ext}} npm run compile

clippy $RUSTFLAGS='-D warnings':
  cargo clippy --all

check_all: rs_check_format test clippy check_spelling vs_build ts_check_format ts_lint 


