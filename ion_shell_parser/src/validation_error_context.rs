use crate::{
    macros::gen_impl_dbg_txt,
    parsing::{HasLocation, RangeDebugText},
    range::Range,
    ProblemSeverity,
};
use derive_more::Display;
/// Command data of syntactic and semantic errors
#[derive(Debug, PartialEq, Clone, Display)]
#[display(fmt = "`{0}`: {0}", range_dbg_txt)]
pub struct ValidationErrorMeta {
    range_dbg_txt: RangeDebugText,
    severity: ProblemSeverity,
}

impl From<Range> for ValidationErrorMeta {
    fn from(value: Range) -> Self {
        let range_dbg_txt = value.into();
        let severity = Default::default();
        Self {
            range_dbg_txt,
            severity,
        }
    }
}

impl ValidationErrorMeta {
    pub fn new(range_dbg_txt: RangeDebugText, severity: ProblemSeverity) -> Self {
        Self {
            range_dbg_txt,
            severity,
        }
    }

    pub fn severity(&self) -> ProblemSeverity {
        self.severity
    }

    pub fn as_warning(mut self) -> Self {
        self.severity = ProblemSeverity::Warning;
        self
    }

    pub fn range_dbg_txt(&self) -> &RangeDebugText {
        &self.range_dbg_txt
    }
}

gen_impl_dbg_txt! {ValidationErrorMeta, range_dbg_txt}

impl HasLocation for ValidationErrorMeta {
    fn location(&self) -> crate::range::Range {
        self.range_dbg_txt.location()
    }
}
