use derive_more::Display;
#[derive(Debug, Clone, Copy, PartialEq, Eq, Display)]
pub enum ProblemSeverity {
    Error,
    Warning,
}

impl Default for ProblemSeverity {
    fn default() -> Self {
        Self::Error
    }
}
