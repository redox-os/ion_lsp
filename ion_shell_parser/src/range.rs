use crate::parsing::HasLocation;

/// Guarantee:
/// `start` is always equal or smaller than `end`.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Default, PartialOrd, Ord)]
pub struct Range {
    start: usize,
    end: usize,
}

impl HasLocation for Range {
    fn location(&self) -> Range {
        *self
    }
}

impl From<(usize, usize)> for Range {
    fn from((start, end): (usize, usize)) -> Self {
        Range::new_internal(start, end)
    }
}

impl std::fmt::Display for Range {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Bytes:({},{}) ", self.start, self.end)
    }
}

impl Range {
    pub fn new_point(point: usize) -> Self {
        let (start, end) = (point, point);
        Self { start, end }
    }

    pub fn is_within(&self, needle: usize) -> bool {
        self.start <= needle && self.end >= needle
    }

    pub fn end_pos(&self) -> usize {
        self.end
    }

    pub fn start_pos(&self) -> usize {
        self.start
    }

    /// Parameter `start` should be greater than the `end`.
    pub(crate) fn new_internal(start: usize, end: usize) -> Self {
        debug_assert!(
            start <= end,
            "start must not be after the end byte. Start: `{}` and End: `{}`",
            start,
            end
        );
        Self { start, end }
    }

    /// Parameter `start` should be greater than the `end`.
    pub(crate) fn from_ranges_internal(start: &impl HasLocation, end: &impl HasLocation) -> Self {
        let (start, end) = (start.location().start_pos(), end.location().end_pos());
        Self::new_internal(start, end)
    }
    /// Parameter `start` should be greater than the `end`.
    pub(crate) fn from_start_end_internal(start: Range, end: Range) -> Self {
        let (start, end) = (start.start_pos(), end.end_pos());
        Self::new_internal(start, end)
    }
}

pub(crate) struct ToRange<T>(T);

impl<T> ToRange<T> {
    pub fn new(value: T) -> Self {
        Self(value)
    }
}

impl<L, R> From<ToRange<(&L, &R)>> for Range
where
    L: HasLocation,
    R: HasLocation,
{
    fn from(value: ToRange<(&L, &R)>) -> Self {
        let (left, right) = value.0;
        Range::from_ranges_internal(left, right)
    }
}

impl From<ToRange<(Range, Range)>> for Range {
    fn from(value: ToRange<(Range, Range)>) -> Self {
        let (start_range, end_range) = value.0;
        let (start, end) = (start_range.start, end_range.end);
        Range::new_internal(start, end)
    }
}

impl<T, S> From<ToRange<(&T, &[S])>> for Range
where
    T: HasLocation,
    S: HasLocation,
{
    fn from(value: ToRange<(&T, &[S])>) -> Self {
        let value = value.0;
        let (first, rest) = value;
        let first = first.location();
        let last = rest
            .last()
            .map(|element| element.location())
            .unwrap_or(first);
        ToRange((first, last)).into()
    }
}
