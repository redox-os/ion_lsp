pub(crate) use enclosed_range::EnclosedRange;
pub use error_recovery::ChosenErrRecoveryStrategy;
pub use ion_parser::{IonParser, ScopeMap, ScopeRelation};
pub use local_scope::LocalScope;
pub use matched_fragments::SigilKind;
pub use parsing_error::{ParsingError, ParsingErrorKind};
pub use range_debug_text::RangeDebugText;

pub mod language_items;
pub mod prelude;

pub(crate) mod internal_prelude {
    pub(crate) use super::{
        adapting_token_fetching::SkippedToNext,
        ion_parser::TokenFetcherForParsing,
        language_items::{LanguageItem, Value, ValueContext},
        parsing_error::{
            InvalidCommandForOperator, InvalidError, IsNotError, MissingCommandFor, MissingError,
            MissingValueFor, NoEosAfter, ParsingError, ParsingErrorKind,
        },
        HasLocation, ParsingResult, RangeDebugText, RecoverableError,
    };
    pub(crate) use crate::{
        tokens::{IonToken, TokenKind},
        Range, ToRange,
    };
}

mod command;

mod adapting_token_fetching;
mod enclosed_range;
mod error_recovery;
mod ion_parser;
mod local_scope;
mod matched_fragments;
mod parsing_error;
mod parsing_rules;
mod range_debug_text;

#[cfg(test)]
mod test_utils;

#[cfg_attr(
    not(feature = "ion_shell_parser_trace_parse_phase"),
    allow(unused_imports, unused_macros)
)]
mod tracing_utils;

use self::{error_recovery::RecoveryStrategy, language_items::LanguageItemContext};
use crate::{range::Range, ToRange};

pub type ParsingResult<T = LanguageItemContext> = Result<T, ParsingError>;

#[cfg(all(debug_assertions, test))]
pub trait HasOptAllocatedDebugText {
    fn go_through_dbg_txt(&mut self, on_found: &dyn Fn(Range) -> crate::ImmutableClonableText);
}
#[cfg(all(debug_assertions, test))]
impl<T> HasOptAllocatedDebugText for Option<T>
where
    T: HasOptAllocatedDebugText,
{
    fn go_through_dbg_txt(&mut self, on_found: &dyn Fn(Range) -> crate::ImmutableClonableText) {
        if let Some(e) = self {
            e.go_through_dbg_txt(on_found);
        }
    }
}

#[cfg(all(debug_assertions, test))]
impl<T> HasOptAllocatedDebugText for ParsingResult<T>
where
    T: HasOptAllocatedDebugText,
{
    fn go_through_dbg_txt(&mut self, on_found: &dyn Fn(Range) -> crate::ImmutableClonableText) {
        match self {
            Ok(item) => item.go_through_dbg_txt(on_found),
            Err(error) => error.go_through_dbg_txt(on_found),
        }
    }
}

#[cfg(all(debug_assertions, test))]
impl<T> HasOptAllocatedDebugText for Vec<T>
where
    T: HasOptAllocatedDebugText,
{
    fn go_through_dbg_txt(&mut self, on_found: &dyn Fn(Range) -> crate::ImmutableClonableText) {
        self.into_iter()
            .for_each(|element| element.go_through_dbg_txt(&on_found));
    }
}

pub trait RecoverableError: Sized {
    fn recoverable_from(self, strategy: ChosenErrRecoveryStrategy) -> Self;

    fn recoverable_as_fallback(self, strategy: ChosenErrRecoveryStrategy) -> Self;
    fn with_put_last_token_back(self) -> Self;

    fn recoverable_from_next_token_preserved(self) -> Self {
        self.recoverable_from(RecoveryStrategy::NextToken.into())
            .with_put_last_token_back()
    }

    fn fallback_recoverable_from_next_token_preserved(self) -> Self {
        self.recoverable_as_fallback(RecoveryStrategy::NextToken.into())
            .with_put_last_token_back()
    }

    fn fallback_recoverable_from_next_eos(self) -> Self {
        self.recoverable_as_fallback(RecoveryStrategy::EndOfStatement.into())
    }

    fn fallback_recoverable_from_next_end_keyword(self) -> Self {
        self.recoverable_as_fallback(RecoveryStrategy::NextEndKeyword.into())
    }

    fn recoverable_from_next_end_keyword(self) -> Self {
        self.recoverable_from(RecoveryStrategy::NextEndKeyword.into())
    }
    fn recoverable_from_next_eos(self) -> Self {
        self.recoverable_from(RecoveryStrategy::EndOfStatement.into())
    }
}

impl<T> RecoverableError for Result<T, ParsingError> {
    fn recoverable_from(self, strategy: ChosenErrRecoveryStrategy) -> Self {
        self.map_err(|error| error.recoverable_from(strategy))
    }
    fn recoverable_as_fallback(self, strategy: ChosenErrRecoveryStrategy) -> Self {
        self.map_err(|error| error.recoverable_as_fallback(strategy))
    }

    fn with_put_last_token_back(self) -> Self {
        self.map_err(|error| error.with_put_last_token_back())
    }
}

pub trait HasLocation {
    fn location(&self) -> Range;
}

/// Returns `None` if parameter `seq` is empty
pub(crate) fn whole_range_from_seq(seq: &[impl HasLocation]) -> Option<Range> {
    #[cfg(any(test, debug_assertions))]
    {
        let unsorted: Vec<Range> = seq.iter().map(|e| e.location()).collect();
        let mut sorted: Vec<Range> = unsorted.clone();
        sorted.sort();
        assert_eq!(sorted, unsorted, "parameter as sequence must be sorted.");
    }

    match seq {
        [] => None,
        [single] => Some(single.location()),
        [first, last] | [first, .., last] => Some(ToRange::new((first, last)).into()),
    }
}

/// Returns true if parameter `name` is valid variable name.
///
/// A valid variable name:
///
/// - May start and contain a "_".  
/// - Must not start with a number
/// - Every char may be an alphanumeric one except the first one.
pub(crate) fn is_valid_variable_name(name: &str) -> bool {
    let mut iter = name.chars();
    iter.next().map_or(false, |c| c.is_alphabetic() || c == '_')
        && iter.all(|c| c.is_alphanumeric() || c == '_')
}
