use std::collections::VecDeque;

pub use ion_validation_error::ValidationError;
pub use semantic_error::{SemanticError, SemanticKindError};

mod ion_validation_error;
mod semantic_error;

use crate::parsing::{language_items::LanguageItemContext, ParsingResult};

pub type SemanticResult = Result<LanguageItemContext, ValidationError>;

#[derive(Debug)]
pub struct IonSemanticAnalysis {
    parsed: VecDeque<ParsingResult>,
}

impl Iterator for IonSemanticAnalysis {
    type Item = SemanticResult;

    fn next(&mut self) -> Option<Self::Item> {
        let parsed = self.parsed.pop_front()?.map_err(ValidationError::Syntax);
        Some(parsed)
    }
}

impl IonSemanticAnalysis {
    pub fn new(parsed: impl Iterator<Item = ParsingResult>) -> Self {
        let parsed = parsed.into_iter().collect();
        Self { parsed }
    }
}

#[cfg(test)]
mod testing {
    use crate::{parsing::IonParser, tokens::IonTokenizer};

    use super::*;

    type TestResult = Vec<SemanticResult>;
    type TestErrsOnly = Vec<ValidationError>;

    #[test]
    fn show_diagnostics_for_wrong_export_and_let_statement() {
        let input = include_str!("input_files_for_tests/with_wrong_export_and_let_statement.txt");
        let actual: TestErrsOnly = get_errors_only(input);
        insta::assert_debug_snapshot!(actual);
    }

    fn get_analysis(input: &str) -> TestResult {
        let tokens = IonTokenizer::new(input);
        let parsed = IonParser::new(tokens, input);
        IonSemanticAnalysis::new(parsed).collect()
    }

    fn get_errors_only(input: &str) -> TestErrsOnly {
        get_analysis(input)
            .into_iter()
            .filter_map(|error| {
                if let Err(error) = error {
                    Some(error)
                } else {
                    None
                }
            })
            .collect()
    }
}
