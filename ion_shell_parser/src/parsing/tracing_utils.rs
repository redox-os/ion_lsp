macro_rules! emit_object_with_message {
    ($label:expr, $value:expr) => {
        #[cfg(feature = "ion_shell_parser_trace_parse_phase")]
        {

            tracing::event!(tracing::Level::INFO,  $label = ?$value);

        }
    };
    (($a:expr, $b:expr) => ($c:expr, $d:expr)) => {
        #[cfg(feature = "ion_shell_parser_trace_parse_phase")]
        {
            tracing::event!(tracing::Level::INFO, $a = ?$c, $b = ?$d);
        }
    };
}

macro_rules! emit_message {
    ($msg:expr) => {
        #[cfg(feature = "ion_shell_parser_trace_parse_phase")]
        {
            tracing::info!($msg);
        }
    };
}

pub(crate) use emit_message;
pub(crate) use emit_object_with_message;
