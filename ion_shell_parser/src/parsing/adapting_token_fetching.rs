//! Enums which are yielded by the iteration token functions of [`super::IonParser`].
//! They provide a chaining API to narrow the token kind down further.
//!
//! It often used by the parsing functions in the module [`super::parsing_rules`]
//! to query if for example there is a non noise token before the end of a statement.

use crate::IonToken;

use super::{
    command::NoCommand, ion_parser::TokenFetcherForParsing, matched_fragments::MatchedCommand,
};

#[derive(Debug, PartialEq)]
pub enum SkippedToNext {
    EndOfStatement(IonToken),
    Next(IonToken),
    Eoi,
}

#[derive(Debug)]
pub enum SkippedAndNoEoi {
    EndOfStatement(IonToken),
    Next(IonToken),
}

impl SkippedAndNoEoi {
    pub fn token(self) -> IonToken {
        match self {
            SkippedAndNoEoi::EndOfStatement(token) | SkippedAndNoEoi::Next(token) => token,
        }
    }
}

impl SkippedToNext {
    /// Returns `None` If end of input was reached
    pub fn expect_no_eoi(self) -> Option<SkippedAndNoEoi> {
        match self {
            SkippedToNext::EndOfStatement(next) => Some(SkippedAndNoEoi::EndOfStatement(next)),
            SkippedToNext::Next(next) => Some(SkippedAndNoEoi::Next(next)),
            SkippedToNext::Eoi => None,
        }
    }

    pub fn expect_no_end_of_statement_no_eoi(self) -> Option<IonToken> {
        let without_eoi = self.expect_no_eoi()?;
        match without_eoi {
            SkippedAndNoEoi::EndOfStatement(_) => None,
            SkippedAndNoEoi::Next(next) => Some(next),
        }
    }

    pub(crate) fn match_to_command<T>(
        self,
        context: &mut TokenFetcherForParsing<T>,
    ) -> Result<MatchedCommand, NoCommand>
    where
        T: Iterator<Item = IonToken>,
    {
        context.match_command_from_skipped(self)
    }
}
