use crate::{
    parsing::{internal_prelude::*, language_items::UnQuoatedStr},
    NumberValue,
};

use super::ParsingError;

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn parse_number_value(&mut self, matched_number: NumberValueToken) -> ValueContext {
        let (number_value, token) = matched_number.into();
        match self.next_token() {
            Some(maybe_work_token) if maybe_work_token.kind() == TokenKind::Word => {
                let range = ToRange::new((&token, &maybe_work_token)).into();
                ValueContext::new(range, Value::UnQuotedStr(UnQuoatedStr::Plain))
            }
            Some(maybe_work_token) => {
                self.push_back(maybe_work_token);
                ValueContext::new(token.location(), Value::Number(number_value))
            }
            None => ValueContext::new(token.location(), Value::Number(number_value)),
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn parse_word_starting_with_operator(
        &mut self,
        token: IonToken,
    ) -> ParsingResult<ValueContext> {
        fn create_invalid_value(
            start_token: &impl HasLocation,
            next_token: &impl HasLocation,
        ) -> ParsingError {
            let range = Range::from_ranges_internal(start_token, next_token);
            ParsingError::new(range, InvalidError::Value.into())
        }

        fn create_str_value(
            start_token: &impl HasLocation,
            previous_token_range: &impl HasLocation,
        ) -> ValueContext {
            let range = Range::from_ranges_internal(start_token, previous_token_range);
            ValueContext::new(range, Value::UnQuotedStr(UnQuoatedStr::Plain))
        }

        let start_token = token;
        let mut previous_token_range = start_token.location();
        loop {
            let Some(next_token) = self.next_token() else {
                let range = Range::from_ranges_internal(&start_token, &previous_token_range);
                return Ok(ValueContext::new(
                    range,
                    Value::UnQuotedStr(UnQuoatedStr::Plain),
                ));
            };

            match next_token.kind() {
                TokenKind::Word => {
                    return Ok(create_str_value(&start_token, &next_token));
                }
                TokenKind::Number(_) => previous_token_range = next_token.location(),
                TokenKind::WhiteSpace(_)
                | TokenKind::OpenCurlyBracket
                | TokenKind::Tab(_)
                | TokenKind::ArraySigil
                | TokenKind::StringSigil
                | TokenKind::Eoi
                | TokenKind::NewLine => {
                    self.push_back(next_token);
                    return Ok(create_str_value(&start_token, &previous_token_range));
                }
                token if token.is_word_like() => previous_token_range = next_token.location(),
                _ => return Err(create_invalid_value(&start_token, &next_token)),
            }
        }
    }
}

#[derive(Debug)]
pub struct NumberValueToken {
    value: NumberValue,
    token: IonToken,
}

impl From<NumberValueToken> for (NumberValue, IonToken) {
    fn from(value: NumberValueToken) -> Self {
        (value.value, value.token)
    }
}

impl NumberValueToken {
    pub fn new(value: NumberValue, token: IonToken) -> Self {
        debug_assert!(matches!(token.kind(), TokenKind::Number(_)));
        Self { value, token }
    }
}
