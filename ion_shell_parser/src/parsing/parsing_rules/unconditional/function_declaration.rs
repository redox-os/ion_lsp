use std::collections::HashSet;

use crate::{
    parsing::{
        internal_prelude::*,
        language_items::{
            variables::{TypeAnnotationContext, VariableDeclaration, VariableName},
            DocString, FunctionDeclaration,
        },
        matched_fragments::MatchedFn,
    },
    tokens::TypeAnnotation,
};

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_par
        ser_trace_
       parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn function_declaration(
        &mut self,
        start_token: MatchedFn,
    ) -> ParsingResult<FunctionDeclaration> {
        let function_name = self.expect_variable_name(&start_token)?;
        let (arguments, docstring) = self.variable_declaration_for_function(&start_token)?;

        let mut arguments_name: HashSet<&str> = Default::default();
        for next in arguments.iter() {
            let name = next.name();
            let range = name.location();
            let text = crate::get_string_from_input(self.input(), range);
            if !arguments_name.insert(text) {
                return Err(ParsingError::new(
                    range,
                    ParsingErrorKind::DuplicateArguments,
                ));
            }
        }

        let function = FunctionDeclaration::new(&start_token, function_name, arguments, docstring);
        Ok(function)
    }

    #[cfg_attr(
        feature = "ion_shell_par
        ser_trace_
       parse_phase",
        tracing::instrument(skip(self))
    )]
    fn variable_declaration_for_function(
        &mut self,
        start: &MatchedFn,
    ) -> ParsingResult<(Vec<VariableDeclaration>, Option<DocString>)> {
        let mut names: Vec<VariableDeclaration> = Default::default();
        loop {
            match self.skip_horz_noise() {
                SkippedToNext::Eoi | SkippedToNext::EndOfStatement(_) => return Ok((names, None)),
                SkippedToNext::Next(next) => match next.kind() {
                    TokenKind::Hash => {
                        self.push_back(next);
                        return Ok((names, None));
                    }
                    TokenKind::StartDocstring => {
                        let comment = self.parse_comment(next);
                        return Ok((names, Some(DocString::new(comment))));
                    }
                    _ => {
                        self.push_back(next);
                        let next_name = self.expect_variable_name(start)?;
                        match self.next_token() {
                            Some(expected_as_type) => match expected_as_type.kind() {
                                TokenKind::TypeIdent(ident) => {
                                    let declaration =
                                        self.on_type_ident(next_name, expected_as_type, ident)?;
                                    names.push(declaration)
                                }
                                _ => {
                                    let declaration = VariableDeclaration::new(next_name, None);
                                    self.push_back(expected_as_type);
                                    names.push(declaration);
                                }
                            },
                            None => {
                                let declaration = VariableDeclaration::new(next_name, None);
                                names.push(declaration);
                            }
                        };
                    }
                },
            }
        }
    }

    fn on_type_ident(
        &mut self,
        next_name: VariableName,
        expected_as_type: IonToken,
        ident: TypeAnnotation,
    ) -> ParsingResult<VariableDeclaration> {
        let context = TypeAnnotationContext::new(expected_as_type.location(), ident);
        match self.next_token() {
            Some(must_not_be_noises) if !must_not_be_noises.kind().is_noise() => {
                let range = Range::from_start_end_internal(
                    expected_as_type.location(),
                    must_not_be_noises.location(),
                );
                return Err(ParsingError::new(
                    range,
                    InvalidError::TypeAnnotation.into(),
                ));
            }
            Some(push_back) => self.push_back(push_back),
            None => (),
        }
        let declaration = VariableDeclaration::new(next_name, Some(context));
        Ok(declaration)
    }
}

#[cfg(test)]
mod testing {
    use crate::parsing::test_utils;

    #[test]
    fn detect_duplicate_argument_name() {
        const INPUT: &str = include_str!("input_files_for_tests/error_duplicate_arguments.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }
}
