use crate::{
    parsing::{
        command::NoCommand,
        internal_prelude::*,
        language_items::commands::{
            ChainLogicalOperatorContext, CommandChain, CommandExpression,
            FileInteractionWithCommand, NextCommand, NextPipedCommand, PipeOfCommands,
            PipeOperatorContext, RawCommandExpression,
        },
        matched_fragments::MatchedCommand,
        parsing_rules::unconditional::{
            unconditional_fragments::{CommandValue, FileRedirectToken},
            validation_builins::{self, TextForValueProvider},
        },
        tracing_utils,
    },
    prelude::{
        command_operators::DetachOperator,
        commands::{DetachOperatorContext, FileRedirectContext},
    },
};

use super::unconditional_fragments::CommandChainOperator;

#[derive(Debug)]
pub(crate) enum OperatorAfterPipe {
    Logical(ChainLogicalOperatorContext),
    Detach(DetachOperatorContext),
}

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn command_chain(&mut self, first_command: MatchedCommand) -> ParsingResult<CommandChain> {
        fn create_parsing_err_from_no_command<T>(
            fetcher: &TokenFetcherForParsing<T>,
            no_command: NoCommand,
            current_logical: &ChainLogicalOperatorContext,
        ) -> ParsingError
        where
            T: Iterator<Item = IonToken>,
        {
            match no_command {
                NoCommand::Eoi => ParsingError::new(
                    ToRange::new((current_logical, &fetcher.last_location())).into(),
                    MissingCommandFor::Logical(current_logical.location()).into(),
                ),
                NoCommand::EndOfStatement(eos) => ParsingError::new(
                    ToRange::new((current_logical.location(), eos.location())).into(),
                    MissingCommandFor::Logical(current_logical.location()).into(),
                )
                .recoverable_from_next_token_preserved(),
                NoCommand::IllegalOperator(token) => {
                    ParsingError::new(token.location(), InvalidError::Command.into())
                }
            }
        }
        let mut rest: Vec<NextCommand> = Vec::default();
        let first = self.pipe_of_commands(first_command)?;
        match first {
            (only_piped, None) => Ok(CommandChain::new(only_piped, rest, None)),
            (only_piped, Some(OperatorAfterPipe::Detach(detach))) => {
                Ok(CommandChain::new(only_piped, rest, Some(detach)))
            }
            (first_piped, Some(OperatorAfterPipe::Logical(first_logical))) => {
                let mut current_logical = first_logical;
                loop {
                    let matched: MatchedCommand = match self
                        .skip_horz_noise()
                        .match_to_command(self)
                    {
                        Ok(matched) => Ok(matched),
                        Err(NoCommand::EndOfStatement(token)) if token.kind().is_new_line() => self
                            .skip_horz_noise()
                            .match_to_command(self)
                            .map_err(|error| {
                                create_parsing_err_from_no_command(self, error, &current_logical)
                            }),
                        Err(NoCommand::EndOfStatement(token)) => Err(ParsingError::new(
                            token.location(),
                            MissingCommandFor::Logical(current_logical.location()).into(),
                        ))
                        .recoverable_from_next_token_preserved(),
                        error => error.map_err(|error| {
                            create_parsing_err_from_no_command(self, error, &current_logical)
                        }),
                    }?;

                    let (next_piped, maybe_next_logical) = self.pipe_of_commands(matched)?;
                    let current_last = NextCommand::new(current_logical, next_piped);
                    rest.push(current_last);

                    match maybe_next_logical {
                        None => {
                            return Ok(CommandChain::new(first_piped, rest, None));
                        }
                        Some(OperatorAfterPipe::Detach(detach)) => {
                            return Ok(CommandChain::new(first_piped, rest, Some(detach)));
                        }
                        Some(OperatorAfterPipe::Logical(next_logical)) => {
                            current_logical = next_logical
                        }
                    }
                }
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn pipe_of_commands(
        &mut self,
        first_command: MatchedCommand,
    ) -> ParsingResult<(PipeOfCommands, Option<OperatorAfterPipe>)> {
        let mut rest: Vec<NextPipedCommand> = Default::default();
        match self.command_expression(first_command)? {
            (without_pipe, None) => Ok((PipeOfCommands::new(without_pipe, rest), None)),
            (without_pipe_before_logical, Some(CommandChainOperator::Detach(detach))) => Ok((
                PipeOfCommands::new(without_pipe_before_logical, rest),
                Some(OperatorAfterPipe::Detach(detach)),
            )),
            (without_pipe_before_logical, Some(CommandChainOperator::Logical(logical))) => Ok((
                PipeOfCommands::new(without_pipe_before_logical, rest),
                Some(OperatorAfterPipe::Logical(logical)),
            )),
            (with_pipe, Some(CommandChainOperator::Pipe(pipe))) => {
                let head_command = with_pipe;
                let mut current_pipe = pipe;
                loop {
                    let next_maybe_command = self
                        .skip_horz_noise()
                        .match_to_command(self)
                        .map_err(|error| match error {
                            NoCommand::Eoi => ParsingError::new(
                                ToRange::new((&current_pipe, &self.last_location())).into(),
                                MissingCommandFor::Pipe(current_pipe.location()).into(),
                            ),
                            NoCommand::EndOfStatement(token) => ParsingError::new(
                                ToRange::new((&current_pipe, &token)).into(),
                                MissingCommandFor::Pipe(current_pipe.location()).into(),
                            )
                            .recoverable_from_next_token_preserved(),
                            NoCommand::IllegalOperator(token) => ParsingError::new(
                                ToRange::new((&current_pipe, &token)).into(),
                                InvalidError::CommandAfterPipe(InvalidCommandForOperator::new(
                                    token.location(),
                                    current_pipe.location(),
                                ))
                                .into(),
                            ),
                        })?;

                    let actual_expression = self.command_expression(next_maybe_command)?;
                    match actual_expression {
                        (
                            before_logical,
                            Some(CommandChainOperator::Logical(logical_ends_piping_here)),
                        ) => {
                            let tail_at_piping =
                                NextPipedCommand::new(current_pipe, before_logical);
                            rest.push(tail_at_piping);
                            return Ok((
                                PipeOfCommands::new(head_command, rest),
                                Some(OperatorAfterPipe::Logical(logical_ends_piping_here)),
                            ));
                        }
                        (before_detach, Some(CommandChainOperator::Detach(detach))) => {
                            let tail_at_piping = NextPipedCommand::new(current_pipe, before_detach);
                            rest.push(tail_at_piping);
                            return Ok((
                                PipeOfCommands::new(head_command, rest),
                                Some(OperatorAfterPipe::Detach(detach)),
                            ));
                        }
                        (before_current_pipe, Some(CommandChainOperator::Pipe(next_pipe))) => {
                            let new_piped_command =
                                NextPipedCommand::new(current_pipe, before_current_pipe);
                            current_pipe = next_pipe;
                            rest.push(new_piped_command);
                        }
                        (last_command_in_pipe, None) => {
                            let tail_at_piping =
                                NextPipedCommand::new(current_pipe, last_command_in_pipe);
                            rest.push(tail_at_piping);
                            return Ok((PipeOfCommands::new(head_command, rest), None));
                        }
                    }
                }
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn command_expression(
        &mut self,
        command: MatchedCommand,
    ) -> ParsingResult<(CommandExpression, Option<CommandChainOperator>)> {
        #[cfg_attr(
            feature = "ion_shell_parser_trace_parse_phase",
            tracing::instrument(skip(parser))
        )]
        fn builtin_or_executable<T>(
            parser: &mut TokenFetcherForParsing<T>,
            command: MatchedCommand,
        ) -> ParsingResult<(RawCommandExpression, Option<CommandChainOperator>)>
        where
            T: Iterator<Item = IonToken>,
        {
            #[derive(Debug)]
            enum RedirectFileArgument {
                InsteadCommandValue(CommandValue),
                Redirect(FileInteractionWithCommand),
            }

            #[cfg_attr(
                feature = "ion_shell_parser_trace_parse_phase",
                tracing::instrument(skip(parser))
            )]
            fn argument_or_file_redirect<T>(
                parser: &mut TokenFetcherForParsing<T>,
                next_token: IonToken,
            ) -> ParsingResult<RedirectFileArgument>
            where
                T: Iterator<Item = IonToken>,
            {
                if let TokenKind::Redirect(file_redirect) = next_token.kind() {
                    let redirect = parser.file_redirect_rule(FileRedirectToken::new(
                        next_token.location(),
                        file_redirect,
                    ))?;
                    tracing_utils::emit_message!("Detected file redirection.");
                    Ok(RedirectFileArgument::Redirect(redirect))
                } else {
                    let value = parser.command_value(next_token)?;
                    Ok(RedirectFileArgument::InsteadCommandValue(value))
                }
            }

            let mut values: Vec<ValueContext> = Default::default();
            let mut redirections: Vec<FileInteractionWithCommand> = Default::default();

            loop {
                match parser.skip_horz_noise() {
                    SkippedToNext::Eoi => {
                        return Ok((
                            RawCommandExpression::new(command.into(), values, redirections),
                            None,
                        ))
                    }
                    SkippedToNext::EndOfStatement(eos) => {
                        parser.push_back(eos);
                        return Ok((
                            RawCommandExpression::new(command.into(), values, redirections),
                            None,
                        ));
                    }
                    SkippedToNext::Next(comment) if comment.kind().is_comment() => {
                        parser.push_back(comment);
                        return Ok((
                            RawCommandExpression::new(command.into(), values, redirections),
                            None,
                        ));
                    }
                    SkippedToNext::Next(next_token) => {
                        let value = argument_or_file_redirect(parser, next_token)?;
                        match value {
                            RedirectFileArgument::InsteadCommandValue(CommandValue::Value(
                                value,
                            )) => values.push(value),
                            RedirectFileArgument::InsteadCommandValue(CommandValue::None) => {
                                return Ok((
                                    RawCommandExpression::new(command.into(), values, redirections),
                                    None,
                                ))
                            }
                            RedirectFileArgument::Redirect(redirect) => redirections.push(redirect),
                            RedirectFileArgument::InsteadCommandValue(CommandValue::PipeOp(
                                found_pipe,
                            )) => {
                                return Ok((
                                    RawCommandExpression::new(command.into(), values, redirections),
                                    Some(CommandChainOperator::Pipe(found_pipe)),
                                ))
                            }
                            RedirectFileArgument::InsteadCommandValue(CommandValue::Logical(
                                found_logical,
                            )) => {
                                return Ok((
                                    RawCommandExpression::new(command.into(), values, redirections),
                                    Some(CommandChainOperator::Logical(found_logical)),
                                ))
                            }
                            RedirectFileArgument::InsteadCommandValue(CommandValue::Detach(
                                detach,
                            )) => {
                                return Ok((
                                    RawCommandExpression::new(command.into(), values, redirections),
                                    Some(CommandChainOperator::Detach(detach)),
                                ))
                            }
                        }
                    }
                }
            }
        }

        fn detect_validate_builtin<T>(
            fetcher: &TokenFetcherForParsing<T>,
            expression: RawCommandExpression,
        ) -> ParsingResult<CommandExpression>
        where
            T: Iterator<Item = IonToken>,
        {
            let (command, values, redirections) = expression.into();
            let input = TextForValueProvider::new(fetcher.input());
            let values = validation_builins::try_map_raw_to_executable_or_builtin(
                command.location(),
                input,
                values,
            )?;
            let to_return = CommandExpression::new(command, values, redirections);
            Ok(to_return)
        }

        let (builtin, rest) = builtin_or_executable(self, command)?;
        let validated = detect_validate_builtin(self, builtin)?;
        Ok((validated, rest))
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn command_value(&mut self, token: IonToken) -> ParsingResult<CommandValue> {
        let kind = token.kind();
        match kind {
            TokenKind::Pipe(op) => {
                tracing_utils::emit_message!("Pipe operator detected");
                Ok(CommandValue::PipeOp(PipeOperatorContext::new(
                    token.location(),
                    op,
                )))
            }
            TokenKind::LogicalChain(op) => {
                tracing_utils::emit_message!("Logical operator detected");
                Ok(CommandValue::Logical(ChainLogicalOperatorContext::new(
                    token.location(),
                    op,
                )))
            }
            TokenKind::ClosedParanteseCurlyBracket => {
                self.push_back(token);
                Ok(CommandValue::None)
            }
            TokenKind::Detach(DetachOperator::ToBackGround) => Ok(CommandValue::Detach(
                DetachOperatorContext::new(token.location(), DetachOperator::ToBackGround),
            )),
            TokenKind::Detach(DetachOperator::Disown) => Ok(CommandValue::Detach(
                DetachOperatorContext::new(token.location(), DetachOperator::Disown),
            )),
            _ => self
                .value_up_to_delimiter(token, |kind| kind == TokenKind::ClosedParanteseCurlyBracket)
                .map(|(value, end)| {
                    if let Some(end) = end {
                        self.push_back(end);
                    }
                    CommandValue::Value(value)
                }),
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn file_redirect_rule(
        &mut self,
        prev_context: FileRedirectToken,
    ) -> ParsingResult<FileInteractionWithCommand> {
        match self.skip_horz_noise() {
            SkippedToNext::Next(valid_file_names)
                if valid_file_names.kind().is_valid_file_name() =>
            {
                let file_redirect = prev_context.redirection();
                let context = FileRedirectContext::new(prev_context.location(), file_redirect);
                tracing_utils::emit_object_with_message!(
                    ("File redirection", "Valid file name") => (file_redirect, &valid_file_names)
                );
                Ok(FileInteractionWithCommand::new(
                    context,
                    valid_file_names.location(),
                ))
            }
            SkippedToNext::Next(invalid_file_name) => {
                let location = invalid_file_name.location();
                Err(ParsingError::new(
                    location,
                    InvalidError::FileRedirectArg.into(),
                ))
            }
            SkippedToNext::EndOfStatement(end_of_statement) => {
                let location = end_of_statement.location();
                Err(ParsingError::new(
                    location,
                    MissingError::ArgFileDirect.into(),
                ))
                .recoverable_from_next_token_preserved()
            }
            SkippedToNext::Eoi => Err(ParsingError::new(
                self.last_location(),
                MissingError::ArgFileDirect.into(),
            )),
        }
    }
}

#[cfg(test)]
mod testing {
    use crate::parsing::test_utils;

    #[test]
    fn parse_detaching_processes() {
        const INPUT: &str = include_str!("input_files_for_tests/detaching_processes.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }
}
