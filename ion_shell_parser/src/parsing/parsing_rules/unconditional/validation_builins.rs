use std::collections::HashMap;

use once_cell::sync::Lazy;

use crate::parsing::{
    self,
    internal_prelude::*,
    language_items::{
        builtins::SourceBuiltin,
        commands::{Builtin, ExecutableOrBuiltin},
    },
    parsing_error::{BuilinErrorKind, BuiltinError},
    tracing_utils,
};

pub type BuiltinMapResult = Result<ExecutableOrBuiltin, BuilinErrorKind>;
pub type CommandValues = Vec<ValueContext>;

type MapFunction = fn(CommandValues, TextForValueProvider) -> BuiltinMapResult;
type MappingSystem = HashMap<&'static str, MapFunction>;

static BUILTIN_MAP: Lazy<MappingSystem> =
    Lazy::new(|| HashMap::from([("source", source as MapFunction)]));

#[derive(Debug)]
pub struct TextForValueProvider<'a> {
    input: &'a str,
}

impl<'a> TextForValueProvider<'a> {
    pub fn new(input: &'a str) -> Self {
        Self { input }
    }

    pub fn text_from(&self, range: Range) -> &str {
        crate::get_string_from_input(self.input, range)
    }
}

#[cfg_attr(feature = "ion_shell_parser_trace_parse_phase", tracing::instrument)]
pub fn try_map_raw_to_executable_or_builtin(
    command_range: Range,
    input: TextForValueProvider,
    values: CommandValues,
) -> ParsingResult<ExecutableOrBuiltin> {
    let command_text = input.text_from(command_range);
    let range_from_start_to_end_value = parsing::whole_range_from_seq(&values);
    match BUILTIN_MAP.get(command_text) {
        None => Ok(ExecutableOrBuiltin::Executable(values)),
        Some(builtin) => {
            tracing_utils::emit_message!("Builtin invocation is being validated");
            builtin(values, input).map_err(|error| {
                let builtin_error = BuiltinError::new(command_range, error);
                let end_range = range_from_start_to_end_value.unwrap_or(command_range);
                let whole_range = ToRange::new((command_range, end_range)).into();
                ParsingError::new(whole_range, ParsingErrorKind::Builtin(builtin_error))
            })
        }
    }
}

fn source(values: CommandValues, _accessor: TextForValueProvider) -> BuiltinMapResult {
    const EXPECT_EXACT_NUMBER_ARGS: usize = 1;

    if values.len() < EXPECT_EXACT_NUMBER_ARGS {
        Err(BuilinErrorKind::TooFewArgs(EXPECT_EXACT_NUMBER_ARGS))
    } else {
        let mut without_source = values.into_iter();
        let to_source = without_source
            .next()
            .expect("Previous if check ensures that there is at least one element there.");
        let rest: Vec<ValueContext> = without_source.collect();
        let source_builtin = SourceBuiltin::new(to_source, rest);
        Ok(ExecutableOrBuiltin::Builtin(Builtin::Source(
            source_builtin,
        )))
    }
}
