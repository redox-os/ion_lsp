use crate::parsing::{
    command::NoCommand,
    internal_prelude::*,
    matched_fragments::{
        MatchedCase, MatchedElse, MatchedElseIf, MatchedFor, MatchedIfClause, MatchedMatch,
        MatchedWhile,
    },
    prelude::*,
    whole_range_from_seq,
};

use self::item_scopes::{ElseContext, EnterScope, ForLoopContext, ScopeCondition};
use self::{commands::CommandChain, variables::VariableName};

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn if_clause(&mut self, matched: MatchedIfClause) -> ParsingResult<ScopeCondition> {
        let matched: IonToken = matched.into();
        let command =
            self.parse_for_command_chain(&matched, MissingCommandFor::If.into(), |_| {
                InvalidError::CommandAfterIf.into()
            })?;
        let context = ScopeCondition::new(matched.location(), command);
        Ok(context)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn else_if_clause(&mut self, matched: MatchedElseIf) -> ParsingResult<ScopeCondition> {
        let matched: IonToken = matched.into();
        let command =
            self.parse_for_command_chain(&matched, MissingCommandFor::ElseIf.into(), |_| {
                InvalidError::CommandAfterElseIf.into()
            })?;
        let context = ScopeCondition::new(matched.location(), command);
        Ok(context)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn else_clause(&mut self, matched: MatchedElse) -> ParsingResult<ElseContext> {
        let matched: IonToken = matched.into();
        self.check_for_trailing_comment(|| NoEosAfter::Else.into())?;
        Ok(ElseContext::new(matched.location()))
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn while_clause(&mut self, matched: MatchedWhile) -> ParsingResult<EnterScope> {
        let matched: IonToken = matched.into();
        let command =
            self.parse_for_command_chain(&matched, MissingCommandFor::While.into(), |_| {
                InvalidError::CommandAfterWhile.into()
            })?;
        let context = EnterScope::While(ScopeCondition::new(matched.location(), command));
        Ok(context)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn match_clause(&mut self, matched_start: MatchedMatch) -> ParsingResult<Match> {
        let start_of_value = self.progress_skip_horz_bail_if_eos_or_eoi(&matched_start, || {
            MissingValueFor::Match.into()
        })?;
        let value = self.value(start_of_value)?;
        self.check_for_trailing_comment(|| NoEosAfter::Match.into())?;
        let matched_context = Match::new(matched_start.location(), value);
        Ok(matched_context)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn case_clause(&mut self, matched_start: MatchedCase) -> ParsingResult<Case> {
        let start_of_value = self.progress_skip_horz_bail_if_eos_or_eoi(&matched_start, || {
            MissingValueFor::Case.into()
        })?;
        let value = self.value(start_of_value)?;
        let if_guard: Option<IfGuard> = match self.skip_horz_noise() {
            SkippedToNext::EndOfStatement(_) | SkippedToNext::Eoi => None,
            SkippedToNext::Next(next) if next.kind() == TokenKind::If => {
                let command_chain =
                    self.parse_for_command_chain(&next, MissingCommandFor::IfGuard.into(), |_| {
                        InvalidError::Command.into()
                    })?;
                self.check_for_trailing_comment(|| NoEosAfter::Case.into())?;
                let scope_condition = ScopeCondition::new(next.location(), command_chain);
                let if_guard = IfGuard::new(scope_condition);
                Some(if_guard)
            }
            SkippedToNext::Next(to_push_back) => {
                self.push_back(to_push_back);
                self.check_for_trailing_comment(|| NoEosAfter::Case.into())?;
                None
            }
        };
        let case_context = Case::new(matched_start.location(), value, if_guard);
        Ok(case_context)
    }

    pub fn for_loop(&mut self, matched_start: MatchedFor) -> ParsingResult<ForLoopContext> {
        let var_names: Vec<VariableName> = self.parse_for_variable_names(&matched_start)?;

        let source = self.parse_values_in_for(&matched_start, &var_names)?;

        let source_rangen = whole_range_from_seq(&source).unwrap();
        let range = Range::from_ranges_internal(&matched_start, &source_rangen);
        self.check_for_trailing_comment(|| NoEosAfter::For.into())?;
        let for_context = ForLoopContext::new(range, var_names, source);

        Ok(for_context)
    }

    fn parse_for_variable_names(
        &mut self,
        matched_start: &MatchedFor,
    ) -> ParsingResult<Vec<VariableName>> {
        let mut var_names: Vec<VariableName> = Default::default();
        let mut encountered_first_value = false;
        loop {
            let next = self.progress_skip_horz_bail_if_eos_or_eoi(matched_start, || {
                if encountered_first_value {
                    MissingError::VariableName.into()
                } else {
                    MissingError::InKeyword.into()
                }
            })?;
            if next.kind() == TokenKind::In {
                return Ok(var_names);
            }
            let variable_name = self.validate_as_ident(next)?;
            var_names.push(variable_name);
            encountered_first_value = true;
        }
    }

    fn parse_values_in_for(
        &mut self,
        matched_start: &MatchedFor,
        var_names: &[VariableName],
    ) -> ParsingResult<Vec<ValueContext>> {
        let value = self.progress_skip_horz_bail_if_eos_or_eoi(matched_start, || {
            let range = whole_range_from_seq(var_names)
                .expect("Sequence is not empty otherwise a parsing error would have occurred.");
            MissingValueFor::For(range).into()
        })?;
        let first_value = self.value(value)?;
        let mut values = vec![first_value];
        loop {
            match self.skip_horz_noise() {
                SkippedToNext::Eoi => return Ok(values),
                SkippedToNext::EndOfStatement(eos) => {
                    self.push_back(eos);
                    return Ok(values);
                }
                SkippedToNext::Next(next) => {
                    if next.kind().is_comment() {
                        self.push_back(next);
                        return Ok(values);
                    } else {
                        let next_value = self.value(next)?;
                        values.push(next_value);
                    }
                }
            }
        }
    }

    fn parse_for_command_chain(
        &mut self,
        matched: &IonToken,
        on_eoi_or_eoi_err: ParsingErrorKind,
        on_illegal: impl Fn(Range) -> ParsingErrorKind,
    ) -> ParsingResult<CommandChain> {
        let might_be_command = self
            .skip_horz_noise()
            .match_to_command(self)
            .map_err(|error| match error {
                NoCommand::Eoi => {
                    let err_range = ToRange::new((matched.location(), self.last_location())).into();
                    ParsingError::new(err_range, on_eoi_or_eoi_err)
                }
                NoCommand::EndOfStatement(eoi) => {
                    let err_range = ToRange::new((matched.location(), eoi.location())).into();
                    ParsingError::new(err_range, on_eoi_or_eoi_err)
                        .fallback_recoverable_from_next_token_preserved()
                }
                NoCommand::IllegalOperator(illegal) => {
                    let illegal_range = illegal.location();
                    ParsingError::new(illegal_range, on_illegal(illegal_range))
                        .fallback_recoverable_from_next_eos()
                }
            })?;

        self.command_chain(might_be_command)
    }
}

#[cfg(test)]
mod testing {
    use crate::parsing::test_utils;

    #[test]
    fn parse_for_loop() {
        const INPUT: &str = include_str!("input_files_for_tests/for_loop.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_ifs_with_correct_scopes() {
        const INPUT: &str = include_str!("input_files_for_tests/nested_ifs.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_end_without_if_start() {
        const INPUT: &str = include_str!("input_files_for_tests/end_block_without_scope_enter.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_else_without_if() {
        const INPUT: &str = include_str!("input_files_for_tests/else_without_if.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_duplicate_else() {
        const INPUT: &str = include_str!("input_files_for_tests/duplicate_else.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_if_elseif_else() {
        const INPUT: &str = include_str!("input_files_for_tests/if_elseif_else.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_detect_else_if_without_previous() {
        const INPUT: &str = include_str!("input_files_for_tests/else_if_without_previous_if.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_missing_end_for_if() {
        const INPUT: &str = include_str!("input_files_for_tests/missing_end_for_second_if.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_while_loop() {
        const INPUT: &str = include_str!("input_files_for_tests/while_loop.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_errors_in_while_loop() {
        const INPUT: &str = include_str!("input_files_for_tests/while_loops_with_errors.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_errors_in_for_loop() {
        const INPUT: &str = include_str!("input_files_for_tests/errors_in_for_loop.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_detect_errors_in_matches() {
        const INPUT: &str = include_str!("input_files_for_tests/match_with_errors.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_simple_match() {
        const INPUT: &str = include_str!("input_files_for_tests/simple_match.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_match_case_if_guard() {
        const INPUT: &str = include_str!("input_files_for_tests/match_case_if_guard.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_error_trailing_piece_after_condition() {
        const INPUT: &str =
            include_str!("input_files_for_tests/trailing_error_after_condition.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_chunked_iterations() {
        const INPUT: &str = include_str!("input_files_for_tests/chunked_iterations.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_break_continue() {
        const INPUT: &str = include_str!("input_files_for_tests/break_continue.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_break_continue_error() {
        const INPUT: &str = include_str!("input_files_for_tests/break_continue_error.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }
}
