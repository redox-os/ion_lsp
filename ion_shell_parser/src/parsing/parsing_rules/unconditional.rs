mod commands;
mod function_declaration;
mod validation_builins;

use crate::{
    parsing::{
        internal_prelude::*,
        language_items::{
            assignment::{AssignmentContext, ExportContext},
            variables::{TypeAnnotationContext, VariableDeclaration},
        },
        matched_fragments::{MatchedExport, MatchedLet},
    },
    tokens::AssignmentOpKind,
};

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn export_declaration(
        &mut self,
        export_token: MatchedExport,
    ) -> ParsingResult<ExportContext> {
        let variable_name = self.expect_variable_name(&export_token)?;
        let export_range = export_token.location();

        let next = match self.skip_horz_noise() {
            SkippedToNext::Eoi | SkippedToNext::EndOfStatement(_) => {
                return Ok(ExportContext::new(export_range, variable_name, None))
            }
            SkippedToNext::Next(next) => next,
        };

        if next.kind() != TokenKind::AssignmentOp(AssignmentOpKind::Equal) {
            return Err(ParsingError::new(
                next.location(),
                IsNotError::EqualAssignment.into(),
            ));
        }

        let next = self.progress_skip_horz_bail_if_eos_or_eoi(&export_token, || {
            MissingValueFor::Variable(variable_name.location()).into()
        })?;

        let value = self.value(next)?;
        self.check_for_trailing_comment(|| ParsingErrorKind::TooManyValuesForAssignment)?;
        let context = ExportContext::new(export_range, variable_name, Some(value));
        Ok(context)
    }

    #[cfg_attr(
        feature = "ion_shell_par
        ser_trace_
       parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn let_statement(&mut self, let_token: MatchedLet) -> ParsingResult<LanguageItem> {
        let (op, variables) = self.parse_variables_up_to_assignment(&let_token)?;
        let let_token: IonToken = let_token.into();

        let up_to = variables.len();
        let mut values: Vec<ValueContext> = Vec::with_capacity(up_to);
        for variable in variables.iter() {
            let next_token = self.progress_skip_horz_bail_if_eos_or_eoi(variable, || {
                let location = variable.location();
                MissingValueFor::Variable(location).into()
            })?;

            let value = self.value(next_token)?;
            values.push(value);
        }
        self.check_for_trailing_comment(|| ParsingErrorKind::TooManyValuesForAssignment)?;
        let variable_assignment =
            AssignmentContext::new(let_token.location(), variables, op, values);
        let item = LanguageItem::Assigmnet(variable_assignment);
        Ok(item)
    }

    #[cfg_attr(
        feature = "ion_shell_par
        ser_trace_
       parse_phase",
        tracing::instrument(skip(self))
    )]
    fn parse_variables_up_to_assignment(
        &mut self,
        let_token: &MatchedLet,
    ) -> ParsingResult<(AssignmentOpKind, Vec<VariableDeclaration>)> {
        fn next_variable<T>(
            fetcher: &mut TokenFetcherForParsing<T>,
            let_token: &MatchedLet,
        ) -> ParsingResult<(Option<AssignmentOpKind>, VariableDeclaration)>
        where
            T: Iterator<Item = IonToken>,
        {
            let variable_name = fetcher.expect_variable_name(let_token)?;
            let assignment_or_type_annotation = fetcher
                .progress_skip_horz_bail_if_eos_or_eoi(&variable_name, || {
                    MissingError::Assignment.into()
                })?;
            match assignment_or_type_annotation.kind() {
                TokenKind::AssignmentOp(op) => {
                    Ok((Some(op), VariableDeclaration::new(variable_name, None)))
                }
                TokenKind::TypeIdent(ident) => {
                    let type_context =
                        TypeAnnotationContext::new(assignment_or_type_annotation.location(), ident);
                    let var_declaration =
                        VariableDeclaration::new(variable_name, Some(type_context));
                    Ok((None, var_declaration))
                }
                _ => {
                    fetcher.push_back(assignment_or_type_annotation);
                    Ok((None, VariableDeclaration::new(variable_name, None)))
                }
            }
        }

        let mut variable_names: Vec<VariableDeclaration> = Default::default();

        loop {
            match next_variable(self, let_token)? {
                (None, next) => variable_names.push(next),
                (Some(op), next) => {
                    variable_names.push(next);
                    return Ok((op, variable_names));
                }
            }

            let might_be_assignment = self
                .progress_skip_horz_bail_if_eos_or_eoi(let_token, || {
                    MissingError::Assignment.into()
                })?;
            if let TokenKind::AssignmentOp(op) = might_be_assignment.kind() {
                return Ok((op, variable_names));
            } else {
                self.push_back(might_be_assignment);
            }
        }
    }

    pub fn check_for_trailing_comment(
        &mut self,
        on_err_kind: impl Fn() -> ParsingErrorKind,
    ) -> ParsingResult<()> {
        match self.skip_horz_noise() {
            SkippedToNext::EndOfStatement(_) => (),
            SkippedToNext::Next(next) if next.kind() == TokenKind::Hash => self.push_back(next),
            SkippedToNext::Next(no_comment) => {
                let error = ParsingError::new(no_comment.location(), on_err_kind());
                self.push_back(no_comment);
                return Err(error.recoverable_from_next_eos());
            }
            SkippedToNext::Eoi => (),
        }
        Ok(())
    }
}

mod unconditional_fragments {
    use crate::{
        parsing::language_items::{
            command_operators::FileRedirect,
            commands::{ChainLogicalOperatorContext, PipeOperatorContext},
            ValueContext,
        },
        prelude::commands::DetachOperatorContext,
        Range,
    };

    #[derive(Debug, Clone, Copy)]
    pub struct FileRedirectToken {
        location: Range,
        redirection: FileRedirect,
    }

    impl FileRedirectToken {
        pub fn new(location: Range, redirection: FileRedirect) -> Self {
            Self {
                location,
                redirection,
            }
        }

        pub fn location(&self) -> Range {
            self.location
        }

        pub fn redirection(&self) -> FileRedirect {
            self.redirection
        }
    }

    #[derive(Debug)]
    pub enum CommandValue {
        None,
        Detach(DetachOperatorContext),
        PipeOp(PipeOperatorContext),
        Logical(ChainLogicalOperatorContext),
        Value(ValueContext),
    }

    #[derive(Debug)]
    pub enum CommandChainOperator {
        Logical(ChainLogicalOperatorContext),
        Detach(DetachOperatorContext),
        Pipe(PipeOperatorContext),
    }
}

#[cfg(test)]
mod testing {
    use crate::{parsing::test_utils, IonToken, IonTokenizer};

    #[test]
    fn tokens_multiple_assignment() {
        let input = "let a:str b:[str] c:int d:[float] = one [two three] 4 [5.1 6.2 7.3]";
        let actual: Vec<IonToken> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_simple_if_else() {
        const INPUT: &str = include_str!("input_files_for_tests/simple_if_else.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_detect_too_many_arguments() {
        const INPUT: &str = "source test.ion 22 \n\
            let a = hello";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_detect_missing_argument_for_source() {
        const INPUT: &str = "source \n\
            && echo \n\
            let a = hello";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_source_statement() {
        const INPUT: &str = "source test.ion";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_string_val_as_command() {
        const INPUT: &str = "echo && \"aaa\"\n\
                echo $name";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_missing_arg_for_or_over_more_than_one_line() {
        const INPUT: &str = "echo aaa && a22adfasdf ||\n\
            \n\
            asdf dsfa sdfas";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_command_chained_over_lines() {
        const INPUT: &str =
            include_str!("input_files_for_tests/command_logical_chaining_over_lines.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_detect_missing_command_after_and_op() {
        const INPUT: &str = "test true &&";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_command_and_operator_logical_piping_redirection() {
        const INPUT: &str = include_str!("input_files_for_tests/logical_piping_redirection.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_command_and_operator_changing() {
        const INPUT: &str = "test `1 == 1 && echo \"1 equals 1\" || echo error404.24";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_command_expression_with_file_redirections() {
        const INPUT: &str = "some_app -d < text.txt > out.txt";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_file_redirect_before_arg_for_command_expression() {
        const INPUT: &str = "some_app < text.txt -d";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_file_redirect_with_redirect() {
        const INPUT: &str = "some_app --debug true >> text.txt";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_error_missing_arg_for_redirect() {
        const INPUT: &str = "some_app --debug true >>\n\
            export var";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_file_director_args_with_weird_values() {
        // Ion shell allows file names like "22" or "=="
        const INPUT: &str = "some_app >> 22 < ==";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_all_possible_redirections() {
        const INPUT: &str = include_str!("input_files_for_tests/all_redirections.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_piping() {
        const INPUT: &str = "seq 2 10 | sort > out.txt";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_piping_stderr_and_both() {
        const INPUT: &str = "seq \"2\" true ^| sort > out.txt &| cat";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_missing_val_for_var_and_end_of_statement_after_pipe() {
        const INPUT: &str = "let a = \necho $a | \n let right = true";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_missing_variable_name_and_missing_assignment() {
        const INPUT: &str =
            "let \nlet dsaf\n world_exe 2 > out.txt ^| audit_error_messages > err.log";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_function() {
        const INPUT: &str = include_str!("input_files_for_tests/function.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_function_with_args() {
        const INPUT: &str = include_str!("input_files_for_tests/function_with_args.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_function_with_docstring() {
        const INPUT: &str = include_str!("input_files_for_tests/function_with_docstring.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_epxort_with_value_assignment() {
        const INPUT: &str = "export GLOBAL_VAL = \"this\"";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_functrion_with_annotation() {
        const INPUT: &str = include_str!("input_files_for_tests/function_with_annotation.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_error_type_annotation() {
        const INPUT: &str = include_str!("input_files_for_tests/error_type_annotation.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_multiple_assignment() {
        const INPUT: &str = include_str!("input_files_for_tests/multiple_assignments.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_array_concatenation_and_variable_stripping() {
        const INPUT: &str =
            include_str!("input_files_for_tests/array_concatenation_variable_stripping.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }
}
