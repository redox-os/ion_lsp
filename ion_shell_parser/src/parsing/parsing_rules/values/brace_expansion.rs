use crate::{
    parsing::{internal_prelude::*, matched_fragments::MatchedOpenCurlyBracket},
    prelude::{BraceExpansionComponent, BraceExpansionContext, BraceSliceExpansion, SliceKind},
};

use super::FoundSliceIndexKind;

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn brace_expansion(
        &mut self,
        start: MatchedOpenCurlyBracket,
    ) -> ParsingResult<(Range, BraceExpansionContext)> {
        let mut elements: Vec<BraceExpansionComponent> = Default::default();

        loop {
            let next = self.progress_skip_bail_if_eos_or_eoi(&start, || {
                MissingError::ClosedCurlyBracket.into()
            })?;
            match self.value_up_to_delimiter(next, |kind| {
                matches!(
                    kind,
                    TokenKind::Comma | TokenKind::ClosedCurlyBracket | TokenKind::DoublePoint
                )
            })? {
                (next_value, None) => {
                    let might_be_closing = self
                        .progress_skip_horz_bail_if_eos_or_eoi(&start, || {
                            MissingError::ClosedCurlyBracket.into()
                        })?;
                    if might_be_closing.kind() == TokenKind::ClosedCurlyBracket {
                        let next_value = BraceExpansionComponent::Value(next_value);
                        elements.push(next_value);
                        return Ok(create_ok(&start, &might_be_closing, elements));
                    } else {
                        let range = Range::from_ranges_internal(&start, &might_be_closing);

                        return Err(ParsingError::new(
                            range,
                            MissingError::ClosedCurlyBracket.into(),
                        ));
                    }
                }
                (next_value, Some(end)) => match end.kind() {
                    TokenKind::Comma => {
                        let next_value = BraceExpansionComponent::Value(next_value);
                        elements.push(next_value);
                    }
                    TokenKind::ClosedCurlyBracket => {
                        let next_value = BraceExpansionComponent::Value(next_value);
                        elements.push(next_value);
                        return Ok(create_ok(&start, &end, elements));
                    }
                    _ => match self.slice_index_kind(&start, end)? {
                        FoundSliceIndexKind::None => {
                            return Err(ParsingError::new(
                                Range::from_start_end_internal(
                                    start.location(),
                                    self.last_location(),
                                ),
                                MissingError::DoublePoint.into(),
                            ))
                        }
                        FoundSliceIndexKind::Inclusive { .. } => {
                            let next = self
                                .progress_skip_horz_bail_if_eos_or_eoi(&start, || {
                                    MissingValueFor::SecondIndex.into()
                                })?;
                            if let Some(whole_range) = self.parse_second_index_for_slice(
                                &start,
                                next,
                                next_value,
                                SliceKind::Inclusive,
                                &mut elements,
                            )? {
                                let item = BraceExpansionContext::new(elements);
                                return Ok((whole_range, item));
                            }
                        }
                        FoundSliceIndexKind::Exclusive {
                            after_double_point, ..
                        } => {
                            let next = after_double_point;
                            if let Some(whole_range) = self.parse_second_index_for_slice(
                                &start,
                                next,
                                next_value,
                                SliceKind::Exclusive,
                                &mut elements,
                            )? {
                                let item = BraceExpansionContext::new(elements);
                                return Ok((whole_range, item));
                            }
                        }
                    },
                },
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    fn parse_second_index_for_slice<S>(
        &mut self,
        start: &S,
        next: IonToken,
        first_index: ValueContext,
        slice_kind: SliceKind,
        elements: &mut Vec<BraceExpansionComponent>,
    ) -> ParsingResult<Option<Range>>
    where
        S: HasLocation + std::fmt::Debug,
    {
        let (second_index, delimiter) = self.value_up_to_delimiter(next, |kind| {
            matches!(kind, TokenKind::Comma | TokenKind::ClosedCurlyBracket)
        })?;
        let range = Range::from_ranges_internal(&first_index, &second_index);
        let next_value = BraceExpansionComponent::SliceGenerator(BraceSliceExpansion::new(
            range,
            slice_kind,
            first_index,
            second_index,
        ));
        elements.push(next_value);

        let to_return = if let Some(end) = delimiter {
            match end.kind() {
                TokenKind::ClosedCurlyBracket => Some(Range::from_ranges_internal(start, &end)),
                TokenKind::Comma => None,
                _ => unreachable!(),
            }
        } else {
            let range = Range::from_ranges_internal(start, &self.last_location());
            let error = ParsingError::new(range, MissingError::ClosedCurlyBracket.into());
            return Err(error);
        };
        Ok(to_return)
    }
}

fn create_ok(
    start: &impl HasLocation,
    end: &impl HasLocation,
    elements: Vec<BraceExpansionComponent>,
) -> (Range, BraceExpansionContext) {
    let whole_range = Range::from_ranges_internal(start, end);
    let item = BraceExpansionContext::new(elements);
    (whole_range, item)
}
