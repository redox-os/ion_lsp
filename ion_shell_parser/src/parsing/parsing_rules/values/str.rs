use crate::{
    parsing::{
        internal_prelude::*,
        language_items::{
            variables::VariableName, ComplexQuotedStr, IndexAccessContext, QuotedStr,
            QuotedStrComponent, Value, ValueContext,
        },
        matched_fragments::{
            MatchedOpenSquareBracket, MatchedQuotationMark, MatchedQuotationToken, MatchedSigil,
            SigilKind,
        },
        parsing_error::UnmathcedStrError,
        EnclosedRange,
    },
    prelude::variables::{VariableReference, VariableReferenceContext},
};

pub type QuotedStrResult<T = ValueContext> = Result<T, UnmathcedStrError>;

enum AfterStrVarParse {
    NoVar,
    Again(MatchedSigil),
    Return(Range),
}
impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn str<Q>(&mut self, quotation: Q) -> QuotedStrResult
    where
        Q: MatchedQuotationMark,
    {
        match self.str_simple_range(&quotation)? {
            (inner, SigilOrMatchingQuotation::End(end)) => {
                let whole_range = Range::from_ranges_internal(&quotation, &end);
                Ok(ValueContext::new(
                    whole_range,
                    Value::QuotedStr(QuotedStr::Simple(inner.map(RangeDebugText::from))),
                ))
            }
            (inner, SigilOrMatchingQuotation::Sigil(sigil)) => {
                let mut components: Vec<QuotedStrComponent> = inner
                    .map(|e| vec![QuotedStrComponent::Range(e.into())])
                    .unwrap_or(Vec::default());
                let mut warnings: Vec<ParsingError> = Default::default();
                let mut parse_var: Option<MatchedSigil> = Some(sigil);
                loop {
                    if let Some(previous_sigil) = parse_var {
                        parse_var = match self.on_found_sigil(
                            &quotation,
                            previous_sigil,
                            &mut components,
                            &mut warnings,
                        )? {
                            AfterStrVarParse::NoVar => None,
                            AfterStrVarParse::Again(again) => Some(again),
                            AfterStrVarParse::Return(whole_range) => {
                                return Ok(create_complex_str_value(
                                    whole_range,
                                    components,
                                    warnings,
                                ))
                            }
                        };
                    } else {
                        match self.str_simple_range(&quotation)? {
                            (inner, SigilOrMatchingQuotation::End(end)) => {
                                add_plain(inner, &mut components);
                                let whole_range = Range::from_ranges_internal(&quotation, &end);
                                return Ok(create_complex_str_value(
                                    whole_range,
                                    components,
                                    warnings,
                                ));
                            }
                            (inner, SigilOrMatchingQuotation::Sigil(sigil)) => {
                                add_plain(inner, &mut components);
                                parse_var = Some(sigil);
                            }
                        }
                    }
                }
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    fn str_var(
        &mut self,
        sigil: MatchedSigil,
        quotation: &impl MatchedQuotationMark,
    ) -> QuotedStrResult<(MaybeVariableReference, Option<ParsingError>)> {
        let var_or_opening_bracket = self.next_non_eos_nor_eoi_for_quoted_str(&sigil, quotation)?;
        let (var_name, closing_needed) =
            if var_or_opening_bracket.kind() == TokenKind::OpenCurlyBracket {
                let maybe_var_name = self.next_non_eos_nor_eoi_for_quoted_str(&sigil, quotation)?;
                (maybe_var_name, true)
            } else {
                (var_or_opening_bracket, false)
            };
        let to_return = match self.validate_as_ident(var_name.clone()) {
            Ok(valid_var_name) => match self.next_token() {
                Some(square_bracket) if square_bracket.kind() == TokenKind::OpenSquareBracket => {
                    let square_bracket = MatchedOpenSquareBracket::new(square_bracket);
                    self.match_on_slice_index(
                        square_bracket,
                        closing_needed,
                        quotation,
                        &sigil,
                        valid_var_name,
                    )
                }
                Some(non_square_bracket) => {
                    self.push_back(non_square_bracket);
                    self.check_for_closing_bracket_if_opened_encountered(
                        closing_needed,
                        quotation,
                        &sigil,
                        valid_var_name,
                        None,
                    )
                }
                None => (
                    MaybeVariableReference::Var(create_var_with(&sigil, valid_var_name, None)),
                    None,
                ),
            },
            Err(error) => (
                on_unexpected_token(quotation, &sigil, &var_name),
                Some(error),
            ),
        };
        Ok(to_return)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    fn str_simple_range(
        &mut self,
        quotation: &impl MatchedQuotationMark,
    ) -> QuotedStrResult<(Option<Range>, SigilOrMatchingQuotation)> {
        let mut inner = EnclosedRange::default();
        loop {
            match self.next_token() {
                Some(next_token) => match next_token.kind() {
                    TokenKind::StringSigil => {
                        let inner_range = inner.inner();
                        return Ok((
                            inner_range,
                            SigilOrMatchingQuotation::Sigil(MatchedSigil::new(
                                next_token.location(),
                                SigilKind::String,
                            )),
                        ));
                    }
                    TokenKind::ArraySigil => {
                        let inner_range = inner.inner();
                        return Ok((
                            inner_range,
                            SigilOrMatchingQuotation::Sigil(MatchedSigil::new(
                                next_token.location(),
                                SigilKind::Array,
                            )),
                        ));
                    }
                    might_match if quotation.is_mark(might_match) => {
                        let inner_range = inner.inner();
                        return Ok((
                            inner_range,
                            SigilOrMatchingQuotation::End(MatchedQuotationToken::new(next_token)),
                        ));
                    }
                    might_be_visible_eos if might_be_visible_eos.is_visible_end_of_statement() => {
                        inner.update(next_token.location())
                    }
                    _ => inner.update(next_token.location()),
                },
                None => return Err(quotation.error_kind()),
            }
        }
    }

    fn on_found_sigil(
        &mut self,
        quotation: &impl MatchedQuotationMark,
        previous_sigil: MatchedSigil,
        components: &mut Vec<QuotedStrComponent>,
        warnings: &mut Vec<ParsingError>,
    ) -> QuotedStrResult<AfterStrVarParse> {
        let (variable, warning) = self.str_var(previous_sigil, quotation)?;

        if let Some(warning) = warning {
            warnings.push(warning);
        }

        let ok_value = match variable {
            MaybeVariableReference::Var(var) => {
                components.push(QuotedStrComponent::Variable(var));
                AfterStrVarParse::NoVar
            }
            MaybeVariableReference::Plain(plain) => {
                add_plain(Some(plain), components);
                AfterStrVarParse::NoVar
            }
            MaybeVariableReference::FoundSigil(NoVariableWith { before, matched }) => {
                add_plain(Some(before), components);
                AfterStrVarParse::Again(matched)
            }
            MaybeVariableReference::VarWithInvalidIndex(VariableBeforeInvalidIndex {
                variable,
                invalid_for_index,
                found_end,
            }) => {
                components.push(QuotedStrComponent::Variable(variable));
                add_plain(Some(invalid_for_index), components);
                if let Some(end) = found_end {
                    let whole_range = Range::from_ranges_internal(quotation, &end);
                    AfterStrVarParse::Return(whole_range)
                } else {
                    AfterStrVarParse::NoVar
                }
            }
            MaybeVariableReference::FoundEnd(NoVariableWith { before, matched }) => {
                add_plain(Some(before), components);
                let whole_range = Range::from_ranges_internal(quotation, &matched);
                AfterStrVarParse::Return(whole_range)
            }
        };

        Ok(ok_value)
    }
    fn match_on_slice_index(
        &mut self,
        matched: MatchedOpenSquareBracket,
        closing_needed: bool,
        quotation: &impl MatchedQuotationMark,
        sigil: &MatchedSigil,
        variable: VariableName,
    ) -> (MaybeVariableReference, Option<ParsingError>) {
        let matched_location = matched.location();
        match self.slice_index(matched) {
            Ok(index) => self.check_for_closing_bracket_if_opened_encountered(
                closing_needed,
                quotation,
                sigil,
                variable,
                Some(index),
            ),

            Err(error) => {
                let last_token = self
                    .last_token()
                    .expect("There must be a last token because of a returned parsing error");
                let kind = last_token.kind();
                let (invalid_for_index, found_end) = if quotation.is_mark(kind) || kind.is_sigil() {
                    let end = last_token.location();
                    let end_pos = end.start_pos();
                    (
                        Range::new_internal(matched_location.start_pos(), end_pos),
                        Some(end),
                    )
                } else {
                    (Range::from_ranges_internal(&matched_location, &error), None)
                };
                let variable = create_var_with(sigil, variable, None);

                let variable =
                    MaybeVariableReference::VarWithInvalidIndex(VariableBeforeInvalidIndex {
                        variable,
                        invalid_for_index,
                        found_end,
                    });
                (variable, Some(error))
            }
        }
    }
    fn check_for_closing_bracket_if_opened_encountered(
        &mut self,
        needs_to_check: bool,
        quoation: &impl MatchedQuotationMark,
        sigil: &MatchedSigil,
        valid_var_name: VariableName,
        index: Option<IndexAccessContext>,
    ) -> (MaybeVariableReference, Option<ParsingError>) {
        if needs_to_check {
            self.next_token()
                .map(|token| {
                    let end = token.location();
                    let whole = Range::from_ranges_internal(sigil, &end);
                    if token.kind() == TokenKind::ClosedCurlyBracket {
                        let reference =
                            VariableReference::new(valid_var_name, sigil.kind(), None, index);
                        let context = VariableReferenceContext::new(whole, reference);
                        (MaybeVariableReference::Var(context), None)
                    } else {
                        let warning =
                            ParsingError::new(whole, MissingError::ClosedCurlyBracket.into());
                        let not_variable_reference = on_unexpected_token(quoation, sigil, &token);
                        (not_variable_reference, Some(warning))
                    }
                })
                .unwrap_or_else(|| {
                    let whole = Range::from_ranges_internal(sigil, &self.last_location());
                    let warning = ParsingError::new(whole, MissingError::ClosedCurlyBracket.into());
                    let plain = MaybeVariableReference::Plain(Range::from_ranges_internal(
                        sigil,
                        &self.last_location(),
                    ));
                    (plain, Some(warning))
                })
        } else {
            let context = create_var_with(sigil, valid_var_name, index);
            (MaybeVariableReference::Var(context), None)
        }
    }

    fn next_non_eos_nor_eoi_for_quoted_str(
        &mut self,
        sigil: &MatchedSigil,
        quotation: &impl MatchedQuotationMark,
    ) -> QuotedStrResult<IonToken> {
        self.progress_skip_horz_bail_if_eos_or_eoi(sigil, || quotation.error_kind().into())
            .map_err(|_| quotation.error_kind())
    }
}

fn create_var_with(
    sigil: &MatchedSigil,
    valid_var_name: VariableName,
    index: Option<IndexAccessContext>,
) -> VariableReferenceContext {
    let end = index
        .as_ref()
        .map(|element| element.location())
        .unwrap_or_else(|| valid_var_name.location());
    let whole = Range::from_ranges_internal(sigil, &end);
    let reference = VariableReference::new(valid_var_name, sigil.kind(), None, index);
    VariableReferenceContext::new(whole, reference)
}

fn on_unexpected_token(
    quoatation: &impl MatchedQuotationMark,
    sigil: &MatchedSigil,
    token: &IonToken,
) -> MaybeVariableReference {
    fn on_return_found_sigil(
        sigil: &MatchedSigil,
        token: &IonToken,
        kind: SigilKind,
    ) -> MaybeVariableReference {
        MaybeVariableReference::FoundSigil(NoVariableWith {
            before: before_invalid_token(sigil, token),
            matched: MatchedSigil::new(token.location(), kind),
        })
    }
    fn before_invalid_token(sigil: &MatchedSigil, token: &IonToken) -> Range {
        Range::new_internal(sigil.location().start_pos(), token.location().start_pos())
    }

    let kind = token.kind();
    if quoatation.is_mark(kind) {
        MaybeVariableReference::FoundEnd(NoVariableWith {
            before: before_invalid_token(sigil, token),
            matched: MatchedQuotationToken::new(token.clone()),
        })
    } else if kind == TokenKind::StringSigil {
        on_return_found_sigil(sigil, token, SigilKind::String)
    } else if kind == TokenKind::ArraySigil {
        on_return_found_sigil(sigil, token, SigilKind::Array)
    } else {
        MaybeVariableReference::Plain(Range::from_ranges_internal(sigil, token))
    }
}

fn add_plain(plain: Option<Range>, add_to: &mut Vec<QuotedStrComponent>) {
    match (plain, add_to.last_mut()) {
        (Some(plain), Some(QuotedStrComponent::Range(range))) => {
            let new_range = Range::from_start_end_internal(range.location(), plain);
            *range = new_range.into();
        }
        (Some(plain), _) => add_to.push(QuotedStrComponent::Range(plain.into())),
        (None, _) => (),
    }
}

fn create_complex_str_value(
    whole_range: Range,
    components: Vec<QuotedStrComponent>,
    warnings: Vec<ParsingError>,
) -> ValueContext {
    ValueContext::new(
        whole_range,
        Value::QuotedStr(QuotedStr::Complex(ComplexQuotedStr::new(
            components, warnings,
        ))),
    )
}

#[derive(Debug)]
enum SigilOrMatchingQuotation {
    Sigil(MatchedSigil),
    End(MatchedQuotationToken),
}

#[derive(Debug)]
enum MaybeVariableReference {
    Var(VariableReferenceContext),
    VarWithInvalidIndex(VariableBeforeInvalidIndex),
    Plain(Range),
    FoundSigil(NoVariableWith<MatchedSigil>),
    FoundEnd(NoVariableWith<MatchedQuotationToken>),
}

#[derive(Debug)]
struct NoVariableWith<T> {
    pub before: Range,
    pub matched: T,
}

#[derive(Debug)]
struct VariableBeforeInvalidIndex {
    pub variable: VariableReferenceContext,
    pub invalid_for_index: Range,
    pub found_end: Option<Range>,
}
