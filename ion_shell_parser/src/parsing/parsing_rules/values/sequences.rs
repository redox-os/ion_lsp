use crate::{
    parsing::{
        internal_prelude::*,
        language_items::{IndexAccess, IndexAccessContext, IndexKind, SingleIndex, SliceIndex},
        matched_fragments::MatchedOpenSquareBracket,
        parsing_rules::values::FoundSliceIndexKind,
    },
    prelude::SliceKind,
    NumberValue,
};

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self, on_missing_error))
    )]
    pub fn values_up_to_token(
        &mut self,
        start_token: IonToken,
        delimiter: TokenKind,
        on_missing_error: impl Fn() -> ParsingErrorKind,
    ) -> ParsingResult<(Vec<ValueContext>, Range)> {
        enum NextItem {
            End(IonToken),
            LastValue(ValueContext, IonToken),
            Value(ValueContext),
        }

        fn next_array_value<T>(
            fetcher: &mut TokenFetcherForParsing<T>,
            start: &IonToken,
            delimiter: TokenKind,
            on_missing_error: impl Fn() -> ParsingErrorKind,
        ) -> ParsingResult<NextItem>
        where
            T: Iterator<Item = IonToken>,
        {
            let next = fetcher.progress_skip_bail_if_eos_or_eoi(start, on_missing_error)?;
            if next.kind() == delimiter {
                Ok(NextItem::End(next))
            } else {
                fetcher
                    .value_up_to_delimiter(next, |kind| kind == delimiter)
                    .map(|to_map| match to_map {
                        (value, None) => NextItem::Value(value),
                        (value, Some(delimiter)) => NextItem::LastValue(value, delimiter),
                    })
            }
        }
        let mut values: Vec<ValueContext> = Vec::default();

        loop {
            match next_array_value(self, &start_token, delimiter, &on_missing_error)? {
                NextItem::LastValue(value, end) => {
                    values.push(value);
                    let whole_range: Range =
                        ToRange::new((start_token.location(), end.location())).into();
                    return Ok((values, whole_range));
                }
                NextItem::Value(value) => values.push(value),
                NextItem::End(token) => {
                    let whole_range: Range =
                        ToRange::new((start_token.location(), token.location())).into();
                    return Ok((values, whole_range));
                }
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn single_index(&mut self, token: IonToken) -> ParsingResult<SingleIndex> {
        match token.kind() {
            TokenKind::Word => {
                let access = SingleIndex::new(token.location(), IndexKind::Text);
                Ok(access)
            }
            TokenKind::Number(NumberValue::Int) => {
                let index_location = token.location();
                let index_parsed: usize =
                    crate::get_string_from_input(self.input(), index_location)
                        .parse()
                        .map_err(|_| {
                            ParsingError::new(
                                index_location,
                                InvalidError::RepresentableUnsignedSize.into(),
                            )
                        })?;
                let access = SingleIndex::new(index_location, IndexKind::Numeric(index_parsed));
                Ok(access)
            }
            TokenKind::StringSigil => {
                let var_name = self.expect_variable_name(&token)?;
                let index_location =
                    Range::from_start_end_internal(token.location(), var_name.location());
                let access = SingleIndex::new(index_location, IndexKind::TextVarRef(var_name));

                Ok(access)
            }
            _ => Err(ParsingError::new(
                token.location(),
                InvalidError::Index.into(),
            )),
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn slice_index(
        &mut self,
        start_token: MatchedOpenSquareBracket,
    ) -> ParsingResult<IndexAccessContext> {
        fn parse_second_index<T>(
            fetcher: &mut TokenFetcherForParsing<T>,
            start_token: MatchedOpenSquareBracket,
            second_index: IonToken,
            first_index: SingleIndex,
            slice_kind: SliceKind,
        ) -> ParsingResult<IndexAccessContext>
        where
            T: Iterator<Item = IonToken>,
        {
            let second_index = fetcher.single_index(second_index)?;
            let closed_bracket = fetcher.progress_disallow_noise(&start_token, || {
                MissingError::ClosedBracketSquare.into()
            })?;
            if closed_bracket.kind() != TokenKind::ClosedSquareBracket {
                return Err(ParsingError::new(
                    closed_bracket.location(),
                    IsNotError::ClosingSquareBracket.into(),
                ));
            }
            let range =
                Range::from_start_end_internal(start_token.location(), closed_bracket.location());
            let slice = SliceIndex::new(first_index, second_index, slice_kind);
            let access = IndexAccess::Slice(slice);
            let context = IndexAccessContext::new(range, access);
            Ok(context)
        }

        fn create_index_access_context(
            open_square_bracket: MatchedOpenSquareBracket,
            closed_square_bracket: IonToken,
            index: IndexAccess,
        ) -> IndexAccessContext {
            let range = Range::from_ranges_internal(&open_square_bracket, &closed_square_bracket);
            IndexAccessContext::new(range, index)
        }

        let first_index =
            self.progress_disallow_noise(&start_token, || MissingError::Index.into())?;

        let first_index = self.single_index(first_index)?;

        let double_point = self
            .progress_disallow_noise(&start_token, || MissingError::ClosedBracketSquare.into())?;

        let double_point_loc = double_point.location();
        match double_point.kind() {
            TokenKind::ClosedSquareBracket => Ok(create_index_access_context(
                start_token,
                double_point,
                IndexAccess::Index(first_index),
            )),
            _ => match self.slice_index_kind(&start_token, double_point)? {
                FoundSliceIndexKind::None => Err(ParsingError::new(
                    double_point_loc,
                    IsNotError::ClosingSquareBracket.into(),
                )),
                FoundSliceIndexKind::Inclusive => {
                    let second_index = self.progress_disallow_noise(&start_token, || {
                        MissingError::NextIndexOrEqual.into()
                    })?;
                    parse_second_index(
                        self,
                        start_token,
                        second_index,
                        first_index,
                        SliceKind::Inclusive,
                    )
                }
                FoundSliceIndexKind::Exclusive { after_double_point } => parse_second_index(
                    self,
                    start_token,
                    after_double_point,
                    first_index,
                    SliceKind::Exclusive,
                ),
            },
        }
    }
}
