use crate::{
    parsing::{
        self,
        internal_prelude::*,
        language_items::{variables::VariableName, IndexAccessContext},
        matched_fragments::{
            MatchedOpenParanteseCurlyBracket, MatchedOpenSquareBracket, MatchedSigil,
        },
    },
    prelude::variables::{NamespaceContext, VariableReference, VariableReferenceContext},
};

use super::ValueStartingWithSigil;

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub(crate) fn variable_reference(
        &mut self,
        sigil: MatchedSigil,
    ) -> ParsingResult<ValueStartingWithSigil> {
        let may_be_delimiter = self.next_token().ok_or_else(|| {
            ParsingError::new(sigil.location(), MissingError::VariableName.into())
        })?;

        let needs_closing_culry_bracket = if may_be_delimiter.kind() == TokenKind::OpenCurlyBracket
        {
            true
        } else {
            self.push_back(may_be_delimiter);
            false
        };

        let namespace = self.namespace(needs_closing_culry_bracket, &sigil)?;
        let var_name = self.expect_variable_name(&sigil)?;

        let index: Option<IndexAccessContext> = match self.next_token() {
            Some(next) => match next.kind() {
                TokenKind::OpenSquareBracket => {
                    let index = self.slice_index(MatchedOpenSquareBracket::new(next))?;
                    Ok(Some(index))
                }
                TokenKind::OpenParanteseCurlyBracket => {
                    let context =
                        self.methods(sigil, var_name, MatchedOpenParanteseCurlyBracket::new(next))?;
                    return Ok(ValueStartingWithSigil::Method(context));
                }
                _ => {
                    self.push_back(next);
                    Ok(None)
                }
            },
            None => Ok(None),
        }?;

        let end_range = if needs_closing_culry_bracket {
            let next = self
                .progress_bail_if_eos_or_eoi(&sigil, || MissingError::ClosedBracketSquare.into())?;
            if next.kind() == TokenKind::ClosedCurlyBracket {
                Ok(next.location())
            } else {
                let end_range = index
                    .as_ref()
                    .map(|e| e.location())
                    .unwrap_or(var_name.location());
                let range = Range::from_ranges_internal(&sigil, &end_range);
                Err(ParsingError::new(
                    range,
                    MissingError::ClosedCurlyBracket.into(),
                ))
            }
        } else {
            Ok(index
                .as_ref()
                .map(|e| e.location())
                .unwrap_or(var_name.location()))
        }?;

        let range = Range::from_ranges_internal(&sigil, &end_range);
        let reference = VariableReference::new(var_name, sigil.kind(), namespace, index);
        let value = VariableReferenceContext::new(range, reference);
        Ok(ValueStartingWithSigil::VariableReference(value))
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn expect_variable_name<R>(&mut self, start: &R) -> ParsingResult<VariableName>
    where
        R: HasLocation + std::fmt::Debug,
    {
        let next_token = self
            .progress_skip_horz_bail_if_eos_or_eoi(start, || MissingError::VariableName.into())?;

        self.validate_as_ident(next_token)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn validate_as_ident(&mut self, next_token: IonToken) -> ParsingResult<VariableName> {
        let kind = next_token.kind();
        match kind {
            TokenKind::Word => {
                let range = next_token.location();
                let might_be_invalid = crate::get_string_from_input(self.input(), range);
                if parsing::is_valid_variable_name(might_be_invalid) {
                    let var_name = VariableName::new(range);
                    Ok(var_name)
                } else {
                    Err(ParsingError::new(range, InvalidError::VariableName.into()))
                }
            }
            _ => {
                let location = next_token.location();
                Err(ParsingError::new(
                    location,
                    InvalidError::VariableName.into(),
                ))
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    fn namespace(
        &mut self,
        needs_closing_culry_bracket: bool,
        sigil: &MatchedSigil,
    ) -> ParsingResult<Option<NamespaceContext>> {
        let namespace = if needs_closing_culry_bracket {
            let next =
                self.progress_bail_if_eos_or_eoi(sigil, || MissingError::VariableName.into())?;
            match next.kind() {
                TokenKind::Namespace(namespace) => {
                    let range = next.location();
                    let namespace = NamespaceContext::new(range, namespace);
                    Some(namespace)
                }
                _not_a_namespace => {
                    self.push_back(next);
                    None
                }
            }
        } else {
            None
        };
        Ok(namespace)
    }
}

#[cfg(test)]
mod testing {
    use crate::{parsing::test_utils, IonTokenizer, TokenKind};

    #[test]
    fn parse_namespaces() {
        const INPUT: &str = include_str!("input_files_for_tests/namespaces.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn tokens_namespaces() {
        const INPUT: &str = include_str!("input_files_for_tests/namespaces.txt");

        let actual: Vec<TokenKind> = IonTokenizer::new(INPUT).map(|token| token.kind()).collect();
        insta::assert_debug_snapshot!(actual);
    }
}
