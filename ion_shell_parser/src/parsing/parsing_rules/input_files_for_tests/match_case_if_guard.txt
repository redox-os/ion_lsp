let foo = bar
match $string
    case "this" if eq $foo bar
        echo "this and foo = bar"
    case "this"
        echo "this and foo != bar"
    case _ 
        echo "no match found"
end
