mod brace_expansion;
mod sequences;
mod str;

use crate::{
    parsing::{
        self,
        internal_prelude::*,
        language_items::{
            variables::VariableName, CommandExpansion, CommandExpansionKind, MathExpression,
            Method, MethodKind, UnQuoatedStr,
        },
        matched_fragments::{
            MatchedCommandExpansionBegin, MatchedDoubleQuotationMark, MatchedMathBegin,
            MatchedOpenCurlyBracket, MatchedOpenParanteseCurlyBracket, MatchedOpenSquareBracket,
            MatchedSigil, MatchedSingleQuotationMark, SigilKind,
        },
        parsing_rules::edge_cases::NumberValueToken,
        EnclosedRange,
    },
    tokens::AssignmentOpKind,
};

#[derive(Debug)]
pub(crate) enum FoundSliceIndexKind {
    None,
    Inclusive,
    Exclusive { after_double_point: IonToken },
}

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn math_expression(&mut self, start: MatchedMathBegin) -> ParsingResult<ValueContext> {
        let mut inner = EnclosedRange::default();
        loop {
            let next = self.next_token().ok_or_else(|| {
                let range = Range::from_start_end_internal(start.location(), self.last_location());
                ParsingError::new(range, MissingError::EndMathDelimiter.into())
            })?;
            match next.kind() {
                TokenKind::MatchExprEnd => {
                    let range = Range::from_start_end_internal(start.location(), next.location());
                    let inner = inner.inner();
                    let value =
                        ValueContext::new(range, Value::MathExpression(MathExpression::new(inner)));
                    return Ok(value);
                }
                _ => inner.update(next.location()),
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn methods(
        &mut self,
        matched: MatchedSigil,
        variable_name: VariableName,
        matched_opening: MatchedOpenParanteseCurlyBracket,
    ) -> ParsingResult<(Method, Range)> {
        let kind = match matched.kind() {
            SigilKind::String => MethodKind::String,
            SigilKind::Array => MethodKind::Array,
        };
        let (values, whole_range) = self.values_up_to_token(
            matched_opening.into(),
            TokenKind::ClosedParanteseCurlyBracket,
            || MissingError::ClosedParanteseCurlyBracket.into(),
        )?;

        let whole_range = Range::from_start_end_internal(matched.location(), whole_range);
        Ok((Method::new(kind, variable_name, values), whole_range))
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self, on_kind))
    )]
    pub fn process_expansion(
        &mut self,
        matched: MatchedCommandExpansionBegin,
        on_kind: impl Fn() -> CommandExpansionKind,
    ) -> ParsingResult<(CommandExpansion, Range)> {
        let next = self.progress_skip_horz_bail_if_eos_or_eoi(&matched, || {
            MissingCommandFor::ProcessExpansion.into()
        })?;

        let command = match self
            .match_command_delimiter(next, |kind| kind == TokenKind::ClosedParanteseCurlyBracket)?
        {
            (command, Some(end)) => {
                self.push_back(end);
                command
            }
            (command, None) => command,
        };

        let command_chain = self.command_chain(command)?;

        let closed = self.progress_skip_horz_bail_if_eos_or_eoi(&matched, || {
            MissingError::ClosedParanteseCurlyBracket.into()
        })?;
        let closed_range_location = closed.location();
        let whole_range = Range::from_ranges_internal(&matched, &closed_range_location);
        let expansion = CommandExpansion::new(command_chain, on_kind());
        Ok((expansion, whole_range))
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn array_expression(
        &mut self,
        start_token: MatchedOpenSquareBracket,
    ) -> ParsingResult<ValueContext> {
        let (values, whole_range) =
            self.values_up_to_token(start_token.into(), TokenKind::ClosedSquareBracket, || {
                MissingError::ClosedBracketSquare.into()
            })?;
        let value = Value::ArrayExpression(values);
        let context = ValueContext::new(whole_range, value);
        Ok(context)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self, on_delimiter))
    )]
    pub fn value_up_to_delimiter(
        &mut self,
        token: IonToken,
        on_delimiter: impl Fn(TokenKind) -> bool,
    ) -> ParsingResult<(ValueContext, Option<IonToken>)> {
        fn is_start_of_next_unquoted_val(kind: TokenKind) -> bool {
            kind.is_noise() || kind.is_quotation() || kind.is_end_of_statement()
        }

        if let Some(str_val) = self.may_parse_str_value(&token) {
            let str_val = str_val?;
            return Ok((str_val, None));
        }

        let first_value = self.parse_one_value(token)?;
        match self.next_token() {
            None => Ok((first_value, None)),
            Some(noise) if is_start_of_next_unquoted_val(noise.kind()) => {
                self.push_back(noise);
                Ok((first_value, None))
            }
            Some(mut next) => {
                if self.done() {
                    return Ok((first_value, None));
                }
                if on_delimiter(next.kind()) {
                    return Ok((first_value, Some(next)));
                }
                let mut values: Vec<ValueContext> = vec![first_value];

                let mut found_delimiter = None;
                loop {
                    if on_delimiter(next.kind()) {
                        found_delimiter = Some(next);
                        break;
                    }
                    let next_value = self.parse_one_value(next)?;
                    values.push(next_value);
                    let next_token = self.next_token();
                    if self.done() {
                        break;
                    }
                    match next_token {
                        None => break,
                        Some(noise) if is_start_of_next_unquoted_val(noise.kind()) => {
                            self.push_back(noise);
                            break;
                        }
                        Some(next_token) => next = next_token,
                    }
                }

                let whole_range = parsing::whole_range_from_seq(&values).unwrap();
                let value = Value::UnQuotedStr(UnQuoatedStr::Complex(values));
                Ok((ValueContext::new(whole_range, value), found_delimiter))
            }
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn value(&mut self, token: IonToken) -> ParsingResult<ValueContext> {
        let (value, _always_none) = self.value_up_to_delimiter(token, |_| false)?;
        Ok(value)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn slice_index_kind<S>(
        &mut self,
        start: &S,
        next_token: IonToken,
    ) -> ParsingResult<FoundSliceIndexKind>
    where
        S: HasLocation + std::fmt::Debug,
    {
        let kind = if next_token.kind() == TokenKind::DoublePoint {
            let after_double_point =
                self.progress_disallow_noise(start, || MissingError::NextIndexOrEqual.into())?;
            if matches!(
                after_double_point.kind(),
                TokenKind::AssignmentOp(AssignmentOpKind::Equal)
            ) {
                FoundSliceIndexKind::Inclusive
            } else {
                FoundSliceIndexKind::Exclusive { after_double_point }
            }
        } else {
            FoundSliceIndexKind::None
        };
        Ok(kind)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    fn may_parse_str_value(&mut self, token: &IonToken) -> Option<ParsingResult<ValueContext>> {
        macro_rules! map_quoted_str_err {
            ($result:expr) => {
                $result.map_err(|error| {
                    let range = Range::from_ranges_internal(token, &self.last_location());
                    ParsingError::new(range, error.into())
                })
            };
        }

        match token.kind() {
            TokenKind::Quotation => Some(map_quoted_str_err!(
                self.str(MatchedDoubleQuotationMark::from(token.location()))
            )),
            TokenKind::SingleQuotation => Some(map_quoted_str_err!(
                self.str(MatchedSingleQuotationMark::from(token.location()))
            )),
            _ => None,
        }
    }

    fn parse_one_value(&mut self, token: IonToken) -> ParsingResult<ValueContext> {
        fn command_expression<T>(
            fetcher: &mut TokenFetcherForParsing<T>,
            token: IonToken,
            on_kind: impl Fn() -> CommandExpansionKind,
        ) -> ParsingResult<ValueContext>
        where
            T: Iterator<Item = IonToken>,
        {
            fetcher
                .process_expansion(MatchedCommandExpansionBegin::new(token), on_kind)
                .map(|(expansion, whole_range)| {
                    ValueContext::new(whole_range, Value::ProcessExpansion(Box::new(expansion)))
                })
        }

        match token.kind() {
            TokenKind::OpenCurlyBracket => self
                .brace_expansion(MatchedOpenCurlyBracket::new(token))
                .map(|(range, item)| ValueContext::new(range, Value::BraceExpansion(item))),
            TokenKind::CommandStringExpansionBegin => {
                command_expression(self, token, || CommandExpansionKind::String)
            }
            TokenKind::CommandArrayExpansionBegin => {
                command_expression(self, token, || CommandExpansionKind::Array)
            }
            TokenKind::MatchExprBegin => self.math_expression(MatchedMathBegin::new(token)),
            TokenKind::Bool => Ok(ValueContext::new(token.location(), Value::Bool)),
            TokenKind::StringSigil => self
                .variable_reference(MatchedSigil::new(token.location(), SigilKind::String))
                .map(ValueContext::from),
            TokenKind::ArraySigil => self
                .variable_reference(MatchedSigil::new(token.location(), SigilKind::Array))
                .map(ValueContext::from),
            TokenKind::OpenSquareBracket => {
                self.array_expression(MatchedOpenSquareBracket::new(token))
            }
            TokenKind::Number(number_value) => {
                Ok(self.parse_number_value(NumberValueToken::new(number_value, token)))
            }
            TokenKind::Word => Ok(ValueContext::new(
                token.location(),
                Value::UnQuotedStr(UnQuoatedStr::Plain),
            )),
            wordy_like if wordy_like.is_word_like() => {
                self.parse_word_starting_with_operator(token)
            }
            _ => {
                let location = token.location();
                Err(ParsingError::new(location, InvalidError::Value.into()))
            }
        }
    }
}

#[cfg(test)]
mod testing {
    use crate::parsing::test_utils;

    #[test]
    fn parse_array_expression() {
        let input = include_str!("input_files_for_tests/array_expression.txt");
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_array_indexing() {
        let input = include_str!("input_files_for_tests/indexing.txt");
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_array_with_errors() {
        let input = include_str!("input_files_for_tests/arrays_with_errors.txt");
        let actual = test_utils::act_parsing_error_tolerance(input);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_map_expressions() {
        let input = include_str!("input_files_for_tests/map_expressions.txt");
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_math_expression() {
        let input = include_str!("input_files_for_tests/math_expression.txt");
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_error_in_math_expression() {
        let input = "let a = $(( a * b \n )";
        let actual = test_utils::act_parsing_error_tolerance(input);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_let_typed_statement() {
        const INPUT: &str = include_str!("input_files_for_tests/let_typed_statement.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_process_expansion() {
        const INPUT: &str = include_str!("input_files_for_tests/process_expansion.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_detect_errors_in_process_expansion() {
        const INPUT: &str = include_str!("input_files_for_tests/error_process_expansion.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_methods() {
        const INPUT: &str = include_str!("input_files_for_tests/methods.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_unquoted_values() {
        const INPUT: &str = include_str!("input_files_for_tests/unquoted_values.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_unquoted_values_err() {
        const INPUT: &str = include_str!("input_files_for_tests/err_unquoted_values.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_brace_expansion() {
        const INPUT: &str = include_str!("input_files_for_tests/brace_expansion.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_brace_expansion_err() {
        const INPUT: &str = include_str!("input_files_for_tests/brace_expansion_err.txt");
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }
}
