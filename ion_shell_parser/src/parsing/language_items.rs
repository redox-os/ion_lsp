pub use command_expansion::{CommandExpansion, CommandExpansionKind};
pub use comment::{Comment, DocString};
pub use function::FunctionDeclaration;
pub use has_command_invocation::HasCommandInvocation;
pub use has_var_declaration::{
    FoundVariableDeclaration, HasVariableDeclaration, OwnedFoundVariableDeclaration,
};
pub use has_var_references::HasVariableReference;
pub use language_item::{CanHaveSourceBuiltin, FoundSourceBuiltin, LanguageItem, ScopeLabel};
pub use matching::{Case, IfGuard, Match};
pub use methods::{Method, MethodKind};
pub use slice_index::{
    IndexAccess, IndexAccessContext, IndexKind, SingleIndex, SliceIndex, SliceKind,
};
pub use values::{
    BraceExpansionComponent, BraceExpansionContext, BraceSliceExpansion, ComplexQuotedStr,
    MathExpression, QuotedStr, QuotedStrComponent, UnQuoatedStr, Value, ValueContext,
};

pub mod assignment;
pub mod builtins;
pub mod command_operators;
pub mod commands;
pub mod item_scopes;
pub mod variables;

mod command_expansion;
mod comment;
mod function;
mod has_command_invocation;
mod has_var_declaration;
mod has_var_references;
mod language_item;
mod matching;
mod methods;
mod slice_index;
mod values;

use crate::{macros::gen_impl_has_location, parsing::tracing_utils};

use self::variables::VariableReference;

use super::{local_scope::LocalScope, ParsingError};

pub trait HasWarnings {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&ParsingError));
}

impl<T> HasWarnings for &[T]
where
    T: HasWarnings,
{
    fn all_warnings(&self, on_found: &mut dyn FnMut(&ParsingError)) {
        for next in *self {
            next.all_warnings(on_found);
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct LanguageItemContext {
    scope: LocalScope,
    #[cfg(all(test, debug_assertions))]
    dbg_label: crate::OptImmutableClonableText,
    item: LanguageItem,
}

impl HasWarnings for LanguageItemContext {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&ParsingError)) {
        self.item.all_warnings(on_found);
    }
}

impl HasCommandInvocation for LanguageItemContext {
    fn command_invocation(&self, on_found: &mut dyn FnMut(crate::Range)) {
        self.item.command_invocation(on_found);
    }
}

gen_impl_has_location! {LanguageItemContext, item}

impl LanguageItemContext {
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(name = "new_language_item")
    )]
    pub(crate) fn new(item: LanguageItem, scope: LocalScope) -> Self {
        tracing_utils::emit_message!("New scoped language item is produced");
        #[cfg(all(test, debug_assertions))]
        {
            Self {
                item,
                scope,
                dbg_label: Default::default(),
            }
        }
        #[cfg(not(all(test, debug_assertions)))]
        {
            Self { item, scope }
        }
    }

    pub fn item(&self) -> &LanguageItem {
        &self.item
    }

    pub fn scope(&self) -> LocalScope {
        self.scope
    }
}

impl HasVariableDeclaration for LanguageItemContext {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        self.item.var_declaration(on_found);
    }
}

impl HasVariableReference for LanguageItemContext {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.item.var_references(on_found);
    }
}

impl CanHaveSourceBuiltin for LanguageItemContext {
    fn source_calls(&self, on_found: impl FnMut(language_item::FoundSourceBuiltin)) {
        self.item.source_calls(on_found);
    }
}

#[cfg(all(test, debug_assertions))]
impl crate::parsing::HasOptAllocatedDebugText for LanguageItemContext {
    fn go_through_dbg_txt(
        &mut self,
        on_found: &dyn Fn(crate::Range) -> crate::ImmutableClonableText,
    ) {
        use crate::parsing::HasLocation;
        self.item.go_through_dbg_txt(&on_found);
        let location = self.location();
        let to_insert = on_found(location);
        let _ = self.dbg_label.insert(to_insert);
    }
}

#[cfg(test)]
mod testing {
    use crate::parsing::{language_items::CanHaveSourceBuiltin, test_utils, HasLocation};

    #[test]
    fn get_all_source_commands() {
        #[derive(Debug)]
        pub struct TestResult<'a> {
            pub source_command: &'a str,
            pub to_be_sourced: &'a str,
        }

        const EXPECTED_COUNT: usize = 4;
        const INPUT: &str = include_str!("input_files_for_tests/with_source_commands.txt");

        let parsed = test_utils::act_parsing_without_error(INPUT);
        let mut sourcing = Vec::with_capacity(EXPECTED_COUNT);
        parsed.into_iter().for_each(|next| {
            next.source_calls(|found| {
                let to_push = TestResult {
                    source_command: crate::get_string_from_input(
                        INPUT,
                        found.source_command_range(),
                    ),
                    to_be_sourced: crate::get_string_from_input(
                        INPUT,
                        found.to_be_sourced().location(),
                    ),
                };
                sourcing.push(to_push);
            })
        });

        assert_eq!(EXPECTED_COUNT, sourcing.len(), "Input:\n {}", INPUT);
        insta::assert_debug_snapshot!(sourcing);
    }
}
