use std::fmt::Debug;

use crate::{
    macros::gen_impl_has_location,
    range::Range,
    tokens::{IonToken, TokenKind},
};
use derive_more::Display;
use derive_more::{AsRef, Deref, From, Into};

use super::{
    language_items::ValueContext, parsing_error::UnmathcedStrError, HasLocation, RangeDebugText,
};

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedCommand(ValueContext);

impl MatchedCommand {
    pub(crate) fn new(value: ValueContext) -> Self {
        Self(value)
    }
}

macro_rules! impl_new_assert {
    ($name:ident, $($expected:expr),+ ) => {
        impl $name {
            pub(crate) fn new(value: IonToken) -> Self {
                debug_assert!($(value.kind() == ($expected))||*);
                Self(value)
            }
        }
    };
}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedCommandExpansionBegin(IonToken);

impl_new_assert! {MatchedCommandExpansionBegin,
TokenKind::CommandStringExpansionBegin,
TokenKind::CommandArrayExpansionBegin
}

gen_impl_has_location! {MatchedCommandExpansionBegin}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedHash(IonToken);
impl_new_assert! {MatchedHash, TokenKind::Hash}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedLet(IonToken);
impl_new_assert! {MatchedLet, TokenKind::Let}
gen_impl_has_location! {MatchedLet}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedExport(IonToken);
impl_new_assert! {MatchedExport, TokenKind::Export}
gen_impl_has_location! {MatchedExport}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedIfClause(IonToken);
impl_new_assert! {MatchedIfClause, TokenKind::If}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedElse(IonToken);
impl_new_assert! {MatchedElse, TokenKind::Else}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedElseIf(IonToken);
impl_new_assert! {MatchedElseIf, TokenKind::ElseIf}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedMatch(IonToken);
impl_new_assert! {MatchedMatch, TokenKind::Match}
gen_impl_has_location! {MatchedMatch}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedCase(IonToken);
impl_new_assert! {MatchedCase, TokenKind::Case}
gen_impl_has_location! {MatchedCase}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedFor(IonToken);
gen_impl_has_location! {MatchedFor}
impl_new_assert! {MatchedFor, TokenKind::For}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedEnd(IonToken);
impl_new_assert! {MatchedEnd, TokenKind::End}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedContinue(IonToken);
impl_new_assert! {MatchedContinue, TokenKind::Continue}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedBreak(IonToken);
impl_new_assert! {MatchedBreak, TokenKind::Break}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedWhile(IonToken);
impl_new_assert! {MatchedWhile, TokenKind::While}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedFn(IonToken);
impl_new_assert! {MatchedFn, TokenKind::Fn}
gen_impl_has_location! {MatchedFn}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedMathBegin(IonToken);
impl_new_assert! {MatchedMathBegin, TokenKind::MatchExprBegin}
gen_impl_has_location! {MatchedMathBegin}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedOpenCurlyBracket(IonToken);
impl_new_assert! {MatchedOpenCurlyBracket, TokenKind::OpenCurlyBracket}
gen_impl_has_location! {MatchedOpenCurlyBracket}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedOpenSquareBracket(IonToken);

impl_new_assert! {MatchedOpenSquareBracket, TokenKind::OpenSquareBracket}
gen_impl_has_location! {MatchedOpenSquareBracket}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedOpenParanteseCurlyBracket(IonToken);
impl_new_assert! {MatchedOpenParanteseCurlyBracket, TokenKind::OpenParanteseCurlyBracket}
gen_impl_has_location! {MatchedOpenParanteseCurlyBracket}

#[derive(Debug, AsRef, Deref, Into)]
pub struct MatchedQuotationToken(IonToken);
impl_new_assert! {MatchedQuotationToken, TokenKind::SingleQuotation, TokenKind::Quotation}
gen_impl_has_location! {MatchedQuotationToken}

impl From<MatchedCommand> for RangeDebugText {
    fn from(value: MatchedCommand) -> Self {
        value.0.location().into()
    }
}

#[derive(Debug, Clone, Copy)]
pub struct MatchedSigil {
    range: Range,
    kind: SigilKind,
}

impl HasLocation for MatchedSigil {
    fn location(&self) -> Range {
        self.range
    }
}

impl MatchedSigil {
    pub fn new(range: Range, kind: SigilKind) -> Self {
        Self { range, kind }
    }

    pub fn kind(&self) -> SigilKind {
        self.kind
    }
}
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SigilKind {
    String,
    Array,
}

pub trait MatchedQuotationMark: HasLocation + std::fmt::Debug {
    const SYMBOL: &'static str;

    fn error_kind(&self) -> UnmathcedStrError;
    fn is_mark(&self, token: TokenKind) -> bool;
}

#[derive(Debug, Clone, Copy, From)]
pub struct MatchedDoubleQuotationMark(Range);

impl HasLocation for MatchedDoubleQuotationMark {
    fn location(&self) -> Range {
        self.0
    }
}

impl MatchedQuotationMark for MatchedDoubleQuotationMark {
    const SYMBOL: &'static str = "\"";
    fn error_kind(&self) -> UnmathcedStrError {
        UnmathcedStrError::Double
    }

    fn is_mark(&self, token: TokenKind) -> bool {
        matches!(token, TokenKind::Quotation)
    }
}

#[derive(Debug, Clone, Copy, From, Display)]
#[display = "'"]
pub struct MatchedSingleQuotationMark(Range);

impl HasLocation for MatchedSingleQuotationMark {
    fn location(&self) -> Range {
        self.0
    }
}

impl MatchedQuotationMark for MatchedSingleQuotationMark {
    const SYMBOL: &'static str = "'";

    fn error_kind(&self) -> UnmathcedStrError {
        UnmathcedStrError::Single
    }

    fn is_mark(&self, token: TokenKind) -> bool {
        matches!(token, TokenKind::SingleQuotation)
    }
}
