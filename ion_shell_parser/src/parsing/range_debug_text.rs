use crate::{macros::gen_impl_dbg_txt, range::Range};

use super::HasLocation;

#[cfg(all(debug_assertions, test))]
#[derive(Debug, PartialEq, Clone)]
pub struct RangeDebugText {
    range: Range,
    dbg_text: crate::OptImmutableClonableText,
}

impl std::fmt::Display for RangeDebugText {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        #[cfg(all(debug_assertions, test))]
        {
            let text = self
                .dbg_text
                .as_ref()
                .map(|e| e.to_string())
                .unwrap_or_default();
            write!(f, "{}{}", self.range, text)
        }

        #[cfg(not(all(debug_assertions, test)))]
        {
            write!(f, "{}", self.0)
        }
    }
}

#[cfg(not(all(debug_assertions, test)))]
#[derive(Debug, PartialEq, Clone)]
pub struct RangeDebugText(Range);

impl RangeDebugText {
    pub fn new(range: Range) -> Self {
        #[cfg(all(debug_assertions, test))]
        {
            Self {
                range,
                #[cfg(all(debug_assertions, test))]
                dbg_text: None,
            }
        }
        #[cfg(not(all(debug_assertions, test)))]
        Self(range)
    }
}

impl HasLocation for RangeDebugText {
    fn location(&self) -> Range {
        #[cfg(all(debug_assertions, test))]
        {
            self.range
        }
        #[cfg(not(all(debug_assertions, test)))]
        self.0
    }
}

gen_impl_dbg_txt! {RangeDebugText <= dbg_text}

impl From<Range> for RangeDebugText {
    fn from(value: Range) -> Self {
        Self::new(value)
    }
}
