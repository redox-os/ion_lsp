use crate::IonToken;

use super::{tracing_utils, ParsingResult};

pub use scope_manager::{ScopeMap, ScopeRelation};

pub(crate) use scope_manager::ScopeManager;
pub(crate) use token_fetcher_parsing::TokenFetcherForParsing;

mod scope_manager;
mod token_fetcher_parsing;

/// # Rules for the parsing
///
/// See the file called `grammar_for_syntax.txt`
#[derive(Debug)]
pub struct IonParser<'a, T> {
    fetcher: TokenFetcherForParsing<'a, T>,
    scoping: ScopeManager,
}

impl<'a, T> IonParser<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    pub fn new(seq: T, input: &'a str) -> Self {
        let fetcher = TokenFetcherForParsing::new(seq, input);
        tracing_utils::emit_object_with_message!("Input to parse", input);
        Self {
            fetcher,
            scoping: Default::default(),
        }
    }
}

impl<'a, T> IonParser<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    pub fn scope_map(&self) -> &ScopeMap {
        self.scoping.scope_map()
    }

    pub fn into_scope_map(self) -> ScopeMap {
        self.scoping.into_scope_map()
    }

    pub fn input(&self) -> &str {
        self.fetcher.input()
    }
}

impl<'a, T> Iterator for IonParser<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    type Item = ParsingResult;

    /// Warning is triggered by #[cfg(all(debug_assertions, test))]
    #[allow(clippy::collapsible_match)]
    fn next(&mut self) -> Option<Self::Item> {
        if self.fetcher.done() {
            return None;
        }

        let context = self.fetcher.statement(&mut self.scoping);

        context.map(|unscoped| {
            #[cfg(all(debug_assertions, test))]
            let mut unscoped = unscoped;
            #[cfg(all(debug_assertions, test))]
            {
                use crate::parsing::HasOptAllocatedDebugText;
                unscoped.go_through_dbg_txt(&|range| {
                    crate::get_string_from_input(self.input(), range).into()
                })
            }

            match unscoped {
                Err(unscoped_error) => {
                    self.fetcher
                        .err_recovery_progress_to_checkpoint(unscoped_error.opt_recovery_point());
                    Err(unscoped_error)
                }
                Ok(unscoped) => {
                    let scoped = self.scoping.try_turn_to_scoped(unscoped).map_err(|error| {
                        self.fetcher
                            .err_recovery_progress_to_checkpoint(error.opt_recovery_point());
                        error
                    });

                    #[cfg(all(debug_assertions, test))]
                    let mut scoped = scoped;
                    #[cfg(all(debug_assertions, test))]
                    {
                        use crate::parsing::HasOptAllocatedDebugText;
                        scoped.go_through_dbg_txt(&|range| {
                            crate::get_string_from_input(self.input(), range).into()
                        })
                    }

                    scoped
                }
            }
        })
    }
}
