mod conditional;
mod edge_cases;
mod unconditional;
mod values;
mod variables;

use crate::parsing::language_items::ValueContext;

use super::internal_prelude::*;
use super::language_items::variables::VariableReferenceContext;
use super::matched_fragments::{MatchedBreak, MatchedContinue};
use super::{
    ion_parser::ScopeManager,
    language_items::{
        item_scopes::{EnterScope, ExitScope, SwitchScope},
        Comment, Method,
    },
    matched_fragments::{
        MatchedCase, MatchedElse, MatchedElseIf, MatchedEnd, MatchedExport, MatchedFn, MatchedFor,
        MatchedHash, MatchedIfClause, MatchedLet, MatchedMatch, MatchedWhile,
    },
};

#[derive(Debug)]
pub(crate) enum ValueStartingWithSigil {
    Method((Method, Range)),
    VariableReference(VariableReferenceContext),
}

impl From<ValueStartingWithSigil> for ValueContext {
    fn from(value: ValueStartingWithSigil) -> Self {
        match value {
            ValueStartingWithSigil::Method(context) => context.into(),
            ValueStartingWithSigil::VariableReference(reference) => reference.into(),
        }
    }
}

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    /// # Returns
    ///
    /// * None if end of input was encountered.j
    /// * Otherwise an error in syntax or an stytaticly atomic statement in the language.
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn statement(&mut self, scope: &mut ScopeManager) -> Option<ParsingResult<LanguageItem>> {
        let Some(token) = self.skip_noise_or_eos().expect_no_eoi() else {
            match scope.check_for_left_over_scopes().map_err(|scope| {
                ParsingError::new(
                    self.last_location(),
                    MissingError::EndForScope(scope).into(),
                )
            }) {
                Ok(_) => return None,
                Err(error) => return Some(Err(error)),
            }
        };

        let next_token = token.token();

        let item = match next_token.kind() {
            TokenKind::Eoi => return None,
            TokenKind::Fn => self
                .function_declaration(MatchedFn::new(next_token))
                .map(|function| LanguageItem::Enter(EnterScope::Function(function)))
                .recoverable_from_next_end_keyword(),
            TokenKind::Let => self
                .let_statement(MatchedLet::new(next_token))
                .fallback_recoverable_from_next_eos(),
            TokenKind::Case => self.on_matched_case(scope, MatchedCase::new(next_token)),
            TokenKind::Match => self
                .match_clause(MatchedMatch::new(next_token))
                .map(|parsed| LanguageItem::Enter(EnterScope::Match(parsed)))
                .recoverable_from_next_end_keyword(),
            TokenKind::Export => self
                .export_declaration(MatchedExport::new(next_token))
                .map(LanguageItem::Export)
                .fallback_recoverable_from_next_eos(),
            TokenKind::Hash => Ok(self.comment(MatchedHash::new(next_token))),
            TokenKind::For => self
                .for_loop(MatchedFor::new(next_token))
                .map(|for_context| LanguageItem::Enter(EnterScope::For(for_context)))
                .recoverable_from_next_end_keyword(),
            TokenKind::If => self
                .if_clause(MatchedIfClause::new(next_token))
                .map(|if_context| LanguageItem::Enter(EnterScope::If(if_context)))
                .recoverable_from_next_end_keyword(),
            TokenKind::Else => self
                .else_clause(MatchedElse::new(next_token))
                .map(|else_context| LanguageItem::Switch(SwitchScope::Else(else_context)))
                .recoverable_from_next_end_keyword(),
            TokenKind::ElseIf => self
                .else_if_clause(MatchedElseIf::new(next_token))
                .map(|else_context| LanguageItem::Switch(SwitchScope::ElseIf(else_context)))
                .recoverable_from_next_end_keyword(),
            TokenKind::While => self
                .while_clause(MatchedWhile::new(next_token))
                .map(LanguageItem::Enter)
                .recoverable_from_next_end_keyword(),
            TokenKind::Continue => self
                .continue_keyword(MatchedContinue::new(next_token))
                .map(LanguageItem::Continue)
                .fallback_recoverable_from_next_eos(),
            TokenKind::Break => self
                .break_keyword(MatchedBreak::new(next_token))
                .map(LanguageItem::Break)
                .fallback_recoverable_from_next_eos(),
            TokenKind::End => self
                .end_scope(MatchedEnd::new(next_token))
                .map(|end_context| LanguageItem::Exit(end_context.location().into()))
                .fallback_recoverable_from_next_eos(),
            _command => self
                .match_command(next_token)
                .and_then(|matched_command| {
                    self.command_chain(matched_command)
                        .map(LanguageItem::Command)
                })
                .fallback_recoverable_from_next_eos(),
        };

        Some(item)
    }

    fn on_matched_case(
        &mut self,
        scope: &ScopeManager,
        next_token: MatchedCase,
    ) -> ParsingResult<LanguageItem> {
        let no_previous_match = !scope.there_is_a_previous_match_or_case();
        if no_previous_match {
            Err(ParsingError::new(
                next_token.location(),
                MissingError::MatchForCase.into(),
            ))
            .fallback_recoverable_from_next_eos()
        } else {
            self.case_clause(next_token)
                .map(|case_parsed| {
                    if scope.case_comes_before() {
                        LanguageItem::Switch(SwitchScope::Case(case_parsed))
                    } else {
                        LanguageItem::Enter(EnterScope::Case(case_parsed))
                    }
                })
                .fallback_recoverable_from_next_end_keyword()
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn comment(&mut self, hash_token: MatchedHash) -> LanguageItem {
        let comment = self.parse_comment(hash_token.into());
        LanguageItem::Comment(comment)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn parse_comment(&mut self, token: IonToken) -> Comment {
        fn return_comment_content(hash_token: IonToken, previous_range: Range) -> Comment {
            let end_of_previous = previous_range.end_pos();
            let hash_location = hash_token.location();
            let outer = Range::new_internal(hash_location.start_pos(), end_of_previous);
            let inner = Range::new_internal(hash_location.end_pos(), end_of_previous);
            Comment::new(outer, inner)
        }

        let mut previous_range = token.location();
        while let Some(next_token) = self.next_token() {
            if next_token.kind().is_new_line() {
                return return_comment_content(token, previous_range);
            }
            previous_range = next_token.location();
        }

        return_comment_content(token, previous_range)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn end_scope(&mut self, matched: MatchedEnd) -> ParsingResult<ExitScope> {
        match self.skip_horz_noise() {
            SkippedToNext::EndOfStatement(_) | SkippedToNext::Eoi => Ok(()),
            SkippedToNext::Next(comment) if comment.kind().is_comment() => {
                self.push_back(comment);
                Ok(())
            }
            SkippedToNext::Next(not_expected) => {
                let not_expected = not_expected.location();
                Err(ParsingError::new(not_expected, InvalidError::End.into()))
            }
        }?;
        Ok(ExitScope::new(matched.location()))
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn continue_keyword(&mut self, matched: MatchedContinue) -> ParsingResult<RangeDebugText> {
        let context: RangeDebugText = IonToken::from(matched).location().into();
        self.check_for_trailing_comment(|| NoEosAfter::Else.into())?;
        Ok(context)
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn break_keyword(&mut self, matched: MatchedBreak) -> ParsingResult<RangeDebugText> {
        let context: RangeDebugText = IonToken::from(matched).location().into();
        self.check_for_trailing_comment(|| NoEosAfter::Else.into())?;
        Ok(context)
    }
}

#[cfg(test)]
mod testing {

    use crate::parsing::test_utils;

    const SIMPLE_EXPORT_TEST_INPUT: &str = "export aaaa";

    #[test]
    fn parse_zero_as_number_value() {
        const INPUT: &str = "let a = 0";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_word_value_starting_with_number() {
        const INPUT: &str = "let a = 22aa22aacc!";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn detect_invalid_var_name() {
        const INPUT: &str = "let 💝 = 'hello  world'\n\
            let 2 = 2\n\
            let _hello = world";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_with_escaped_single_quotation_within() {
        let input: &str = "let hello = 'hello \\\'\\\' world '";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_with_escaped_quotation_within() {
        let input: &str = "let hello = \"hello ''  \\\" world \"";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_err_unmatched_double_single_quotation() {
        let input: &str = "let hello = \"2
let name = aa
let hello = '2
let hello = bb";
        let actual = test_utils::act_parsing_error_tolerance(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parsing_invalid_variable_name() {
        let input = "let 2a = hello";
        let actual = test_utils::act_parsing_error_tolerance(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parsing_without_assignment() {
        let input =
            "let 2aa 2 2aa2a2aaasdasdfsdfasdfasdfasd asdfasdfasdfkj\"aadsfa2inpuasdfast\"\n";
        let actual = test_utils::act_parsing_error_tolerance(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parsing_with_comments() {
        let input = include_str!("input_files_for_tests/statements_with_comments.txt");
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn error_tolerant_parsing() {
        let input = "let name aaaa
export some_name";
        let actual = test_utils::act_parsing_error_tolerance(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_export_statement() {
        let actual = test_utils::act_parsing_without_error(SIMPLE_EXPORT_TEST_INPUT);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_export_statement_with_noise() {
        let actual = test_utils::act_parsing_without_error(SIMPLE_EXPORT_TEST_INPUT);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_echo_with_string_ref() {
        let input = "echo $name";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_echo_with_array_ref() {
        let input = "echo @name";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_several_echo_lines() {
        let input = "echo hello
echo world
let name = aaa";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn several_lines_with_string_vars_echos_operator_in_words() {
        let input = include_str!("../input_files_for_tests/string_variables_tokens.txt");
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_echo() {
        let input = "echo";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_echo_with_args() {
        let input = "echo one two";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(actual)
    }

    #[test]
    fn parse_single_val_var_assignment() {
        let input = "let name = \"some name\"";
        let actual = test_utils::act_parsing_without_error(input);
        insta::assert_debug_snapshot!(&actual);
    }

    #[test]
    fn parse_let_statement_with_too_many_value() {
        let input: &str = include_str!("./input_files_for_tests/too_many_values_after_let.txt");
        let actual = test_utils::act_parsing_error_tolerance(input);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_statement_with_trailing_comments() {
        const INPUT: &str =
            include_str!("./input_files_for_tests/statement_with_trailing_comment.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parse_statements_with_semicolons() {
        const INPUT: &str = include_str!("./input_files_for_tests/statements_with_semicolons.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parses_empty_str() {
        const INPUT: &str = "let a = \"\"";
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parses_str_with_vars() {
        const INPUT: &str = include_str!("input_files_for_tests/str_with_vars.txt");
        let actual = test_utils::act_parsing_without_error(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parses_str_with_vars_unmatched_quotation() {
        const INPUT: &str = "echo '$x => @";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parses_str_with_vars_double_unmatched_quotation() {
        const INPUT: &str = "echo \" $x =>  ";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }

    #[test]
    fn parses_str_with_vars_unmatched_on_eoi_after_valid_variable_name() {
        const INPUT: &str = "echo \" $x =>  $a";
        let actual = test_utils::act_parsing_error_tolerance(INPUT);
        insta::assert_debug_snapshot!(actual);
    }
}
