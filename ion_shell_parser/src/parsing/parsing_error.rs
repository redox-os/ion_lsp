pub use builtin_error_kind::{BuilinErrorKind, BuiltinError};
pub use invalid_error_kind::{InvalidCommandForOperator, InvalidError};
pub use is_not_error_kind::IsNotError;
pub use missing_error_kind::{MissingCommandFor, MissingError, MissingValueFor};
pub use no_eos_error_kind::NoEosAfter;
pub use parsing_error_kind::{ParsingErrorKind, UnmathcedStrError};

mod builtin_error_kind;
mod invalid_error_kind;
mod is_not_error_kind;
mod missing_error_kind;
mod no_eos_error_kind;
mod parsing_error_kind;

use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::tracing_utils,
    range::Range,
    DiagnosticMessage, TextAccessor, ValidationErrorMeta,
};

use super::{
    error_recovery::{ChosenErrRecoveryStrategy, OptErrRecovery},
    HasLocation, RecoverableError,
};

use thiserror::Error;

#[derive(Debug, PartialEq, Error, Clone)]
#[error("{meta_context} => {kind}")]
pub struct ParsingError {
    meta_context: ValidationErrorMeta,
    kind: ParsingErrorKind,
    opt_recovery_point: OptErrRecovery,
}

trait ReportableParsingError {
    fn to_txt<'a>(
        &self,
        on_base_insert: impl Fn() -> &'a str + 'a,
        on_insert: impl Fn(Range) -> std::borrow::Cow<'a, str> + 'a,
    ) -> DiagnosticMessage;

    fn message(&self, accessor: &crate::TextAccessor) -> DiagnosticMessage {
        self.to_txt(
            || accessor.get_content(),
            |range| accessor.get_another_content(range).into(),
        )
    }

    fn for_display(&self) -> String {
        self.to_txt(|| "", |range| range.to_string().into())
            .into_owned()
    }
}

gen_impl_dbg_txt! {ParsingError, meta_context}
gen_impl_has_location! {ParsingError, meta_context}

impl RecoverableError for ParsingError {
    fn recoverable_from(mut self, strategy: ChosenErrRecoveryStrategy) -> Self {
        self.opt_recovery_point = Some(strategy);
        self
    }

    fn recoverable_as_fallback(mut self, strategy: ChosenErrRecoveryStrategy) -> Self {
        let _ = self.opt_recovery_point.get_or_insert(strategy);
        self
    }

    fn with_put_last_token_back(mut self) -> Self {
        self.opt_recovery_point = self.opt_recovery_point.map(|error| error.with_just_peek());
        self
    }
}

impl ParsingError {
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(name = "new_parsing_error")
    )]
    pub fn new(range: Range, kind: ParsingErrorKind) -> Self {
        let meta_context = ValidationErrorMeta::new(range.into(), Default::default());

        tracing_utils::emit_message!("Parsing error detected");
        Self {
            meta_context,
            kind,
            opt_recovery_point: None,
        }
    }

    pub fn message(&self, input: &str) -> DiagnosticMessage {
        let encapsulated = TextAccessor::new(self.location(), input);
        self.kind().message(&encapsulated)
    }

    pub fn as_warning(mut self) -> Self {
        self.meta_context = self.meta_context.as_warning();
        self
    }

    pub fn opt_recovery_point(&self) -> Option<&ChosenErrRecoveryStrategy> {
        self.opt_recovery_point.as_ref()
    }

    pub fn kind(&self) -> ParsingErrorKind {
        self.kind
    }

    pub fn meta_context(&self) -> &ValidationErrorMeta {
        &self.meta_context
    }
}
