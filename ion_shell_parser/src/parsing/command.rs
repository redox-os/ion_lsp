use crate::{
    parsing::{
        adapting_token_fetching::SkippedToNext, ion_parser::TokenFetcherForParsing,
        parsing_error::InvalidError, ParsingError, ParsingResult,
    },
    tokens::TokenKind,
    IonToken,
};

use super::matched_fragments::MatchedCommand;

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    pub fn match_command(&mut self, token: IonToken) -> ParsingResult<MatchedCommand> {
        match token.kind() {
            TokenKind::LogicalChain(_) | TokenKind::Pipe(_) | TokenKind::Redirect(_) => {
                let location = token.location();
                Err(ParsingError::new(location, InvalidError::Command.into()))
            }
            _ => {
                let command = self.value(token)?;
                let matched_command = MatchedCommand::new(command);
                Ok(matched_command)
            }
        }
    }

    pub fn match_command_delimiter(
        &mut self,
        token: IonToken,
        on_delimiter: impl Fn(TokenKind) -> bool,
    ) -> ParsingResult<(MatchedCommand, Option<IonToken>)> {
        match token.kind() {
            TokenKind::LogicalChain(_) | TokenKind::Pipe(_) | TokenKind::Redirect(_) => {
                let location = token.location();
                Err(ParsingError::new(location, InvalidError::Command.into()))
            }
            _ => {
                let (command, delimiter) = self.value_up_to_delimiter(token, on_delimiter)?;

                let matched_command = MatchedCommand::new(command);
                Ok((matched_command, delimiter))
            }
        }
    }

    pub fn match_command_from_skipped(
        &mut self,
        skipped: SkippedToNext,
    ) -> Result<MatchedCommand, NoCommand> {
        match skipped {
            SkippedToNext::Eoi => Err(NoCommand::Eoi),
            SkippedToNext::EndOfStatement(token) => Err(NoCommand::EndOfStatement(token)),
            SkippedToNext::Next(next) => self
                .match_command(next.clone())
                .map_err(|_| NoCommand::IllegalOperator(next)),
        }
    }
}
pub enum NoCommand {
    Eoi,
    EndOfStatement(IonToken),
    IllegalOperator(IonToken),
}
