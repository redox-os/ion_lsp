use std::collections::VecDeque;

use crate::{
    parsing::{
        adapting_token_fetching::SkippedToNext, error_recovery::RefOptErrRecovery, HasLocation,
        ParsingError, ParsingErrorKind, ParsingResult, RecoverableError as _,
    },
    IonToken, Range, TokenKind,
};

#[derive(Debug)]
pub(crate) struct TokenFetcherForParsing<'a, T> {
    tokens: T,
    peeked: VecDeque<IonToken>,
    done: bool,
    last_token: Option<IonToken>,
    input: &'a str,
}

macro_rules! trace_token {
    ($label:expr, ? $token:expr, $fetcher:ident) => {
        #[cfg(feature = "ion_shell_parser_trace_parse_phase")]
        {
            use super::tracing_utils;
            let input = $fetcher.input.clone();
            if let Some(token) = $token.as_ref() {
                let range = token.location();
                let text = $crate::get_string_from_input(&input, range);
                tracing_utils::emit_object_with_message!(($label, "Text") => ($token, text))
            } else {
                tracing_utils::emit_message!("No token left");
            }
        }
    };
    ($label:expr, $token:expr, $fetcher:ident) => {
        #[cfg(feature = "ion_shell_parser_trace_parse_phase")]
        {
            use super::tracing_utils;
            let input = $fetcher.input.clone();
            let range = $token.location();
            let text = $crate::get_string_from_input(&input, range);
            tracing_utils::emit_object_with_message!($label, ($token, text))
        }
    };
}

impl<'a, T> TokenFetcherForParsing<'a, T>
where
    T: Iterator<Item = IonToken>,
{
    pub fn new(tokens: T, input: &'a str) -> Self {
        Self {
            last_token: Default::default(),
            tokens,
            peeked: Default::default(),
            done: false,
            input,
        }
    }
    pub fn last_token(&self) -> Option<&IonToken> {
        self.last_token.as_ref()
    }

    /// Parameter `token`: Will be yielded again for a next token.
    /// Note: More tokens are handled by FIFO principle.  
    pub fn push_back(&mut self, token: IonToken) {
        trace_token!("Token pushed back", &token, self);
        self.peeked.push_back(token);
    }

    /// # Returns
    ///
    /// * None: If end of the input is reached.
    /// * Some: Otherwise the next token is yielded.
    pub fn next_token(&mut self) -> Option<IonToken> {
        let next_token = self.peeked.pop_front().or_else(|| self.tokens.next());
        trace_token!("Next Token", ? &next_token, self);
        if let Some(token) = next_token.as_ref() {
            self.last_token = Some(token.clone());
            if token.kind() == TokenKind::Eoi {
                self.done = true;
            }
        } else {
            self.done = true;
        }

        next_token
    }

    /// Location of last yielded token
    pub fn last_location(&self) -> Range {
        self.last_token
            .as_ref()
            .map(HasLocation::location)
            .unwrap_or_default()
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn err_recovery_progress_to_checkpoint(&mut self, opt_recovery: RefOptErrRecovery) {
        #[derive(Debug, Clone, Copy, Eq, PartialEq)]
        enum CurrentQuotation {
            Single,
            Double,
            None,
        }
        match opt_recovery {
            Some(validator) => {
                let mut machted_quote: CurrentQuotation = CurrentQuotation::None;
                while let Some(next_token) = self.next_token() {
                    let kind = next_token.kind();
                    if machted_quote == CurrentQuotation::None && validator.is_recovery_point(kind)
                    {
                        if validator.put_last_token_back() {
                            self.push_back(next_token);
                        }
                        break;
                    }
                    match (machted_quote, kind) {
                        (CurrentQuotation::Single, TokenKind::SingleQuotation)
                        | (CurrentQuotation::Double, TokenKind::Quotation) => {
                            machted_quote = CurrentQuotation::None
                        }
                        (CurrentQuotation::None, TokenKind::SingleQuotation) => {
                            machted_quote = CurrentQuotation::Single
                        }
                        (CurrentQuotation::None, TokenKind::Quotation) => {
                            machted_quote = CurrentQuotation::Double
                        }
                        _ => (),
                    }
                }
            }
            None => self.done = true,
        }
    }

    pub fn skip_horz_noise(&mut self) -> SkippedToNext {
        self.skip_by(TokenKind::is_horizontal_noise)
    }

    pub fn skip_noise(&mut self) -> SkippedToNext {
        self.skip_by(TokenKind::is_noise)
    }

    pub fn skip_noise_or_eos(&mut self) -> SkippedToNext {
        self.skip_by(TokenKind::is_eos_or_noise)
    }

    fn progress_bail(
        &mut self,
        progressed: SkippedToNext,
        start: &impl HasLocation,
        error_kind: impl Fn() -> ParsingErrorKind,
    ) -> ParsingResult<IonToken> {
        match progressed {
            SkippedToNext::EndOfStatement(_) => {
                Err(ParsingError::new(
                    Range::from_start_end_internal(start.location(), self.last_location()),
                    error_kind(),
                ))
            }
            .recoverable_from_next_token_preserved(),
            SkippedToNext::Eoi => Err(ParsingError::new(
                Range::from_start_end_internal(start.location(), self.last_location()),
                error_kind(),
            )),
            SkippedToNext::Next(next) => Ok(next),
        }
    }

    pub fn progress_skip_horz_bail_if_eos_or_eoi(
        &mut self,
        start: &impl HasLocation,
        error_kind: impl Fn() -> ParsingErrorKind,
    ) -> ParsingResult<IonToken> {
        let progressed = self.skip_horz_noise();
        self.progress_bail(progressed, start, error_kind)
    }

    pub fn progress_disallow_noise(
        &mut self,
        start: &impl HasLocation,
        error_kind: impl Fn() -> ParsingErrorKind,
    ) -> ParsingResult<IonToken> {
        let might_be_noise = self.progress_bail_if_eos_or_eoi(start, error_kind)?;
        match might_be_noise.kind() {
            TokenKind::WhiteSpace(_) => Err(ParsingError::new(
                might_be_noise.location(),
                ParsingErrorKind::WhiteSpaceNotAllowed,
            )),
            TokenKind::Tab(_) => Err(ParsingError::new(
                might_be_noise.location(),
                ParsingErrorKind::TabNotAllowed,
            )),
            TokenKind::NewLine => Err(ParsingError::new(
                might_be_noise.location(),
                ParsingErrorKind::NewLineNotAllowed,
            )),
            _ => Ok(might_be_noise),
        }
    }
    pub fn progress_bail_if_eos_or_eoi(
        &mut self,
        start: &impl HasLocation,
        error_kind: impl Fn() -> ParsingErrorKind,
    ) -> ParsingResult<IonToken> {
        let progressed = match self.next_token() {
            Some(maybe_eos) => {
                if maybe_eos.kind().is_end_of_statement() {
                    SkippedToNext::EndOfStatement(maybe_eos)
                } else {
                    SkippedToNext::Next(maybe_eos)
                }
            }
            None => SkippedToNext::Eoi,
        };
        self.progress_bail(progressed, start, error_kind)
    }

    pub fn progress_skip_bail_if_eos_or_eoi(
        &mut self,
        start: &impl HasLocation,
        error_kind: impl Fn() -> ParsingErrorKind,
    ) -> ParsingResult<IonToken> {
        let progressed = self.skip_noise();
        self.progress_bail(progressed, start, error_kind)
    }

    pub fn skip_by(&mut self, on_check: impl Fn(TokenKind) -> bool) -> SkippedToNext {
        while let Some(next) = self.next_token() {
            if self.done {
                return SkippedToNext::Eoi;
            }
            let kind = next.kind();
            if !on_check(kind) {
                return if kind.is_end_of_statement() {
                    SkippedToNext::EndOfStatement(next)
                } else {
                    SkippedToNext::Next(next)
                };
            }
        }
        SkippedToNext::Eoi
    }

    pub(crate) fn input(&self) -> &str {
        self.input
    }

    pub(crate) fn done(&self) -> bool {
        self.done
    }
}
