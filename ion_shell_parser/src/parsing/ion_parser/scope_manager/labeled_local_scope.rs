use crate::parsing::{language_items::ScopeLabel, LocalScope};

#[derive(Debug, Clone, Copy)]
pub struct LabeledLocalScope {
    label: ScopeLabel,
    scope: LocalScope,
}

impl LabeledLocalScope {
    pub fn new(label: ScopeLabel, scope: LocalScope) -> Self {
        Self { label, scope }
    }

    pub fn label(&self) -> ScopeLabel {
        self.label
    }

    pub fn scope(&self) -> LocalScope {
        self.scope
    }
}
