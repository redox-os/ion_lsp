use crate::parsing::LocalScope;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct ScopeRelation {
    parent: Option<LocalScope>,
    children: Vec<LocalScope>,
}

impl ScopeRelation {
    pub fn new(parent: Option<LocalScope>) -> Self {
        Self {
            parent,
            children: Default::default(),
        }
    }

    pub fn parent(&self) -> Option<LocalScope> {
        self.parent
    }

    pub fn children(&self) -> &[LocalScope] {
        &self.children
    }
    pub fn add_child(&mut self, new: LocalScope) {
        self.children.push(new);
    }
}
