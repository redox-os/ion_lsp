use crate::parsing::{language_items::ScopeLabel, LocalScope, ScopeRelation};

use super::{labeled_local_scope::LabeledLocalScope, ScopeMap};

#[derive(Debug)]
pub struct ScopeStack {
    stack: Vec<LabeledLocalScope>,
    scope_map: ScopeMap,
}

impl Default for ScopeStack {
    fn default() -> Self {
        let first_relation = ScopeRelation::new(None);
        let scope_map = ScopeMap::from_iter([(LocalScope::default(), first_relation)]);
        Self {
            stack: Default::default(),
            scope_map,
        }
    }
}

impl ScopeStack {
    pub fn scope_map(&self) -> &ScopeMap {
        &self.scope_map
    }

    pub fn into_scope_map(self) -> ScopeMap {
        self.scope_map
    }

    pub fn pop(&mut self) -> Option<LabeledLocalScope> {
        self.stack.pop()
    }

    pub fn has_previous_loop(&self) -> bool {
        self.stack
            .iter()
            .any(|scope| scope.label().is_valid_for_break_or_continue())
    }

    pub fn add_stack(&mut self, new_scope: LabeledLocalScope) {
        self.new_scope_to_map(&new_scope);
        self.stack.push(new_scope);
    }

    fn new_scope_to_map(&mut self, new_scope: &LabeledLocalScope) {
        macro_rules! stack_back_find {
            ($label:expr, $expect_msg:literal) => {
                self.stack
                    .iter()
                    .rfind(|scope| scope.label() == $label)
                    .expect($expect_msg)
                    .scope()
            };
        }
        let parent = match new_scope.label() {
            ScopeLabel::Match
            | ScopeLabel::None
            | ScopeLabel::Fn
            | ScopeLabel::For
            | ScopeLabel::While
            | ScopeLabel::If => {
                if let Some(last) = self.stack.last() {
                    last.scope()
                } else {
                    LocalScope::default()
                }
            }
            ScopeLabel::Else | ScopeLabel::ElseIf => {
                let scope = stack_back_find!(
                    ScopeLabel::If,
                    "Else and else if must be preceded by an If clause"
                );

                self.scope_map
                    .get(&scope)
                    .expect("We found this scope via previous iteration.")
                    .parent()
                    .expect("An if as always a parent scope.")
            }

            ScopeLabel::Case => stack_back_find!(
                ScopeLabel::Match,
                "Case clauses must be preceded by an Match clause"
            ),
            ScopeLabel::Exit => return,
        };

        let new_scope = new_scope.scope();
        self.scope_map
            .insert(new_scope, ScopeRelation::new(Some(parent)));
        self.scope_map
            .get_mut(&parent)
            .expect("We inserted the parent just before")
            .add_child(new_scope);
    }

    pub fn stack(&self) -> &[LabeledLocalScope] {
        &self.stack
    }
}
