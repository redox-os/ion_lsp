use crate::parsing::local_scope::ScopeNumber;

#[derive(Debug, Clone, Copy)]
pub struct NonZeroDepth(ScopeNumber);

impl NonZeroDepth {
    pub const fn new(value: ScopeNumber) -> Option<Self> {
        if value > 0 {
            Some(Self(value))
        } else {
            None
        }
    }
    pub const fn value(&self) -> usize {
        self.0
    }

    pub const fn minus_one(&self) -> usize {
        // Safety: inner value is greater than one
        self.0 - 1
    }
}
