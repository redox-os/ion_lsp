pub type ScopeMap = HashMap<LocalScope, ScopeRelation>;

pub use scope_relation::ScopeRelation;

mod labeled_local_scope;
mod non_zero_depth;
mod scope_relation;
mod scope_stack;

use non_zero_depth::NonZeroDepth;

use std::collections::HashMap;

use crate::{
    parsing::{
        error_recovery::RecoveryStrategy,
        language_items::{
            item_scopes::{EnterScope, SwitchScope},
            LanguageItem, LanguageItemContext, ScopeLabel,
        },
        local_scope::{LocalScope, ScopeNumber},
        parsing_error::MissingError,
        tracing_utils::emit_object_with_message,
        HasLocation, ParsingError, ParsingErrorKind, ParsingResult, RecoverableError as _,
    },
    Range,
};

use labeled_local_scope::LabeledLocalScope;

use self::scope_stack::ScopeStack;

#[derive(Debug, Default)]
pub struct ScopeManager {
    order_data: Vec<ScopeNumber>,
    current_scopes: ScopeStack,
}

impl ScopeManager {
    pub fn scope_map(&self) -> &ScopeMap {
        self.current_scopes.scope_map()
    }

    pub fn into_scope_map(self) -> ScopeMap {
        self.current_scopes.into_scope_map()
    }

    pub fn there_is_a_previous_match_or_case(&self) -> bool {
        match self.current_scopes.stack().last() {
            None => false,
            Some(last) => matches!(last.label(), ScopeLabel::Match | ScopeLabel::Case),
        }
    }

    pub fn match_comes_before(&self) -> bool {
        self.current_scopes
            .stack()
            .last()
            .map(|scope| scope.label() == ScopeLabel::Match)
            .unwrap_or(false)
    }

    pub fn case_comes_before(&self) -> bool {
        self.current_scopes
            .stack()
            .last()
            .map(|scope| scope.label() == ScopeLabel::Case)
            .unwrap_or(false)
    }

    pub fn check_for_left_over_scopes(&self) -> Result<(), ScopeLabel> {
        if let Some(left_over) = self.current_scopes.stack().last() {
            let label = left_over.label();
            let label = if label == ScopeLabel::Case {
                ScopeLabel::Match
            } else {
                label
            };
            Err(label)
        } else {
            Ok(())
        }
    }

    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(skip(self))
    )]
    pub fn try_turn_to_scoped(&mut self, unscoped: LanguageItem) -> ParsingResult {
        fn introduce_new_order_in(manager: &mut ScopeManager, depth: NonZeroDepth) -> ScopeNumber {
            const FIRST_ORDER_NUMBER: ScopeNumber = 0;
            const INCREMENT_FOR_NEW_ORDER: ScopeNumber = 1;

            let depth = depth.minus_one();
            match manager.order_data.get(depth).copied() {
                Some(last_order) => {
                    let new_last_order = last_order + INCREMENT_FOR_NEW_ORDER;
                    let to_adjust = manager.order_data.get_mut(depth).expect(
                    "Immutable getter was successful. This code would not be executed otherwise",
                );
                    *to_adjust = new_last_order;
                    new_last_order
                }
                None => {
                    manager.order_data.push(FIRST_ORDER_NUMBER);
                    FIRST_ORDER_NUMBER
                }
            }
        }

        fn integrage_new_scope(
            manager: &mut ScopeManager,
            last_scope: LabeledLocalScope,
            new_scope_label: ScopeLabel,
            depth_increment: ScopeNumber,
        ) -> Result<LocalScope, ParsingErrorKind> {
            let new_depth = {
                let current = last_scope.scope().depth();
                let new = current + depth_increment;
                validate_introduced_scope(new_scope_label, last_scope.label(), current, new)?;
                NonZeroDepth::new(new).expect("Is incremented by one")
            };

            let new_order = introduce_new_order_in(manager, new_depth);
            let new_scope = LocalScope::new(new_depth.value(), new_order);
            let new_scope = LabeledLocalScope::new(new_scope_label, new_scope);
            emit_object_with_message!("New Scope", new_scope);
            let new_local_scope = new_scope.scope();
            manager.current_scopes.add_stack(new_scope);
            Ok(new_local_scope)
        }

        fn create_introduce_scope(
            manager: &mut ScopeManager,
            last_scope: LabeledLocalScope,
            new_scope_label: ScopeLabel,
        ) -> Result<LocalScope, ParsingErrorKind> {
            const DEPTH_INCREMENT: ScopeNumber = 1;
            integrage_new_scope(manager, last_scope, new_scope_label, DEPTH_INCREMENT)
        }

        fn switch_in_same_depth(
            manager: &mut ScopeManager,
            last_scope: LabeledLocalScope,
            new_scope_label: ScopeLabel,
        ) -> Result<LocalScope, ParsingErrorKind> {
            integrage_new_scope(manager, last_scope, new_scope_label, 0)
        }

        #[cfg_attr(feature = "ion_shell_parser_trace_parse_phase", tracing::instrument)]
        fn validate_introduced_scope(
            new_scope: ScopeLabel,
            last_scope: ScopeLabel,
            current_depth: ScopeNumber,
            new_depth: ScopeNumber,
        ) -> Result<(), ParsingErrorKind> {
            let new_scope = if current_depth == new_depth {
                new_scope
            } else {
                ScopeLabel::None
            };
            emit_object_with_message!(("new_scope", "last_scope") => (new_scope, last_scope));
            let was_if_or_else_if = matches!(last_scope, ScopeLabel::If | ScopeLabel::ElseIf);

            match (new_scope, last_scope) {
                (ScopeLabel::If, ScopeLabel::If) => return Err(ParsingErrorKind::DuplicateIf),
                (ScopeLabel::Else, ScopeLabel::Else) => {
                    return Err(ParsingErrorKind::DuplicateElse)
                }
                (ScopeLabel::ElseIf, _) if !was_if_or_else_if => {
                    return Err(MissingError::IfForElseIf.into())
                }
                (ScopeLabel::Else, _) if !was_if_or_else_if => {
                    return Err(MissingError::IfForElse.into())
                }
                (ScopeLabel::Exit, _) => (),
                (_, _) => (),
            };
            Ok(())
        }

        fn create_introduce_first_scope(
            manager: &mut ScopeManager,
            new_scope_label: ScopeLabel,
        ) -> Result<LocalScope, ParsingErrorKind> {
            const FIRST_DEPTH: ScopeNumber = 1;
            validate_introduced_scope(new_scope_label, ScopeLabel::None, FIRST_DEPTH, FIRST_DEPTH)?;
            let order = introduce_new_order_in(
                manager,
                NonZeroDepth::new(FIRST_DEPTH).expect("Is always one"),
            );

            let new_scope = LocalScope::new(FIRST_DEPTH, order);
            let labeled = LabeledLocalScope::new(new_scope_label, new_scope);
            let new_local_scope = labeled.scope();
            manager.current_scopes.add_stack(labeled);
            Ok(new_local_scope)
        }

        fn handle_exited_scope(
            manager: &mut ScopeManager,
            unscoped_range: Range,
        ) -> ParsingResult<LocalScope> {
            while let Some(next) = manager.current_scopes.pop().inspect(|_old_scope| {
                emit_object_with_message!("Exited Scope", _old_scope);
            }) {
                match next.label() {
                    ScopeLabel::If
                    | ScopeLabel::For
                    | ScopeLabel::Fn
                    | ScopeLabel::None
                    | ScopeLabel::Match
                    | ScopeLabel::While => return Ok(next.scope()),
                    // Else if and else scope do not  exist without a If scope
                    // Case scope does not  exist without a match scope
                    ScopeLabel::ElseIf | ScopeLabel::Else | ScopeLabel::Case => (),
                    ScopeLabel::Exit => unreachable!(),
                }
            }

            Err(
                ParsingError::new(unscoped_range, MissingError::ScopeForEnd.into())
                    .recoverable_from_next_token_preserved(),
            )
        }

        fn is_valid_right_after_a_match_clause(unscoped: &LanguageItem) -> bool {
            matches!(
                &unscoped,
                LanguageItem::Enter(EnterScope::Case(_))
                    | LanguageItem::Exit(_)
                    | LanguageItem::Comment(_)
            )
        }

        let range = unscoped.location();
        if self.match_comes_before() && !is_valid_right_after_a_match_clause(&unscoped) {
            return Err(
                ParsingError::new(range, ParsingErrorKind::NonCaseAfterMatch)
                    .recoverable_from(RecoveryStrategy::NextEndKeyword.into()),
            );
        }
        let new_scope = match &unscoped {
            LanguageItem::Assigmnet(_)
            | LanguageItem::Export(_)
            | LanguageItem::Command(_)
            | LanguageItem::Comment(_) => Ok(self
                .current_scopes
                .stack()
                .last()
                .map(|e| e.scope())
                .unwrap_or_default()),
            LanguageItem::Continue(_) | LanguageItem::Break(_) => {
                if self.current_scopes.has_previous_loop() {
                    Ok(self
                        .current_scopes
                        .stack()
                        .last()
                        .map(|e| e.scope())
                        .unwrap_or_default())
                } else {
                    let error_kind = if matches!(&unscoped, LanguageItem::Continue(_)) {
                        ParsingErrorKind::NoScopeForContinue
                    } else {
                        ParsingErrorKind::NoScopeForBreak
                    };
                    return Err(ParsingError::new(unscoped.location(), error_kind)
                        .recoverable_from(RecoveryStrategy::NextToken.into())
                        .with_put_last_token_back());
                }
            }
            LanguageItem::Enter(_) => match self.current_scopes.stack().last().copied() {
                Some(last_scope) => create_introduce_scope(self, last_scope, (&unscoped).into())
                    .map_err(|error| ParsingError::new(range, error)),
                None => create_introduce_first_scope(self, (&unscoped).into())
                    .map_err(|error| ParsingError::new(range, error)),
            },
            LanguageItem::Exit(range) => handle_exited_scope(self, range.location()),
            LanguageItem::Switch(ctx) => match self.current_scopes.stack().last().copied() {
                Some(last_scope) => switch_in_same_depth(self, last_scope, (&unscoped).into())
                    .map_err(|error| ParsingError::new(range, error)),
                None => match ctx {
                    SwitchScope::ElseIf(_) => {
                        Err(ParsingError::new(range, MissingError::IfForElseIf.into()))
                            .recoverable_from(RecoveryStrategy::ToNextConditionalClauseOrEnd.into())
                            .with_put_last_token_back()
                    }
                    SwitchScope::Else(_) => {
                        Err(ParsingError::new(range, MissingError::IfForElse.into()))
                            .recoverable_from(RecoveryStrategy::ToNextConditionalClauseOrEnd.into())
                            .with_put_last_token_back()
                    }
                    SwitchScope::Case(_) => unreachable!(),
                },
            },
        }
        .fallback_recoverable_from_next_end_keyword()?;

        Ok(LanguageItemContext::new(unscoped, new_scope))
    }
}

#[cfg(test)]
mod testing {
    use crate::{parsing::IonParser, IonTokenizer};

    use super::*;

    fn setup(input: &str) -> (Vec<LanguageItemContext>, Vec<(LocalScope, ScopeRelation)>) {
        let tokens = IonTokenizer::new(input);
        let mut parser = IonParser::new(tokens, input);
        let items: Result<Vec<LanguageItemContext>, ParsingError> = parser.by_ref().collect();
        let items: Vec<LanguageItemContext> = items.expect("input should contain no errors.");
        let mut map = parser
            .into_scope_map()
            .into_iter()
            .collect::<Vec<(LocalScope, ScopeRelation)>>();
        map.sort_by_key(|(k, _)| k.clone());
        (items, map)
    }

    #[test]
    fn scope_map_clause_scopes() {
        const INPUT: &str = include_str!("clause_scopes.txt");
        let (_, map) = setup(INPUT);
        insta::assert_debug_snapshot!(map);
    }
}
