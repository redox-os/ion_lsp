pub type ScopeNumber = usize;
#[derive(Debug, PartialEq, Eq, Clone, Copy, Default, Hash, PartialOrd, Ord)]
pub struct LocalScope {
    depth: ScopeNumber,
    order: ScopeNumber,
}

impl LocalScope {
    pub fn new(depth: ScopeNumber, order: ScopeNumber) -> Self {
        Self { depth, order }
    }

    pub fn depth(&self) -> ScopeNumber {
        self.depth
    }

    pub fn order(&self) -> ScopeNumber {
        self.order
    }
}
