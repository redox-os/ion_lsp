pub type OptErrRecovery = Option<ChosenErrRecoveryStrategy>;
pub type RefOptErrRecovery<'a> = Option<&'a ChosenErrRecoveryStrategy>;

use std::fmt::Debug;

use derive_more::Display;

use crate::tokens::TokenKind;

#[derive(Debug, PartialEq, Clone)]
pub struct ChosenErrRecoveryStrategy {
    just_peek: bool,
    strategy: RecoveryStrategy,
}

#[derive(Debug, PartialEq, Display, Clone)]
pub enum RecoveryStrategy {
    #[display(fmt = "Recovering from error by resuming after next end of statement")]
    EndOfStatement,
    #[display(fmt = "Recovering from error by resuming after any next token")]
    NextToken,
    #[display(fmt = "No recovering, just go to the end of the program")]
    ToEndOfProgram,
    #[display(fmt = "Recovering from error by resuming to next `end` token")]
    NextEndKeyword,
    #[display(fmt = "Recovering from error by resuming to next `else|else if or end` token")]
    ToNextConditionalClauseOrEnd,
}

impl From<RecoveryStrategy> for ChosenErrRecoveryStrategy {
    fn from(value: RecoveryStrategy) -> Self {
        Self::new(value)
    }
}

impl ChosenErrRecoveryStrategy {
    pub fn new(strategy: RecoveryStrategy) -> Self {
        Self {
            just_peek: false,
            strategy,
        }
    }

    pub fn with_just_peek(mut self) -> Self {
        self.just_peek = true;
        self
    }

    pub fn is_recovery_point(&self, kind: TokenKind) -> bool {
        match self.strategy {
            RecoveryStrategy::EndOfStatement => kind.is_end_of_statement(),
            RecoveryStrategy::NextToken => true,
            RecoveryStrategy::ToEndOfProgram => false,
            RecoveryStrategy::NextEndKeyword => kind == TokenKind::End,
            RecoveryStrategy::ToNextConditionalClauseOrEnd => {
                matches!(kind, TokenKind::ElseIf | TokenKind::Else | TokenKind::End)
            }
        }
    }

    pub fn put_last_token_back(&self) -> bool {
        self.just_peek
    }
}
