use crate::{
    macros::gen_impl_dbg_txt,
    parsing::{HasLocation, RangeDebugText},
    Range,
};

#[derive(Debug, PartialEq)]
pub struct MathExpression(Option<RangeDebugText>);

impl MathExpression {
    pub fn new(value: Option<Range>) -> Self {
        let value = value.map(RangeDebugText::from);
        Self(value)
    }

    pub fn inner(&self) -> Option<Range> {
        self.0.as_ref().map(HasLocation::location)
    }
}

gen_impl_dbg_txt! {MathExpression ?}
