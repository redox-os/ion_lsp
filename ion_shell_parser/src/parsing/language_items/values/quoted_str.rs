use crate::{
    macros::gen_impl_dbg_txt,
    parsing::RangeDebugText,
    prelude::{
        variables::{VariableReference, VariableReferenceContext},
        HasVariableReference, HasWarnings, ParsingError,
    },
};

#[derive(Debug, PartialEq)]
pub enum QuotedStr {
    Simple(Option<RangeDebugText>),
    Complex(ComplexQuotedStr),
}

impl HasWarnings for QuotedStr {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&ParsingError)) {
        if let Self::Complex(seq) = self {
            seq.all_warnings(on_found)
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct ComplexQuotedStr {
    components: Vec<QuotedStrComponent>,
    warnings: Vec<ParsingError>,
}

gen_impl_dbg_txt! {ComplexQuotedStr, components, warnings}

impl ComplexQuotedStr {
    pub fn new(components: Vec<QuotedStrComponent>, warnings: Vec<ParsingError>) -> Self {
        let warnings = warnings
            .into_iter()
            .map(|as_error| as_error.as_warning())
            .collect();
        Self {
            components,
            warnings,
        }
    }

    pub fn components(&self) -> &[QuotedStrComponent] {
        &self.components
    }

    pub fn warnings(&self) -> &[ParsingError] {
        &self.warnings
    }
}

impl HasWarnings for ComplexQuotedStr {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&ParsingError)) {
        self.warnings().iter().for_each(on_found)
    }
}

impl HasVariableReference for ComplexQuotedStr {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.components.as_slice().var_references(on_found)
    }
}

#[derive(Debug, PartialEq)]
pub enum QuotedStrComponent {
    Range(RangeDebugText),
    Variable(VariableReferenceContext),
}

impl HasVariableReference for QuotedStrComponent {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        if let Self::Variable(var) = self {
            var.var_references(on_found)
        }
    }
}

gen_impl_dbg_txt! {QuotedStrComponent, Variable => ctx, Range => ctx}
