use crate::{macros::gen_impl_dbg_txt, prelude::HasVariableReference};

use super::ValueContext;

#[derive(Debug, PartialEq)]
pub enum UnQuoatedStr {
    Plain,
    Complex(Vec<ValueContext>),
}

impl HasVariableReference for UnQuoatedStr {
    fn var_references(
        &self,
        on_found: &mut dyn FnMut(&crate::prelude::variables::VariableReference),
    ) {
        if let Self::Complex(ctx) = self {
            ctx.as_slice().var_references(on_found)
        }
    }
}

gen_impl_dbg_txt! {UnQuoatedStr, Complex => ctx}
