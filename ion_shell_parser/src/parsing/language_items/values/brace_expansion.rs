use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{prelude::*, RangeDebugText},
    Range,
};
#[derive(Debug, PartialEq)]
pub struct BraceSliceExpansion {
    range: RangeDebugText,
    kind: SliceKind,
    start: ValueContext,
    end: ValueContext,
}

gen_impl_dbg_txt! {BraceSliceExpansion, range, start, end}
gen_impl_has_location! {BraceSliceExpansion, range}

impl BraceSliceExpansion {
    pub fn new(range: Range, kind: SliceKind, start: ValueContext, end: ValueContext) -> Self {
        let range = range.into();
        Self {
            range,
            kind,
            start,
            end,
        }
    }

    pub fn kind(&self) -> SliceKind {
        self.kind
    }

    pub fn start(&self) -> &ValueContext {
        &self.start
    }

    pub fn end(&self) -> &ValueContext {
        &self.end
    }
}

impl HasVariableReference for BraceSliceExpansion {
    fn var_references(&self, on_found: &mut dyn FnMut(&variables::VariableReference)) {
        self.start.var_references(on_found);
        self.end.var_references(on_found);
    }
}

#[derive(Debug, PartialEq)]
pub enum BraceExpansionComponent {
    Value(ValueContext),
    SliceGenerator(BraceSliceExpansion),
}

impl HasVariableReference for BraceExpansionComponent {
    fn var_references(&self, on_found: &mut dyn FnMut(&variables::VariableReference)) {
        match self {
            BraceExpansionComponent::Value(ctx) => ctx.var_references(on_found),
            BraceExpansionComponent::SliceGenerator(ctx) => ctx.var_references(on_found),
        }
    }
}

gen_impl_dbg_txt! {BraceExpansionComponent, Value => ctx, SliceGenerator => ctx}

#[derive(Debug, PartialEq)]
pub struct BraceExpansionContext(Vec<BraceExpansionComponent>);

impl HasVariableReference for BraceExpansionContext {
    fn var_references(&self, on_found: &mut dyn FnMut(&variables::VariableReference)) {
        self.0.as_slice().var_references(on_found)
    }
}

gen_impl_dbg_txt! {BraceExpansionContext}

impl BraceExpansionContext {
    pub fn new(components: Vec<BraceExpansionComponent>) -> Self {
        Self(components)
    }
}
