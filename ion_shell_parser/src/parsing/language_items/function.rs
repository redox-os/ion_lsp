use crate::{
    macros::gen_impl_dbg_txt,
    parsing::{matched_fragments::MatchedFn, whole_range_from_seq, HasLocation, RangeDebugText},
    Range,
};

use super::{
    comment::DocString,
    variables::{VariableDeclaration, VariableName},
    HasVariableDeclaration,
};

#[derive(PartialEq, Debug, Clone)]
pub struct FunctionDeclaration {
    keyword: RangeDebugText,
    name: VariableName,
    arguments: Vec<VariableDeclaration>,
    docstring: Option<DocString>,
}

impl FunctionDeclaration {
    pub fn new(
        keyword: &MatchedFn,
        name: VariableName,
        arguments: Vec<VariableDeclaration>,
        docstring: Option<DocString>,
    ) -> Self {
        let keyword = keyword.location().into();
        Self {
            keyword,
            name,
            arguments,
            docstring,
        }
    }

    pub fn arguments(&self) -> &[VariableDeclaration] {
        &self.arguments
    }

    pub fn docstring(&self) -> Option<&DocString> {
        self.docstring.as_ref()
    }

    pub fn name(&self) -> &VariableName {
        &self.name
    }
}

impl HasVariableDeclaration for FunctionDeclaration {
    fn var_declaration(&self, on_found: &mut dyn FnMut(super::FoundVariableDeclaration)) {
        self.arguments().var_declaration(on_found);
    }
}

gen_impl_dbg_txt! {FunctionDeclaration # keyword, name @ arguments, docstring}

impl HasLocation for FunctionDeclaration {
    fn location(&self) -> Range {
        let start = self.keyword.location();
        let end = whole_range_from_seq(&self.arguments).unwrap_or_else(|| self.name.location());
        Range::from_start_end_internal(start, end)
    }
}
