pub use command_expression::{
    CommandExpression, FileInteractionWithCommand, FileRedirectContext, RawCommandExpression,
};
pub use executable_builin::{Builtin, ExecutableOrBuiltin};
pub use pipe_command::{NextPipedCommand, PipeOfCommands, PipeOperatorContext};

mod command_expression;
mod executable_builin;
mod pipe_command;

use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{whole_range_from_seq, HasLocation, RangeDebugText},
    Range,
};

use super::{
    command_operators::{DetachOperator, LogicalChainOperator},
    language_item::CanHaveSourceBuiltin,
    variables::VariableReference,
    FoundSourceBuiltin, HasCommandInvocation, HasVariableReference, HasWarnings,
};

#[derive(Debug, PartialEq)]
pub struct CommandChain {
    first: PipeOfCommands,
    rest: Vec<NextCommand>,
    detach: Option<DetachOperatorContext>,
}

gen_impl_dbg_txt! {CommandChain # first, detach @ rest}
impl HasLocation for CommandChain {
    fn location(&self) -> Range {
        let start = self.first.location();
        let end = self
            .detach
            .as_ref()
            .map(|e| e.location())
            .unwrap_or_else(|| whole_range_from_seq(&self.rest).unwrap_or(start));
        Range::from_start_end_internal(start, end)
    }
}
impl HasWarnings for CommandChain {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.first().all_warnings(on_found);
        self.rest().all_warnings(on_found);
    }
}

impl HasCommandInvocation for CommandChain {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.first.command_invocation(on_found);
        self.rest.as_slice().command_invocation(on_found);
    }
}

impl HasVariableReference for CommandChain {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.first.var_references(on_found);
        self.rest().var_references(on_found);
    }
}

impl CanHaveSourceBuiltin for CommandChain {
    fn source_calls(&self, mut on_found: impl FnMut(FoundSourceBuiltin)) {
        self.first.source_calls(&mut on_found);
        self.rest
            .iter()
            .for_each(|next| next.source_calls(&mut on_found));
    }
}

impl CommandChain {
    pub fn new(
        first: PipeOfCommands,
        rest: Vec<NextCommand>,
        detach: Option<DetachOperatorContext>,
    ) -> Self {
        Self {
            first,
            rest,
            detach,
        }
    }

    pub fn first(&self) -> &PipeOfCommands {
        &self.first
    }

    pub fn rest(&self) -> &[NextCommand] {
        &self.rest
    }

    pub fn detach(&self) -> Option<&DetachOperatorContext> {
        self.detach.as_ref()
    }
}

#[derive(Debug, PartialEq)]
pub struct NextCommand {
    chain_operator: ChainLogicalOperatorContext,
    command: PipeOfCommands,
}

impl HasWarnings for NextCommand {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.command.all_warnings(on_found);
    }
}

impl HasCommandInvocation for NextCommand {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.command.command_invocation(on_found);
    }
}

impl HasVariableReference for NextCommand {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.command.var_references(on_found);
    }
}

impl CanHaveSourceBuiltin for NextCommand {
    fn source_calls(&self, on_found: impl FnMut(super::language_item::FoundSourceBuiltin)) {
        self.command.source_calls(on_found);
    }
}

impl NextCommand {
    pub fn new(chain_operator: ChainLogicalOperatorContext, command: PipeOfCommands) -> Self {
        Self {
            chain_operator,
            command,
        }
    }
}

gen_impl_dbg_txt! {NextCommand, command, chain_operator}
gen_impl_has_location! {NextCommand, chain_operator <=> command}

#[derive(Debug, PartialEq)]
pub struct DetachOperatorContext {
    range: RangeDebugText,
    operator: DetachOperator,
}

impl DetachOperatorContext {
    pub fn new(range: Range, operator: DetachOperator) -> Self {
        let range = range.into();
        Self { range, operator }
    }

    pub fn operator(&self) -> DetachOperator {
        self.operator
    }
}

gen_impl_dbg_txt! {DetachOperatorContext, range}
gen_impl_has_location! {DetachOperatorContext, range}

#[derive(Debug, PartialEq)]
pub struct ChainLogicalOperatorContext {
    range: RangeDebugText,
    chain_operator: LogicalChainOperator,
}

impl ChainLogicalOperatorContext {
    pub fn new(range: Range, chain_operator: LogicalChainOperator) -> Self {
        let range = range.into();
        Self {
            range,
            chain_operator,
        }
    }

    pub fn chain_operator(&self) -> LogicalChainOperator {
        self.chain_operator
    }
}

gen_impl_dbg_txt! {ChainLogicalOperatorContext, range}
gen_impl_has_location! {ChainLogicalOperatorContext, range}
