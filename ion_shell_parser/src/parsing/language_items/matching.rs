use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{HasLocation, RangeDebugText},
    Range,
};

use super::{
    item_scopes::ScopeCondition, variables::VariableReference, HasCommandInvocation,
    HasVariableReference, HasWarnings, ValueContext,
};

#[derive(Debug, PartialEq)]
pub struct Match {
    range_of_match_keyword: RangeDebugText,
    value: ValueContext,
}

impl Match {
    pub fn new(range_of_match_keyword: Range, value: ValueContext) -> Self {
        let range_of_match_keyword = range_of_match_keyword.into();
        Self {
            range_of_match_keyword,
            value,
        }
    }

    pub fn value(&self) -> &ValueContext {
        &self.value
    }
}

impl HasWarnings for Match {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.value.all_warnings(on_found);
    }
}

impl HasCommandInvocation for Match {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.value.command_invocation(on_found);
    }
}

impl HasVariableReference for Match {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.value.var_references(on_found);
    }
}

gen_impl_has_location! {Match, range_of_match_keyword <=> value}
gen_impl_dbg_txt! {Match, range_of_match_keyword, value}

#[derive(Debug, PartialEq)]
pub struct Case {
    range: RangeDebugText,
    value: ValueContext,
    guard: Option<IfGuard>,
}

impl Case {
    pub fn new(range: Range, value: ValueContext, guard: Option<IfGuard>) -> Self {
        let range = range.into();
        Self {
            range,
            value,
            guard,
        }
    }

    pub fn value(&self) -> &ValueContext {
        &self.value
    }

    pub fn guard(&self) -> Option<&IfGuard> {
        self.guard.as_ref()
    }
}

impl HasWarnings for Case {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.value.all_warnings(on_found);
        self.guard
            .as_ref()
            .inspect(|element| element.all_warnings(on_found));
    }
}

impl HasCommandInvocation for Case {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.value.command_invocation(on_found);
        self.guard.as_ref().command_invocation(on_found);
    }
}

impl HasVariableReference for Case {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.value.var_references(on_found);
        self.guard.var_references(on_found);
    }
}

impl HasLocation for Case {
    fn location(&self) -> Range {
        let start = self.range.location();
        let end = self
            .guard
            .as_ref()
            .map(|to_location| to_location.location())
            .unwrap_or(self.value.location());
        Range::from_start_end_internal(start, end)
    }
}
gen_impl_dbg_txt! {Case ? guard ! range, value  }

#[derive(Debug, PartialEq)]
pub struct IfGuard(ScopeCondition);

impl HasWarnings for IfGuard {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.0.all_warnings(on_found);
    }
}

impl HasCommandInvocation for IfGuard {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.0.command_invocation(on_found);
    }
}

impl HasVariableReference for IfGuard {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.0.var_references(on_found);
    }
}

gen_impl_dbg_txt! {IfGuard}
gen_impl_has_location! {IfGuard}

impl IfGuard {
    pub fn new(command: ScopeCondition) -> Self {
        Self(command)
    }
}
