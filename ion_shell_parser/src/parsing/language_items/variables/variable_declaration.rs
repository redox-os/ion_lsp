use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::tracing_utils,
    prelude::{FoundVariableDeclaration, HasVariableDeclaration},
};

use super::{TypeAnnotationContext, VariableName};

#[derive(Debug, PartialEq, Clone)]
pub struct VariableDeclaration {
    name: VariableName,
    known_type: Option<TypeAnnotationContext>,
}

gen_impl_dbg_txt! {VariableDeclaration ? known_type ! name}
gen_impl_has_location! {VariableDeclaration, name ? known_type}

impl VariableDeclaration {
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(name = "new_variable_declaration")
    )]
    pub fn new(name: VariableName, known_type: Option<TypeAnnotationContext>) -> Self {
        tracing_utils::emit_message!("Variable declaration detected");
        Self { name, known_type }
    }

    pub fn name(&self) -> &VariableName {
        &self.name
    }

    pub fn known_type(&self) -> Option<&TypeAnnotationContext> {
        self.known_type.as_ref()
    }
}

impl HasVariableDeclaration for VariableDeclaration {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        on_found(FoundVariableDeclaration::Annotated(self));
    }
}
