use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{HasLocation, RangeDebugText, SigilKind},
    prelude::{HasVariableReference, IndexAccessContext, Value, ValueContext},
    range::Range,
    tokens::Namespace,
};
use derive_more::Into;

use super::VariableName;

#[derive(Debug, PartialEq, Into)]
pub struct VariableReferenceContext {
    whole: RangeDebugText,
    reference: VariableReference,
}

gen_impl_dbg_txt! {VariableReferenceContext, reference, whole}

impl VariableReferenceContext {
    pub fn new(whole: Range, reference: VariableReference) -> Self {
        let whole = whole.into();
        Self { whole, reference }
    }

    pub fn reference(&self) -> &VariableReference {
        &self.reference
    }
}

impl HasVariableReference for VariableReferenceContext {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        on_found(self.reference())
    }
}

impl From<VariableReferenceContext> for ValueContext {
    fn from(value: VariableReferenceContext) -> Self {
        let (range, reference) = value.into();
        ValueContext::new(range.location(), Value::VariableReference(reference))
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct NamespaceContext {
    range: RangeDebugText,
    namespace: Namespace,
}

gen_impl_dbg_txt! {NamespaceContext, range}
gen_impl_has_location! {NamespaceContext, range}

impl NamespaceContext {
    pub fn new(range: Range, namespace: Namespace) -> Self {
        let range = range.into();
        Self { range, namespace }
    }

    pub fn namespace(&self) -> Namespace {
        self.namespace
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct VariableReference {
    name: VariableName,
    namespace: Option<NamespaceContext>,
    index: Option<IndexAccessContext>,
    kind: SigilKind,
}

gen_impl_dbg_txt! {VariableReference ? index, namespace ! name}
impl HasLocation for VariableReference {
    fn location(&self) -> Range {
        let start = self.name.location();
        let end = self
            .index
            .as_ref()
            .map(|e| e.location())
            .unwrap_or_else(|| {
                self.namespace
                    .as_ref()
                    .map(|e| e.location())
                    .unwrap_or(start)
            });
        Range::from_start_end_internal(start, end)
    }
}

impl VariableReference {
    pub fn new(
        name: VariableName,
        kind: SigilKind,
        namespace: Option<NamespaceContext>,
        index: Option<IndexAccessContext>,
    ) -> Self {
        Self {
            name,
            index,
            namespace,
            kind,
        }
    }

    pub fn name(&self) -> &VariableName {
        &self.name
    }

    pub fn kind(&self) -> SigilKind {
        self.kind
    }

    pub fn index(&self) -> Option<&IndexAccessContext> {
        self.index.as_ref()
    }
}

impl From<VariableReference> for VariableReferenceContext {
    fn from(value: VariableReference) -> Self {
        VariableReferenceContext::new(value.location(), value)
    }
}
