use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::RangeDebugText,
    range::Range,
    tokens::TypeAnnotation,
};

#[derive(PartialEq, Debug, Clone)]
pub struct TypeAnnotationContext {
    range: RangeDebugText,
    kind: TypeAnnotation,
}

gen_impl_dbg_txt! {TypeAnnotationContext, range}
gen_impl_has_location! {TypeAnnotationContext, range}

impl TypeAnnotationContext {
    pub fn new(range: Range, kind: TypeAnnotation) -> Self {
        let range = range.into();
        Self { range, kind }
    }

    pub fn kind(&self) -> TypeAnnotation {
        self.kind
    }
}
