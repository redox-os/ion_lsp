use crate::{
    macros::gen_impl_dbg_txt,
    parsing::{language_items::Value, matched_fragments::SigilKind},
    Range,
};

use super::{variables::VariableName, ValueContext};

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum MethodKind {
    String,
    Array,
}

impl From<MethodKind> for SigilKind {
    fn from(value: MethodKind) -> Self {
        match value {
            MethodKind::String => SigilKind::String,
            MethodKind::Array => SigilKind::Array,
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Method {
    kind: MethodKind,
    name: VariableName,
    args: Vec<ValueContext>,
}

gen_impl_dbg_txt! {Method # name @ args}

impl Method {
    pub fn new(kind: MethodKind, name: VariableName, args: Vec<ValueContext>) -> Self {
        Self { kind, name, args }
    }

    pub fn name(&self) -> &VariableName {
        &self.name
    }

    pub fn kind(&self) -> MethodKind {
        self.kind
    }

    pub fn args(&self) -> &[ValueContext] {
        &self.args
    }
}

impl From<(Method, Range)> for ValueContext {
    fn from((method, range): (Method, Range)) -> Self {
        let method = Value::Method(method);
        ValueContext::new(range, method)
    }
}
