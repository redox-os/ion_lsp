use super::variables::VariableReference;

pub trait HasVariableReference {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference));
}

impl<T> HasVariableReference for &[T]
where
    T: HasVariableReference,
{
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.iter()
            .for_each(|element| element.var_references(on_found));
    }
}

impl<T> HasVariableReference for Option<T>
where
    T: HasVariableReference,
{
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        if let Some(inner) = self {
            inner.var_references(on_found);
        }
    }
}

#[cfg(test)]
mod testing {
    use crate::{
        parsing::{test_utils, HasLocation, LocalScope},
        LineColumn,
    };

    use super::*;

    #[test]
    fn find_all_variable_references() {
        const INPUT: &str = include_str!("test_input_with_idents.txt");
        let (actual, mapping) = test_utils::act_parsing_without_error_mapping(INPUT);

        let all_found: Vec<_> = actual
            .iter()
            .filter_map(|element| {
                let mut seq: Vec<(LocalScope, LineColumn, VariableReference)> = Vec::default();
                element.var_references(&mut |dec| {
                    seq.push((
                        element.scope(),
                        mapping
                            .get_line_column(INPUT, dec.location().start_pos())
                            .unwrap(),
                        dec.clone(),
                    ))
                });
                if !seq.is_empty() {
                    Some(seq)
                } else {
                    None
                }
            })
            .flatten()
            .collect();

        insta::assert_debug_snapshot!(all_found);
    }
}
