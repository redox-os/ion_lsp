use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{HasLocation, RangeDebugText},
    Range, ToRange,
};

use super::{
    assignment::{AssignmentContext, ExportContext},
    builtins::SourceBuiltin,
    commands::CommandChain,
    item_scopes::{EnterScope, SwitchScope},
    variables::VariableReference,
    Comment, HasCommandInvocation, HasVariableDeclaration, HasVariableReference, HasWarnings,
};

#[derive(Debug, PartialEq)]
pub enum LanguageItem {
    Assigmnet(AssignmentContext),
    Export(ExportContext),
    Command(CommandChain),
    Comment(Comment),
    Enter(EnterScope),
    Switch(SwitchScope),
    Exit(RangeDebugText),
    Continue(RangeDebugText),
    Break(RangeDebugText),
}

impl HasWarnings for LanguageItem {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        match self {
            LanguageItem::Exit(_) | LanguageItem::Comment(_) => (),
            LanguageItem::Assigmnet(ctx) => ctx.all_warnings(on_found),
            LanguageItem::Export(ctx) => ctx.all_warnings(on_found),
            LanguageItem::Command(command) => command.all_warnings(on_found),
            LanguageItem::Enter(ctx) => ctx.all_warnings(on_found),
            LanguageItem::Switch(ctx) => ctx.all_warnings(on_found),
            _ => (),
        }
    }
}

impl HasCommandInvocation for LanguageItem {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        match self {
            LanguageItem::Command(command) => command.command_invocation(on_found),
            LanguageItem::Assigmnet(command) => command.command_invocation(on_found),
            LanguageItem::Export(command) => command.command_invocation(on_found),
            LanguageItem::Enter(command) => command.command_invocation(on_found),
            LanguageItem::Switch(command) => command.command_invocation(on_found),
            _ => (),
        }
    }
}

impl HasVariableDeclaration for LanguageItem {
    fn var_declaration(&self, on_found: &mut dyn FnMut(super::FoundVariableDeclaration)) {
        match self {
            LanguageItem::Assigmnet(ctx) => ctx.var_declaration(on_found),
            LanguageItem::Export(ctx) => ctx.var_declaration(on_found),
            LanguageItem::Enter(ctx) => ctx.var_declaration(on_found),
            LanguageItem::Command(_)
            | LanguageItem::Continue(_)
            | LanguageItem::Break(_)
            | LanguageItem::Comment(_)
            | LanguageItem::Switch(_)
            | LanguageItem::Exit(_) => (),
        }
    }
}

impl HasVariableReference for LanguageItem {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        match self {
            Self::Command(command) => command.var_references(on_found),
            Self::Assigmnet(assignment) => assignment.var_references(on_found),
            Self::Export(export) => export.var_references(on_found),
            Self::Enter(enter) => enter.var_references(on_found),
            Self::Switch(switch) => switch.var_references(on_found),
            Self::Exit(_) | Self::Comment(_) | Self::Break(_) | Self::Continue(_) => (),
        }
    }
}

gen_impl_has_location! {LanguageItem,
    Export => export,
    Command => command,
    Comment => comment,
    Enter => scope,
    Switch => scope,
    Exit => exit,
    Assigmnet => assignment,
    Continue => ctx,
    Break => ctx
}

gen_impl_dbg_txt! {LanguageItem,
    Assigmnet => assignment,
    Export => export,
    Command => command,
    Comment => comment,
    Enter => scope,
    Switch => scope,
    Exit => exit,
    Continue => ctx,
    Break => ctx
}

use derive_more::Display;
#[derive(Debug, PartialEq, Eq, Clone, Copy, Display)]
pub enum ScopeLabel {
    Exit,
    If,
    Else,
    ElseIf,
    For,
    None,
    While,
    Fn,
    Case,
    Match,
}

impl ScopeLabel {
    pub fn is_valid_for_break_or_continue(&self) -> bool {
        !matches!(self, Self::None | Self::Exit)
    }
}

impl From<&LanguageItem> for ScopeLabel {
    fn from(value: &LanguageItem) -> Self {
        match value {
            LanguageItem::Assigmnet(_)
            | LanguageItem::Export(_)
            | LanguageItem::Command(_)
            | LanguageItem::Comment(_)
            | LanguageItem::Continue(_)
            | LanguageItem::Break(_) => Self::None,
            LanguageItem::Exit(_) => Self::Exit,
            LanguageItem::Enter(EnterScope::For(_)) => Self::For,
            LanguageItem::Enter(EnterScope::Function(_)) => Self::Fn,
            LanguageItem::Enter(EnterScope::Match(_)) => Self::Match,
            LanguageItem::Enter(EnterScope::Case(_)) => Self::Case,
            LanguageItem::Enter(EnterScope::While(_)) => Self::While,
            LanguageItem::Enter(_) => Self::If,
            LanguageItem::Switch(switch_ctx) => match switch_ctx {
                SwitchScope::ElseIf(_) => Self::ElseIf,
                SwitchScope::Else(_) => Self::Else,
                SwitchScope::Case(_) => Self::Case,
            },
        }
    }
}

pub trait CanHaveSourceBuiltin {
    fn source_calls(&self, on_found: impl FnMut(FoundSourceBuiltin));
}

#[derive(Debug)]
pub struct FoundSourceBuiltin<'a> {
    source_command_range: Range,
    to_be_sourced: &'a SourceBuiltin,
}

impl<'a> HasLocation for FoundSourceBuiltin<'a> {
    fn location(&self) -> crate::Range {
        ToRange::new((&self.source_command_range, self.to_be_sourced)).into()
    }
}

impl<'a> FoundSourceBuiltin<'a> {
    pub fn new(range: Range, to_be_sourced: &'a SourceBuiltin) -> Self {
        Self {
            source_command_range: range,
            to_be_sourced,
        }
    }

    pub fn to_be_sourced(&self) -> &SourceBuiltin {
        self.to_be_sourced
    }

    pub fn source_command_range(&self) -> Range {
        self.source_command_range
    }
}

impl CanHaveSourceBuiltin for LanguageItem {
    fn source_calls(&self, on_found: impl FnMut(FoundSourceBuiltin)) {
        if let LanguageItem::Command(chain) = self {
            chain.source_calls(on_found)
        }
    }
}
