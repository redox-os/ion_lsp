use derive_more::{From, Into};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum LogicalChainOperator {
    And,
    Or,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, From, Into)]
pub struct PipeOperator(OutputChannels);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum FileRedirect {
    Output(FileOutputRedirection),
    Input,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct FileOutputRedirection {
    append: bool,
    output_kind: OutputChannels,
}

impl FileOutputRedirection {
    fn new(append: bool, output_kind: OutputChannels) -> Self {
        Self {
            append,
            output_kind,
        }
    }
    pub fn new_no_append(output_kind: OutputChannels) -> Self {
        Self::new(false, output_kind)
    }
    pub fn new_append(output_kind: OutputChannels) -> Self {
        Self::new(true, output_kind)
    }

    pub fn append(&self) -> bool {
        self.append
    }

    pub fn output_kind(&self) -> OutputChannels {
        self.output_kind
    }
}

/// These operators serve detaching processes.
/// They do not require CommandExpression after.
///
/// [Link](https://doc.redox-os.org/ion-manual/pipelines.html#detaching-processes)
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DetachOperator {
    ToBackGround,
    Disown,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum OutputChannels {
    StdOut,
    Err,
    Both,
}
