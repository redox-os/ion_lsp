use crate::{parsing::HasLocation, tokens::TypeAnnotation};

use super::variables::{TypeAnnotationContext, VariableDeclaration, VariableName};

pub trait HasVariableDeclaration {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration));
}

#[derive(Debug, Clone, Copy)]
pub enum FoundVariableDeclaration<'a> {
    Annotated(&'a VariableDeclaration),
    NotAnnotated(&'a VariableName),
}

impl<'a> FoundVariableDeclaration<'a> {
    pub fn var_name(&self) -> &VariableName {
        match self {
            FoundVariableDeclaration::Annotated(annotated) => annotated.name(),
            FoundVariableDeclaration::NotAnnotated(var_name) => var_name,
        }
    }
}

impl<'a> HasLocation for FoundVariableDeclaration<'a> {
    fn location(&self) -> crate::Range {
        match self {
            Self::Annotated(ctx) => ctx.location(),
            Self::NotAnnotated(ctx) => ctx.location(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum OwnedFoundVariableDeclaration {
    Annotated(VariableDeclaration),
    NotAnnotated(VariableName),
}
impl OwnedFoundVariableDeclaration {
    pub fn var_name(&self) -> &VariableName {
        match self {
            Self::Annotated(annotated) => annotated.name(),
            Self::NotAnnotated(var_name) => var_name,
        }
    }
    pub fn try_get_type(&self) -> Option<TypeAnnotation> {
        if let Self::Annotated(ctx) = self {
            ctx.known_type().map(TypeAnnotationContext::kind)
        } else {
            None
        }
    }
}

impl HasLocation for OwnedFoundVariableDeclaration {
    fn location(&self) -> crate::Range {
        match self {
            Self::Annotated(ctx) => ctx.location(),
            Self::NotAnnotated(ctx) => ctx.location(),
        }
    }
}

impl From<FoundVariableDeclaration<'_>> for OwnedFoundVariableDeclaration {
    fn from(value: FoundVariableDeclaration<'_>) -> Self {
        match value {
            FoundVariableDeclaration::Annotated(ctx) => {
                OwnedFoundVariableDeclaration::Annotated(ctx.clone())
            }
            FoundVariableDeclaration::NotAnnotated(ctx) => {
                OwnedFoundVariableDeclaration::NotAnnotated(ctx.clone())
            }
        }
    }
}

impl<T> HasVariableDeclaration for &[T]
where
    T: HasVariableDeclaration,
{
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        self.iter()
            .for_each(|element| element.var_declaration(on_found));
    }
}

#[cfg(test)]
mod testing {
    use crate::{
        parsing::{test_utils, HasLocation, LocalScope},
        LineColumn,
    };

    use super::*;

    #[test]
    fn find_all_variable_declarations() {
        const INPUT: &str = include_str!("test_input_with_idents.txt");
        let (actual, mapping) = test_utils::act_parsing_without_error_mapping(INPUT);

        let all_found: Vec<_> = actual
            .iter()
            .filter_map(|element| {
                let mut seq: Vec<(LocalScope, LineColumn, OwnedFoundVariableDeclaration)> =
                    Vec::default();
                element.var_declaration(&mut |dec| {
                    seq.push((
                        element.scope(),
                        mapping
                            .get_line_column(INPUT, dec.location().start_pos())
                            .unwrap(),
                        dec.into(),
                    ))
                });
                if !seq.is_empty() {
                    Some(seq)
                } else {
                    None
                }
            })
            .flatten()
            .collect();

        insta::assert_debug_snapshot!(all_found);
    }
}
