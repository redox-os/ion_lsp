use crate::macros::{gen_impl_dbg_txt, gen_impl_has_location};

use super::{
    commands::CommandChain, variables::VariableReference, HasCommandInvocation,
    HasVariableReference,
};

#[derive(Debug, PartialEq)]
pub enum CommandExpansionKind {
    String,
    Array,
}

#[derive(Debug, PartialEq)]
pub struct CommandExpansion {
    command: CommandChain,
    kind: CommandExpansionKind,
}

gen_impl_has_location! {CommandExpansion, command}
gen_impl_dbg_txt! {CommandExpansion, command}

impl CommandExpansion {
    pub fn new(command: CommandChain, kind: CommandExpansionKind) -> Self {
        Self { command, kind }
    }

    pub fn command(&self) -> &CommandChain {
        &self.command
    }

    pub fn kind(&self) -> &CommandExpansionKind {
        &self.kind
    }
}

impl HasCommandInvocation for CommandExpansion {
    fn command_invocation(&self, on_found: &mut dyn FnMut(crate::Range)) {
        self.command.command_invocation(on_found);
    }
}

impl HasVariableReference for CommandExpansion {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.command.var_references(on_found);
    }
}
