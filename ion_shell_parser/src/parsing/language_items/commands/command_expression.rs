use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{
        language_items::{
            commands::Builtin,
            language_item::{CanHaveSourceBuiltin, FoundSourceBuiltin},
            HasCommandInvocation, HasVariableReference, HasWarnings, ValueContext,
        },
        HasLocation, RangeDebugText,
    },
    prelude::{command_operators::FileRedirect, variables::VariableReference},
    range::ToRange,
    Range,
};

use super::ExecutableOrBuiltin;

#[derive(Debug, PartialEq)]
pub struct FileRedirectContext {
    range: RangeDebugText,
    context: FileRedirect,
}

gen_impl_dbg_txt! {FileRedirectContext, range}
gen_impl_has_location! {FileRedirectContext, range}

impl FileRedirectContext {
    pub fn new(range: Range, context: FileRedirect) -> Self {
        let range = range.into();
        Self { range, context }
    }
}

#[derive(Debug, PartialEq)]
pub struct CommandExpression {
    command: ValueContext,
    values: ExecutableOrBuiltin,
    redirections: Vec<FileInteractionWithCommand>,
}

impl HasWarnings for CommandExpression {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.command.all_warnings(on_found);
        self.values.all_warnings(on_found);
    }
}

impl HasCommandInvocation for CommandExpression {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.command.command_invocation(on_found);
        self.values.command_invocation(on_found);
    }
}

impl HasVariableReference for CommandExpression {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.command.var_references(on_found);
        self.values.var_references(on_found);
    }
}

impl CanHaveSourceBuiltin for CommandExpression {
    fn source_calls(&self, mut on_found: impl FnMut(FoundSourceBuiltin)) {
        if let ExecutableOrBuiltin::Builtin(Builtin::Source(to_be_sourced)) = &self.values {
            let start = self.command.location();
            on_found(FoundSourceBuiltin::new(start, to_be_sourced));
        }
    }
}

impl CommandExpression {
    pub fn new(
        command: ValueContext,
        values: ExecutableOrBuiltin,
        redirections: Vec<FileInteractionWithCommand>,
    ) -> Self {
        Self {
            command,
            values,
            redirections,
        }
    }
}

gen_impl_dbg_txt! { CommandExpression # command, values @ redirections }

impl HasLocation for CommandExpression {
    fn location(&self) -> Range {
        let start = self.command.location();
        let end = self
            .redirections
            .last()
            .map(|e| e.location())
            .unwrap_or_else(|| self.values.try_location().unwrap_or(start));
        ToRange::new((&start, &end)).into()
    }
}

#[derive(Debug, PartialEq)]
pub struct FileInteractionWithCommand {
    operator: FileRedirectContext,
    range_file_name: RangeDebugText,
}

impl FileInteractionWithCommand {
    pub fn new(operator: FileRedirectContext, range_file_name: Range) -> Self {
        let range_file_name = range_file_name.into();
        Self {
            operator,
            range_file_name,
        }
    }
}

gen_impl_dbg_txt! { FileInteractionWithCommand, operator, range_file_name }
gen_impl_has_location! { FileInteractionWithCommand, operator <=> range_file_name }

#[derive(Debug, PartialEq)]
pub struct RawCommandExpression {
    command: ValueContext,
    values: Vec<ValueContext>,
    redirections: Vec<FileInteractionWithCommand>,
}

gen_impl_dbg_txt! { RawCommandExpression # command @ values, redirections }

impl RawCommandExpression {
    pub fn new(
        command: ValueContext,
        values: Vec<ValueContext>,
        redirections: Vec<FileInteractionWithCommand>,
    ) -> Self {
        Self {
            command,
            values,
            redirections,
        }
    }
}

impl HasLocation for RawCommandExpression {
    fn location(&self) -> Range {
        let start = self.command.location();
        let end = self
            .redirections
            .last()
            .map(|e| e.location())
            .unwrap_or_else(|| {
                self.values
                    .last()
                    .map(|e| e.location())
                    .unwrap_or_else(|| start)
            });
        ToRange::new((&start, &end)).into()
    }
}

impl From<RawCommandExpression>
    for (
        ValueContext,
        Vec<ValueContext>,
        Vec<FileInteractionWithCommand>,
    )
{
    fn from(value: RawCommandExpression) -> Self {
        (value.command, value.values, value.redirections)
    }
}
