use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::RangeDebugText,
    prelude::{
        command_operators::PipeOperator, variables::VariableReference, CanHaveSourceBuiltin,
        FoundSourceBuiltin, HasCommandInvocation, HasVariableReference, HasWarnings,
    },
    Range,
};

use super::CommandExpression;

#[derive(Debug, PartialEq)]
pub struct PipeOfCommands {
    first: CommandExpression,
    rest: Vec<NextPipedCommand>,
}

gen_impl_dbg_txt! {PipeOfCommands # first @ rest}
gen_impl_has_location! {PipeOfCommands, first}

impl PipeOfCommands {
    pub fn new(first: CommandExpression, rest: Vec<NextPipedCommand>) -> Self {
        Self { first, rest }
    }

    pub fn rest(&self) -> &[NextPipedCommand] {
        &self.rest
    }

    pub fn first(&self) -> &CommandExpression {
        &self.first
    }
}

impl HasWarnings for PipeOfCommands {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.first().all_warnings(on_found);
        self.rest().all_warnings(on_found);
    }
}

impl HasCommandInvocation for PipeOfCommands {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.first.command_invocation(on_found);
        self.rest.as_slice().command_invocation(on_found);
    }
}

impl HasVariableReference for PipeOfCommands {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.first.var_references(on_found);
        self.rest().var_references(on_found);
    }
}

impl CanHaveSourceBuiltin for PipeOfCommands {
    fn source_calls(&self, mut on_found: impl FnMut(FoundSourceBuiltin)) {
        self.first.source_calls(&mut on_found);
        self.rest
            .iter()
            .for_each(|next| next.source_calls(&mut on_found))
    }
}

#[derive(Debug, PartialEq)]
pub struct NextPipedCommand {
    pipe_operator: PipeOperatorContext,
    command: CommandExpression,
}

gen_impl_dbg_txt! {NextPipedCommand,  pipe_operator, command}
gen_impl_has_location! {NextPipedCommand, command <=> pipe_operator}

impl NextPipedCommand {
    pub fn new(pipe_operator: PipeOperatorContext, command: CommandExpression) -> Self {
        Self {
            pipe_operator,
            command,
        }
    }
}

impl HasWarnings for NextPipedCommand {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.command.all_warnings(on_found);
    }
}

impl HasCommandInvocation for NextPipedCommand {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.command.command_invocation(on_found);
    }
}

impl HasVariableReference for NextPipedCommand {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.command.var_references(on_found);
    }
}

impl CanHaveSourceBuiltin for NextPipedCommand {
    fn source_calls(&self, on_found: impl FnMut(FoundSourceBuiltin)) {
        self.command.source_calls(on_found);
    }
}

#[derive(Debug, PartialEq)]
pub struct PipeOperatorContext {
    range: RangeDebugText,
    pipe_operator: PipeOperator,
}

gen_impl_dbg_txt! {PipeOperatorContext, range}
gen_impl_has_location! {PipeOperatorContext, range}

impl PipeOperatorContext {
    pub fn new(range: Range, pipe_operator: PipeOperator) -> Self {
        let range = range.into();
        Self {
            range,
            pipe_operator,
        }
    }

    pub fn pipe_operator(&self) -> PipeOperator {
        self.pipe_operator
    }
}
