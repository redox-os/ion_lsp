use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{
        self,
        language_items::{
            builtins::SourceBuiltin, HasVariableReference, HasWarnings, ValueContext,
        },
        HasLocation,
    },
    prelude::{variables::VariableReference, HasCommandInvocation},
    Range,
};

#[derive(Debug, PartialEq)]
pub enum ExecutableOrBuiltin {
    Executable(Vec<ValueContext>),
    Builtin(Builtin),
}

impl HasCommandInvocation for ExecutableOrBuiltin {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        if let Self::Executable(values) = self {
            values.as_slice().command_invocation(on_found);
        }
    }
}

impl HasWarnings for ExecutableOrBuiltin {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&parsing::ParsingError)) {
        match self {
            ExecutableOrBuiltin::Executable(ctx) => ctx.as_slice().all_warnings(on_found),
            ExecutableOrBuiltin::Builtin(ctx) => ctx.all_warnings(on_found),
        }
    }
}

impl HasVariableReference for ExecutableOrBuiltin {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        match self {
            Self::Builtin(built_in) => built_in.var_references(on_found),
            Self::Executable(args) => args.as_slice().var_references(on_found),
        }
    }
}

gen_impl_dbg_txt! {ExecutableOrBuiltin, Executable => value, Builtin => value}

impl ExecutableOrBuiltin {
    pub fn try_location(&self) -> Option<Range> {
        match self {
            ExecutableOrBuiltin::Executable(values) => parsing::whole_range_from_seq(values),
            ExecutableOrBuiltin::Builtin(builtin) => builtin.try_location(),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Builtin {
    Source(SourceBuiltin),
}

impl HasWarnings for Builtin {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&parsing::ParsingError)) {
        match self {
            Self::Source(source) => source.all_warnings(on_found),
        }
    }
}

impl HasVariableReference for Builtin {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        match self {
            Self::Source(to_source) => to_source.var_references(on_found),
        }
    }
}

impl Builtin {
    pub fn try_location(&self) -> Option<Range> {
        match self {
            Builtin::Source(value) => Some(value.location()),
        }
    }
}

gen_impl_dbg_txt! {Builtin, Source => value}
gen_impl_has_location! {Builtin, Source => value}
