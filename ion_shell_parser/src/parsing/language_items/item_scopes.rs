pub use else_clause::ElseContext;
pub use enter_scope::EnterScope;
pub use exit_scope::ExitScope;
pub use for_loop::ForLoopContext;
pub use switch_scope::SwitchScope;

mod else_clause;
mod enter_scope;
mod exit_scope;
mod for_loop;
mod switch_scope;

use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{HasLocation, RangeDebugText},
    Range,
};

use super::{
    commands::CommandChain, variables::VariableReference, HasCommandInvocation,
    HasVariableReference, HasWarnings,
};

#[derive(Debug, PartialEq)]
pub struct ScopeCondition {
    keyword_range: RangeDebugText,
    command: CommandChain,
}

gen_impl_has_location! {ScopeCondition, keyword_range <=> command}
gen_impl_dbg_txt! {ScopeCondition, keyword_range, command}

impl ScopeCondition {
    pub fn new(keyword_range: Range, command: CommandChain) -> Self {
        let keyword_range = keyword_range.into();
        Self {
            keyword_range,
            command,
        }
    }

    pub fn command(&self) -> &CommandChain {
        &self.command
    }

    pub fn keyword_range(&self) -> Range {
        self.keyword_range.location()
    }
}

impl HasWarnings for ScopeCondition {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.command.all_warnings(on_found);
    }
}

impl HasCommandInvocation for ScopeCondition {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.command.command_invocation(on_found);
    }
}

impl HasVariableReference for ScopeCondition {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.command.var_references(on_found);
    }
}
