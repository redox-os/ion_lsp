pub use brace_expansion::{BraceExpansionComponent, BraceExpansionContext, BraceSliceExpansion};
pub use math_expression::MathExpression;
pub use quoted_str::{ComplexQuotedStr, QuotedStr, QuotedStrComponent};
pub use unquoted_str::UnQuoatedStr;

mod brace_expansion;
mod math_expression;
mod quoted_str;
mod unquoted_str;

use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{tracing_utils, HasLocation, ParsingError, RangeDebugText},
    NumberValue, Range,
};

use super::{
    variables::VariableReference, CommandExpansion, HasCommandInvocation, HasVariableReference,
    HasWarnings, Method,
};

use derive_more::Into;

#[derive(Debug, PartialEq, Into)]
pub struct ValueContext {
    range: RangeDebugText,
    value: Value,
}

gen_impl_has_location! {ValueContext, range}
gen_impl_dbg_txt! {ValueContext, range, value}

impl ValueContext {
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(name = "new_value")
    )]
    pub fn new(range: Range, value: Value) -> Self {
        tracing_utils::emit_message!("Ion shell value was detected");
        let range = range.into();
        Self { value, range }
    }

    pub fn value(&self) -> &Value {
        &self.value
    }
}

impl HasWarnings for ValueContext {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&ParsingError)) {
        self.value.all_warnings(on_found);
    }
}

impl HasCommandInvocation for ValueContext {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        match self.value() {
            Value::UnQuotedStr(UnQuoatedStr::Plain) => on_found(self.range.location()),
            Value::ProcessExpansion(ctx) => ctx.command_invocation(on_found),
            Value::ArrayExpression(ctx) => ctx.as_slice().command_invocation(on_found),
            _ => (),
        };
    }
}

impl HasVariableReference for ValueContext {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.value.var_references(on_found);
    }
}

#[derive(Debug, PartialEq)]
pub enum Value {
    UnQuotedStr(UnQuoatedStr),
    BraceExpansion(BraceExpansionContext),
    Bool,
    Method(Method),
    Number(NumberValue),
    QuotedStr(QuotedStr),
    ProcessExpansion(Box<CommandExpansion>),
    MathExpression(MathExpression),
    VariableReference(VariableReference),
    ArrayExpression(Vec<ValueContext>),
}

impl HasWarnings for Value {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&ParsingError)) {
        if let Self::QuotedStr(ctx) = self {
            ctx.all_warnings(on_found)
        }
    }
}

impl HasVariableReference for Value {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        match self {
            Self::VariableReference(reference) => on_found(reference),
            Self::ProcessExpansion(command) => command.var_references(on_found),
            Value::BraceExpansion(ctx) => ctx.var_references(on_found),
            Self::UnQuotedStr(UnQuoatedStr::Complex(seq)) => {
                seq.as_slice().var_references(on_found)
            }
            Self::QuotedStr(QuotedStr::Complex(seq)) => {
                seq.var_references(on_found);
            }
            Self::ArrayExpression(expression) => expression
                .iter()
                .map(|e| e.value())
                .for_each(|value| value.var_references(on_found)),
            _ => (),
        }
    }
}

#[cfg(all(debug_assertions, test))]
impl crate::parsing::HasOptAllocatedDebugText for Value {
    fn go_through_dbg_txt(
        &mut self,
        on_found: &dyn Fn(crate::Range) -> crate::ImmutableClonableText,
    ) {
        match self {
            Self::UnQuotedStr(UnQuoatedStr::Plain) | Self::Bool | Self::Number(_) => (),
            Self::UnQuotedStr(UnQuoatedStr::Complex(seq)) => seq.go_through_dbg_txt(on_found),
            Self::MathExpression(expression) => expression.go_through_dbg_txt(on_found),
            Self::QuotedStr(QuotedStr::Simple(range)) => {
                let _ = range.as_mut().map(|e| e.go_through_dbg_txt(&on_found));
            }
            Self::BraceExpansion(ctx) => ctx.go_through_dbg_txt(on_found),
            Self::QuotedStr(QuotedStr::Complex(context)) => context.go_through_dbg_txt(&on_found),
            Self::ProcessExpansion(expansion) => expansion.go_through_dbg_txt(on_found),
            Self::Method(method) => method.go_through_dbg_txt(on_found),
            Self::VariableReference(var) => var.go_through_dbg_txt(on_found),
            Self::ArrayExpression(seq) => seq.go_through_dbg_txt(on_found),
        }
    }
}
