pub use type_annotation_context::TypeAnnotationContext;
pub use variable_declaration::VariableDeclaration;
pub use variable_reference::{NamespaceContext, VariableReference, VariableReferenceContext};

mod type_annotation_context;
mod variable_declaration;
mod variable_reference;

use crate::{
    macros::gen_impl_dbg_txt,
    parsing::{tracing_utils, HasLocation, RangeDebugText},
    range::Range,
};

use super::{FoundVariableDeclaration, HasVariableDeclaration};

#[derive(Debug, PartialEq, Clone)]
pub struct VariableName(RangeDebugText);

impl HasVariableDeclaration for VariableName {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        on_found(FoundVariableDeclaration::NotAnnotated(self))
    }
}

gen_impl_dbg_txt! {VariableName}

impl HasLocation for VariableName {
    fn location(&self) -> Range {
        self.0.location()
    }
}

impl VariableName {
    #[cfg_attr(
        feature = "ion_shell_parser_trace_parse_phase",
        tracing::instrument(name = "new_variable_name")
    )]
    pub(crate) fn new(location: Range) -> Self {
        tracing_utils::emit_message!("Variable name detected");
        let location = location.into();
        Self(location)
    }
}
