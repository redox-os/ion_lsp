use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{HasLocation, RangeDebugText},
    Range,
};

use super::variables::VariableName;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum SliceKind {
    Inclusive,
    Exclusive,
}

#[derive(Debug, PartialEq, Clone)]
pub struct IndexAccessContext {
    range: RangeDebugText,
    access: IndexAccess,
}

gen_impl_dbg_txt! {IndexAccessContext, range,  access}
gen_impl_has_location! {IndexAccessContext, range}

impl IndexAccessContext {
    pub(crate) fn new(range: Range, access: IndexAccess) -> Self {
        let range = range.into();
        Self { range, access }
    }

    pub fn access(&self) -> &IndexAccess {
        &self.access
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum IndexKind {
    Numeric(usize),
    TextVarRef(VariableName),
    Text,
}

gen_impl_dbg_txt! {IndexKind, TextVarRef => ctx}

#[derive(Debug, PartialEq, Clone)]
pub enum IndexAccess {
    Index(SingleIndex),
    Slice(SliceIndex),
}

gen_impl_dbg_txt! {IndexAccess, Index => ctx, Slice => ctx}

impl IndexAccess {
    pub fn maybe_location(&self) -> Option<Range> {
        match self {
            IndexAccess::Index(ctx) => Some(ctx.location()),
            IndexAccess::Slice(ctx) => Some(ctx.location()),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SingleIndex {
    range: RangeDebugText,
    index: IndexKind,
}

gen_impl_dbg_txt! {SingleIndex, range, index}
gen_impl_has_location! {SingleIndex, range}

impl SingleIndex {
    pub fn new(range: Range, index: IndexKind) -> Self {
        let range = range.into();
        Self { range, index }
    }

    pub fn index(&self) -> &IndexKind {
        &self.index
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SliceIndex {
    kind: SliceKind,
    start: SingleIndex,
    end: SingleIndex,
}

gen_impl_dbg_txt! {SliceIndex, start,  end}
gen_impl_has_location! {SliceIndex, start <=> end}

impl SliceIndex {
    pub fn new(start: SingleIndex, end: SingleIndex, kind: SliceKind) -> Self {
        Self { start, end, kind }
    }

    pub fn start(&self) -> &SingleIndex {
        &self.start
    }

    pub fn end(&self) -> &SingleIndex {
        &self.end
    }

    pub fn kind(&self) -> SliceKind {
        self.kind
    }
}
