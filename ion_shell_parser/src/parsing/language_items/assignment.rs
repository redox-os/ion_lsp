use super::{
    variables::{VariableName, VariableReference},
    FoundVariableDeclaration, HasCommandInvocation, HasVariableDeclaration, HasVariableReference,
    HasWarnings, ValueContext,
};

use crate::{
    macros::gen_impl_dbg_txt,
    parsing::{
        language_items::variables::VariableDeclaration, whole_range_from_seq, HasLocation,
        RangeDebugText,
    },
    tokens::AssignmentOpKind,
    Range, ToRange,
};

#[derive(Debug, PartialEq)]
pub struct ExportContext {
    export_range: RangeDebugText,
    variable_name: VariableName,
    opt_assignment: Option<ValueContext>,
}

impl HasWarnings for ExportContext {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.opt_assignment
            .as_ref()
            .inspect(|e| e.all_warnings(on_found));
    }
}

impl HasCommandInvocation for ExportContext {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.opt_assignment.as_ref().command_invocation(on_found);
    }
}

impl HasVariableDeclaration for ExportContext {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        self.variable_name.var_declaration(on_found);
    }
}

impl HasVariableReference for ExportContext {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.opt_assignment.var_references(on_found);
    }
}

impl ExportContext {
    pub fn new(
        export_range: Range,
        variable_name: VariableName,
        opt_assignment: Option<ValueContext>,
    ) -> Self {
        let export_range = export_range.into();
        Self {
            export_range,
            variable_name,
            opt_assignment,
        }
    }
}

gen_impl_dbg_txt! {ExportContext ? opt_assignment ! export_range, variable_name}

impl HasLocation for ExportContext {
    fn location(&self) -> Range {
        let start = self.export_range.location();
        let end = self
            .opt_assignment
            .as_ref()
            .map(|e| e.location())
            .unwrap_or(self.variable_name.location());
        Range::from_start_end_internal(start, end)
    }
}

#[derive(Debug, PartialEq)]
pub struct AssignmentContext {
    let_range: RangeDebugText,
    declarations: Vec<VariableDeclaration>,
    operator: AssignmentOpKind,
    expressions: Vec<ValueContext>,
}

impl HasWarnings for AssignmentContext {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.expressions().all_warnings(on_found);
    }
}

impl HasCommandInvocation for AssignmentContext {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.expressions().command_invocation(on_found);
    }
}

impl HasVariableDeclaration for AssignmentContext {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        self.declarations().var_declaration(on_found);
    }
}

impl HasVariableReference for AssignmentContext {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.expressions().var_references(on_found);
    }
}

gen_impl_dbg_txt! {AssignmentContext # let_range @ declarations, expressions}

impl HasLocation for AssignmentContext {
    fn location(&self) -> Range {
        let start = self.let_range.location();
        let end = whole_range_from_seq(self.expressions())
            .expect("Every variable must have at least one value");
        ToRange::new((start, end)).into()
    }
}
impl AssignmentContext {
    pub fn new(
        let_range: Range,
        declarations: Vec<VariableDeclaration>,
        operator: AssignmentOpKind,
        expressions: Vec<ValueContext>,
    ) -> Self {
        let let_range = let_range.into();
        Self {
            let_range,
            declarations,
            operator,
            expressions,
        }
    }

    pub fn new_single(
        let_range: Range,
        declarations: VariableDeclaration,
        operator: AssignmentOpKind,
        expressions: ValueContext,
    ) -> Self {
        let let_range = let_range.into();
        let (declarations, expressions) = (vec![declarations], vec![expressions]);
        Self {
            let_range,
            declarations,
            operator,
            expressions,
        }
    }

    pub fn expressions(&self) -> &[ValueContext] {
        self.expressions.as_slice()
    }

    pub fn operator(&self) -> AssignmentOpKind {
        self.operator
    }

    pub fn declarations(&self) -> &[VariableDeclaration] {
        &self.declarations
    }
}
