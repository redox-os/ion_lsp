use crate::macros::{gen_impl_dbg_txt, gen_impl_has_location};

use super::{variables::VariableReference, HasVariableReference, HasWarnings, ValueContext};

#[derive(Debug, PartialEq)]
pub struct SourceBuiltin {
    to_source: ValueContext,
    args: Vec<ValueContext>,
}

gen_impl_dbg_txt! {SourceBuiltin # to_source @ args }
gen_impl_has_location! {SourceBuiltin, to_source @ args }

impl SourceBuiltin {
    pub fn new(to_source: ValueContext, args: Vec<ValueContext>) -> Self {
        Self { to_source, args }
    }

    pub fn args(&self) -> &[ValueContext] {
        &self.args
    }

    pub fn to_source(&self) -> &ValueContext {
        &self.to_source
    }
}

impl HasWarnings for SourceBuiltin {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.to_source().all_warnings(on_found);
        self.args().all_warnings(on_found);
    }
}

impl HasVariableReference for SourceBuiltin {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.to_source.value().var_references(on_found);
        self.args().var_references(on_found);
    }
}
