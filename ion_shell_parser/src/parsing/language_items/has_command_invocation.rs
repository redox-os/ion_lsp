use crate::Range;

pub trait HasCommandInvocation {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range));
}

impl<T> HasCommandInvocation for &[T]
where
    T: HasCommandInvocation,
{
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.iter()
            .for_each(|element| element.command_invocation(on_found));
    }
}
impl<T> HasCommandInvocation for Option<&T>
where
    T: HasCommandInvocation,
{
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.inspect(|found| found.command_invocation(on_found));
    }
}
