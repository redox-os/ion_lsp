use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{HasLocation, RangeDebugText},
    range::Range,
};
use derive_more::{AsRef, Deref, From, Into};

#[derive(Debug, PartialEq, From, Into, AsRef, Deref, Clone)]
pub struct DocString(Comment);

impl DocString {
    pub fn new(value: Comment) -> Self {
        Self(value)
    }

    pub fn value(&self) -> &Comment {
        &self.0
    }
}

gen_impl_dbg_txt! {DocString}
gen_impl_has_location! {DocString}

#[derive(Debug, PartialEq, Clone)]
pub struct Comment {
    outer: RangeDebugText,
    inner: RangeDebugText,
}

gen_impl_dbg_txt! {Comment, outer, inner}
gen_impl_has_location! {Comment, outer}

impl Comment {
    pub fn new(outer: Range, inner: Range) -> Self {
        let (outer, inner) = (outer.into(), inner.into());
        Self { outer, inner }
    }

    pub fn outer(&self) -> Range {
        self.outer.location()
    }

    pub fn inner(&self) -> Range {
        self.inner.location()
    }
}
