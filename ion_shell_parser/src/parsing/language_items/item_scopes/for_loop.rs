use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::RangeDebugText,
    prelude::{
        variables::{VariableName, VariableReference},
        FoundVariableDeclaration, HasCommandInvocation, HasVariableDeclaration,
        HasVariableReference, HasWarnings, ValueContext,
    },
    Range,
};

#[derive(Debug, PartialEq)]
pub struct ForLoopContext {
    range: RangeDebugText,
    variable_names: Vec<VariableName>,
    source: Vec<ValueContext>,
}

gen_impl_has_location! {ForLoopContext, range}
gen_impl_dbg_txt! {ForLoopContext, variable_names, source}

impl ForLoopContext {
    pub fn new(range: Range, variable_names: Vec<VariableName>, source: Vec<ValueContext>) -> Self {
        let range = range.into();
        Self {
            range,
            variable_names,
            source,
        }
    }

    pub fn variable_name(&self) -> &[VariableName] {
        &self.variable_names
    }

    pub fn source(&self) -> &[ValueContext] {
        &self.source
    }
}

impl HasWarnings for ForLoopContext {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        self.source.as_slice().all_warnings(on_found);
    }
}

impl HasCommandInvocation for ForLoopContext {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        self.source.as_slice().command_invocation(on_found);
    }
}

impl HasVariableDeclaration for ForLoopContext {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        self.variable_names.as_slice().var_declaration(on_found);
    }
}

impl HasVariableReference for ForLoopContext {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        self.source.as_slice().var_references(on_found);
    }
}
