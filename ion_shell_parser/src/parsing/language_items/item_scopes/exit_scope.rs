use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::{HasLocation, RangeDebugText},
    Range,
};

#[derive(Debug, PartialEq)]
pub struct ExitScope(RangeDebugText);

gen_impl_has_location! {ExitScope}
gen_impl_dbg_txt! {ExitScope}

impl ExitScope {
    pub fn new(range: Range) -> Self {
        let range = range.into();
        Self(range)
    }
    pub fn value(&self) -> Range {
        self.0.location()
    }
}
