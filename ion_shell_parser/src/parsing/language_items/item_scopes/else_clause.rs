use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    parsing::RangeDebugText,
    Range,
};

#[derive(Debug, PartialEq)]
pub struct ElseContext(RangeDebugText);

impl ElseContext {
    pub fn new(value: Range) -> Self {
        let range = value.into();
        Self(range)
    }
}

gen_impl_has_location! {ElseContext}
gen_impl_dbg_txt! {ElseContext}
