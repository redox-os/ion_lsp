use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    prelude::{
        variables::VariableReference, Case, HasCommandInvocation, HasVariableReference, HasWarnings,
    },
    Range,
};

use super::{ElseContext, ScopeCondition};

#[derive(Debug, PartialEq)]
pub enum SwitchScope {
    Case(Case),
    ElseIf(ScopeCondition),
    Else(ElseContext),
}

gen_impl_has_location! {SwitchScope, ElseIf => ctx, Else => ctx, Case => ctx}
gen_impl_dbg_txt! {SwitchScope, ElseIf => ctx, Else => ctx, Case => ctx}

impl HasWarnings for SwitchScope {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        match self {
            SwitchScope::Else(_) => (),
            SwitchScope::Case(ctx) => ctx.all_warnings(on_found),
            SwitchScope::ElseIf(ctx) => ctx.all_warnings(on_found),
        }
    }
}

impl HasCommandInvocation for SwitchScope {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        match self {
            SwitchScope::Case(ctx) => ctx.command_invocation(on_found),
            SwitchScope::ElseIf(ctx) => ctx.command_invocation(on_found),
            _ => (),
        }
    }
}

impl HasVariableReference for SwitchScope {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        match self {
            SwitchScope::Case(case_ctx) => case_ctx.var_references(on_found),
            SwitchScope::ElseIf(else_if_ctx) => else_if_ctx.var_references(on_found),
            SwitchScope::Else(_) => (),
        }
    }
}
