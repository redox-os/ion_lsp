use crate::{
    macros::{gen_impl_dbg_txt, gen_impl_has_location},
    prelude::{
        variables::VariableReference, Case, FoundVariableDeclaration, FunctionDeclaration,
        HasCommandInvocation, HasVariableDeclaration, HasVariableReference, HasWarnings, Match,
    },
    Range,
};

use super::{ForLoopContext, ScopeCondition};

#[derive(Debug, PartialEq)]
pub enum EnterScope {
    If(ScopeCondition),
    While(ScopeCondition),
    For(ForLoopContext),
    Function(FunctionDeclaration),
    Match(Match),
    Case(Case),
}

gen_impl_dbg_txt! {EnterScope, If => ctx, While => ctx, For => ctx, Match => ctx, Case => ctx, Function => ctx}
gen_impl_has_location! {EnterScope, If => ctx, While => ctx, For => ctx, Match => ctx, Case => ctx, Function => ctx}

impl HasWarnings for EnterScope {
    fn all_warnings(&self, on_found: &mut dyn FnMut(&crate::parsing::ParsingError)) {
        match self {
            EnterScope::Function(_) => (),
            EnterScope::If(cond) | EnterScope::While(cond) => cond.all_warnings(on_found),
            EnterScope::For(for_ctx) => for_ctx.all_warnings(on_found),
            EnterScope::Match(match_ctx) => match_ctx.all_warnings(on_found),
            EnterScope::Case(case_ctx) => case_ctx.all_warnings(on_found),
        }
    }
}

impl HasCommandInvocation for EnterScope {
    fn command_invocation(&self, on_found: &mut dyn FnMut(Range)) {
        match self {
            EnterScope::If(ctx) => ctx.command_invocation(on_found),
            EnterScope::While(ctx) => ctx.command_invocation(on_found),
            EnterScope::For(ctx) => ctx.command_invocation(on_found),
            EnterScope::Match(ctx) => ctx.command_invocation(on_found),
            EnterScope::Case(ctx) => ctx.command_invocation(on_found),
            EnterScope::Function(_) => (),
        }
    }
}

impl HasVariableDeclaration for EnterScope {
    fn var_declaration(&self, on_found: &mut dyn FnMut(FoundVariableDeclaration)) {
        match self {
            EnterScope::For(ctx) => ctx.var_declaration(on_found),
            EnterScope::Function(ctx) => ctx.var_declaration(on_found),
            EnterScope::If(_)
            | EnterScope::While(_)
            | EnterScope::Match(_)
            | EnterScope::Case(_) => (),
        }
    }
}

impl HasVariableReference for EnterScope {
    fn var_references(&self, on_found: &mut dyn FnMut(&VariableReference)) {
        match self {
            EnterScope::If(cond) | EnterScope::While(cond) => cond.var_references(on_found),
            EnterScope::For(for_ctx) => for_ctx.var_references(on_found),
            EnterScope::Match(match_ctx) => match_ctx.var_references(on_found),
            EnterScope::Case(case_ctx) => case_ctx.var_references(on_found),
            EnterScope::Function(_) => (),
        }
    }
}
