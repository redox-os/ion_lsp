use crate::Range;

#[derive(Debug, Default)]
pub struct EnclosedRange {
    start: Option<Range>,
    end: Option<Range>,
}

impl EnclosedRange {
    pub fn inner(&self) -> Option<Range> {
        match (self.start, self.end) {
            (None, None) => None,
            (Some(start), Some(end)) => {
                let range = Range::from_start_end_internal(start, end);
                Some(range)
            }
            (None, Some(_)) | (Some(_), None) => unreachable!(),
        }
    }

    pub fn update(&mut self, new_end: Range) {
        match (self.start, self.end) {
            (None, None) => {
                self.start = Some(new_end);
                self.end = Some(new_end);
            }
            (Some(_), Some(_)) => {
                self.end = Some(new_end);
            }
            (None, Some(_)) | (Some(_), None) => unreachable!(),
        }

        debug_assert!(self.start <= self.end);
    }
}
