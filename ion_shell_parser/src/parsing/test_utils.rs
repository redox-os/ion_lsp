use crate::{
    parsing::{language_items::LanguageItemContext, HasLocation as _, IonParser, ParsingResult},
    IonTokenizer, LineColumnMap,
};
#[allow(unused_macros)]
macro_rules! tracing_init {
    () => {
        #[cfg(all(test, feature = "ion_shell_parser_trace_parse_phase"))]
        tracing_subscriber::fmt().pretty().init();
        #[cfg(not(all(test, feature = "ion_shell_parser_trace_parse_phase")))]
        compile_error!(
            "Within the crate `ion_shell_parser`, initializing of a tracing subcriber should only be done in an unit test"
        )
    };
}

pub fn act_parsing_without_error_mapping(input: &str) -> (Vec<LanguageItemContext>, LineColumnMap) {
    let tokens = IonTokenizer::new(input);
    let lines = LineColumnMap::new(input);
    let actual: Result<Vec<LanguageItemContext>, _> = IonParser::new(tokens, input).collect();

    let actual = actual.unwrap_or_else(|error| {
        let error_location = error.location();
        let text = crate::get_string_from_input(input, error_location);

        let range = lines.range_line_column(input, error_location).unwrap();
        panic!(
            "error: {:#?}\nLocation: {:#?}\nText: {}",
            error, range, text
        )
    });
    (actual, lines)
}

pub fn act_parsing_without_error(input: &str) -> Vec<LanguageItemContext> {
    act_parsing_without_error_mapping(input).0
}

pub fn act_parsing_error_tolerance(input: &str) -> Vec<ParsingResult> {
    let tokens = IonTokenizer::new(input);
    IonParser::new(tokens, input).collect()
}

#[allow(unused_imports)]
pub(crate) use tracing_init;
