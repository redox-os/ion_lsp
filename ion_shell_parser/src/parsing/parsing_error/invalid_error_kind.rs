use std::borrow::Cow;

use thiserror::Error;

use crate::{DiagnosticMessage, Range};

use super::ReportableParsingError;

#[derive(Debug, PartialEq, Clone, Copy, Error)]
#[error("{}", self.for_display())]
pub enum InvalidError {
    Command,
    VariableName,
    Assignment,
    StartOfStatement,
    RepresentableUnsignedSize,
    Value,
    FileRedirectArg,
    CommandAfterPipe(InvalidCommandForOperator),
    CommandAfterLogicalChain(InvalidCommandForOperator),
    CommandAfterIf,
    CommandAfterWhile,
    TypeAnnotation,
    CommandAfterElseIf,
    Index,
    End,
}

impl ReportableParsingError for InvalidError {
    fn to_txt<'a>(
        &self,
        on_insert_base: impl Fn() -> &'a str + 'a,
        on_insert: impl Fn(Range) -> Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match *self {
            InvalidError::RepresentableUnsignedSize => format!(
                "`{}` can not be parsed into representable unsigned integer",
                on_insert_base()
            ),
            InvalidError::TypeAnnotation => {
                format!("`{}` is not a valid type annotation", on_insert_base())
            }
            InvalidError::Command => format!("`{}` is not valid command", on_insert_base()),
            InvalidError::VariableName => format!(
                "Value `{}` is not valid as a variable name",
                on_insert_base()
            ),
            InvalidError::Assignment => format!(
                "`{}` is not valid as an assignment operator",
                on_insert_base()
            ),
            InvalidError::StartOfStatement => {
                format!("`{}` is not a valid start of a statement", on_insert_base())
            }
            InvalidError::Value => format!("`{}` is not valid value", on_insert_base()),
            InvalidError::FileRedirectArg => format!(
                "`{}` is not valid as a file redirection argument",
                on_insert_base()
            ),
            InvalidError::CommandAfterPipe(invalid) => format!(
                "`{}` is not a valid command after the pipe operator `{}`",
                on_insert(invalid.invalid_command()),
                on_insert(invalid.operator())
            ),
            InvalidError::CommandAfterLogicalChain(invalid) => format!(
                "`{}` is not a valid command after the logical chaining operator `{}`",
                on_insert(invalid.invalid_command()),
                on_insert(invalid.operator())
            ),
            InvalidError::CommandAfterIf => format!(
                "`{}` is not a valid command after an `If` keyword",
                on_insert_base()
            ),
            InvalidError::CommandAfterWhile => format!(
                "`{}` is not a valid command after a `while` keyword",
                on_insert_base()
            ),
            InvalidError::CommandAfterElseIf => format!(
                "`{}` is not a valid command after a `else if` keyword",
                on_insert_base()
            ),
            InvalidError::Index => {
                format!("`{}` is not valid as an index", on_insert_base())
            }
            InvalidError::End => format!(
                "No value are allowed after and end keyword. Remove `{}`",
                on_insert_base()
            ),
        }
        .into()
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct InvalidCommandForOperator {
    invalid_command: Range,
    operator: Range,
}

impl InvalidCommandForOperator {
    pub fn new(invalid_command: Range, operator: Range) -> Self {
        Self {
            invalid_command,
            operator,
        }
    }

    pub fn invalid_command(&self) -> Range {
        self.invalid_command
    }

    pub fn operator(&self) -> Range {
        self.operator
    }
}
