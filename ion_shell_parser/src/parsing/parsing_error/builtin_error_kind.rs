use crate::{DiagnosticMessage, Range};
use thiserror::Error;

use super::ReportableParsingError;

#[derive(Debug, Clone, Copy, PartialEq, Error)]
#[error("`{builtin_range}`: {kind}")]
pub struct BuiltinError {
    builtin_range: Range,
    kind: BuilinErrorKind,
}

impl BuiltinError {
    pub fn new(builtin_range: Range, kind: BuilinErrorKind) -> Self {
        Self {
            builtin_range,
            kind,
        }
    }

    pub fn builtin_range(&self) -> Range {
        self.builtin_range
    }

    pub fn kind(&self) -> BuilinErrorKind {
        self.kind
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Error)]
#[error("{}", self.for_display())]
pub enum BuilinErrorKind {
    TooManyArgs {
        superfluous_arg: Range,
        count: usize,
    },
    InvalidArgCount(usize),
    TooFewArgs(usize),
}

impl ReportableParsingError for BuilinErrorKind {
    fn to_txt<'a>(
        &self,
        _on_base_insert: impl Fn() -> &'a str + 'a,
        on_insert: impl Fn(Range) -> std::borrow::Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match *self {
            BuilinErrorKind::TooManyArgs {
                superfluous_arg,
                count,
            } => format!(
                "Only up to `{}` arguments are allowed. \n\
                    The value `{}` as an argument must be removed",
                count,
                on_insert(superfluous_arg)
            ),
            BuilinErrorKind::InvalidArgCount(count) => {
                format!("{}x arguments must be given.", count,)
            }
            BuilinErrorKind::TooFewArgs(args) => {
                format!("Builtin takes at least {0} arguments", args)
            }
        }
        .into()
    }
}
