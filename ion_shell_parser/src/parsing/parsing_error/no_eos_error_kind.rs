use crate::{DiagnosticMessage, Range};
use thiserror::Error;

use super::ReportableParsingError;
#[derive(Debug, PartialEq, Clone, Copy, Error)]
#[error("{}", self.for_display())]
pub enum NoEosAfter {
    Else,
    For,
    Match,
    Case,
    End,
    Command,
}

impl ReportableParsingError for NoEosAfter {
    fn to_txt<'a>(
        &self,
        on_base_insert: impl Fn() -> &'a str + 'a,
        _on_insert: impl Fn(Range) -> std::borrow::Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        let text = match self {
            NoEosAfter::Command => format!(
                "Command is not terminated correctly. Remove `{}`",
                on_base_insert()
            ),
            NoEosAfter::Else => format!(
                "Else is not terminated correctly. Remove `{}`",
                on_base_insert()
            ),
            NoEosAfter::For => format!(
                "For is not terminated correctly. Remove `{}`",
                on_base_insert()
            ),
            NoEosAfter::Match => format!(
                "Match is not terminated correctly. Remove `{}`",
                on_base_insert()
            ),
            NoEosAfter::Case => format!(
                "Case is not terminated correctly. Remove `{}`",
                on_base_insert()
            ),
            NoEosAfter::End => format!(
                "End is not terminated correctly. Remove `{}`",
                on_base_insert()
            ),
        };
        text.into()
    }
}
