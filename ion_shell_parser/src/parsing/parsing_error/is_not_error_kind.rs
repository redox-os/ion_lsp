use crate::DiagnosticMessage;

use thiserror::Error;

use super::ReportableParsingError;
#[derive(Debug, PartialEq, Clone, Copy, Error)]
#[error("{}", self.for_display())]
pub enum IsNotError {
    ClosingSquareBracket,
    InKeyword,
    EqualAssignment,
    Eos,
}

impl ReportableParsingError for IsNotError {
    fn to_txt<'a>(
        &self,
        on_base_insert: impl Fn() -> &'a str + 'a,
        _on_insert: impl Fn(crate::Range) -> std::borrow::Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match *self {
            IsNotError::ClosingSquareBracket => {
                format!("{} is not a closing square bracket `]`", on_base_insert()).into()
            }
            IsNotError::InKeyword => format!("{} is not the keyword `in`", on_base_insert()).into(),
            IsNotError::EqualAssignment => {
                format!("{} is not equal assignment operator `=`", on_base_insert()).into()
            }
            IsNotError::Eos => format!(
                "Statement is not terminated correctly with the value `{}`",
                on_base_insert()
            )
            .into(),
        }
    }
}
