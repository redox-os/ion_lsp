use std::borrow::Cow;

use thiserror::Error;

use crate::{parsing::language_items::ScopeLabel, DiagnosticMessage, Range};

use super::{ParsingErrorKind, ReportableParsingError};

#[derive(Debug, PartialEq, Eq, Error, Clone, Copy)]
#[error("{}", self.for_display())]
pub enum MissingError {
    Command(#[from] MissingCommandFor),
    Value(#[from] MissingValueFor),
    EndForScope(ScopeLabel),
    DoublePoint,
    FunctionName,
    VariableName,
    EndMathDelimiter,
    MatchForCase,
    Index,
    Assignment,
    ArgFileDirect,
    ScopeForEnd,
    IfForElse,
    IfForElseIf,
    ClosedParanteseCurlyBracket,
    ClosedCurlyBracket,
    ClosedBracketSquare,
    NextIndexOrEqual,
    InKeyword,
}

impl ReportableParsingError for MissingError {
    fn to_txt<'a>(
        &self,
        on_base_insert: impl Fn() -> &'a str + 'a,
        on_insert: impl Fn(Range) -> std::borrow::Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match self {
            Self::DoublePoint => "No double point provided `:`".into(),
            Self::ClosedCurlyBracket => "There is no closed curly bracket `}` provided".into(),
            Self::ClosedParanteseCurlyBracket => {
                "There is no closed parantese bracket `)` provided".into()
            }
            Self::ArgFileDirect => "There is no argument for a file redirection".into(),
            Self::ScopeForEnd => "There is no beginning for an end keyword".into(),
            Self::IfForElse => "There is no matching if for an else keyword".into(),
            Self::IfForElseIf => "There is no matching if for an else if keyword".into(),
            Self::EndForScope(scope) => {
                format!("There is not end keyword for a scope of type `{}`", scope).into()
            }
            Self::ClosedBracketSquare => {
                "Closing square bracket `]` is missing to opening square bracket `[`".into()
            }
            Self::NextIndexOrEqual => "No second index or equal sign provided".into(),
            Self::InKeyword => "Keyword `in` is missing".into(),
            Self::Assignment => "No assignment operator provided".into(),
            Self::Index => "No index provided".into(),
            Self::FunctionName => "Function name is missing".into(),
            Self::VariableName => "Name for variable declaration is missing".into(),
            Self::EndMathDelimiter => "Match expression must be closed with `))`".into(),
            Self::MatchForCase => {
                "A case branch needs to be contained within a match clause".into()
            }
            Self::Command(command) => command.to_txt(on_base_insert, on_insert),
            Self::Value(value) => value.to_txt(on_base_insert, on_insert),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Error, Clone, Copy)]
#[error("{}", self.for_display())]
pub enum MissingCommandFor {
    If,
    ElseIf,
    IfGuard,
    While,
    ProcessExpansion,
    Pipe(Range),
    Logical(Range),
}

impl From<MissingCommandFor> for ParsingErrorKind {
    fn from(value: MissingCommandFor) -> Self {
        let missing: MissingError = value.into();
        ParsingErrorKind::Missing(missing)
    }
}

impl ReportableParsingError for MissingCommandFor {
    fn to_txt<'a>(
        &self,
        _on_base_insert: impl Fn() -> &'a str + 'a,
        on_insert: impl Fn(Range) -> std::borrow::Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match *self {
            MissingCommandFor::ProcessExpansion => {
                "No command within process expansion provided".into()
            }
            MissingCommandFor::If => "No command after an If clause".into(),
            MissingCommandFor::ElseIf => "No command after an else If clause".into(),
            MissingCommandFor::IfGuard => "No command after an If guard clause".into(),
            MissingCommandFor::While => "No command after a while clause".into(),
            MissingCommandFor::Pipe(range) => format!(
                "No command provided after the piper operator {}",
                on_insert(range)
            )
            .into(),
            MissingCommandFor::Logical(range) => format!(
                "No command provided after the logical operator {}",
                on_insert(range)
            )
            .into(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Error, Clone, Copy)]
#[error("{}", self.for_display())]
pub enum MissingValueFor {
    Match,
    Case,
    For(Range),
    Variable(Range),
    Index,
    SecondIndex,
}

impl From<MissingValueFor> for ParsingErrorKind {
    fn from(value: MissingValueFor) -> Self {
        let value: MissingError = value.into();
        ParsingErrorKind::Missing(value)
    }
}

impl ReportableParsingError for MissingValueFor {
    fn to_txt<'a>(
        &self,
        _on_base_insert: impl Fn() -> &'a str + 'a,
        on_insert: impl Fn(Range) -> Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match *self {
            MissingValueFor::Index => "No value for an index provided".into(),
            MissingValueFor::SecondIndex => "No value for a second index".into(),
            MissingValueFor::Match => "No value for match clause provided".into(),
            MissingValueFor::Case => "No value for case clause provided".into(),
            MissingValueFor::For(range) => format!(
                "No value provided within a for loop for the variable declaration: `{}`",
                on_insert(range)
            )
            .into(),
            MissingValueFor::Variable(range) => {
                format!("No value provided for the variable: `{}`", on_insert(range)).into()
            }
        }
    }
}
