use std::borrow::Cow;

use crate::{DiagnosticMessage, Range};
use thiserror::Error;

use super::{
    invalid_error_kind::InvalidError, is_not_error_kind::IsNotError, BuiltinError, MissingError,
    NoEosAfter, ReportableParsingError,
};

#[derive(Debug, PartialEq, Clone, Copy, Error)]
#[error("{}", self.for_display())]
pub enum ParsingErrorKind {
    Missing(#[from] MissingError),
    Invalid(#[from] InvalidError),
    Builtin(#[from] BuiltinError),
    IsNot(#[from] IsNotError),
    NoEos(#[from] NoEosAfter),
    DuplicateArguments,
    NoScopeForContinue,
    NoScopeForBreak,
    NonCaseAfterMatch,
    WhiteSpaceNotAllowed,
    TabNotAllowed,
    NewLineNotAllowed,
    DuplicateElse,
    DuplicateIf,
    TooManyValuesForAssignment,
    Unmatched(#[from] UnmathcedStrError),
}

#[derive(Debug, PartialEq, Clone, Copy, Error)]
#[error("{}", self.for_display())]
pub enum UnmathcedStrError {
    Double,
    Single,
}
impl ReportableParsingError for UnmathcedStrError {
    fn to_txt<'a>(
        &self,
        _on_base_insert: impl Fn() -> &'a str + 'a,
        _on_insert: impl Fn(Range) -> std::borrow::Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match self {
            UnmathcedStrError::Double => {
                "Str delimiter `\"` was not matched with a respective `\"`".into()
            }
            UnmathcedStrError::Single => {
                "Str delimiter `\'` was not matched with a respective `\'`".into()
            }
        }
    }
}
impl ReportableParsingError for ParsingErrorKind {
    fn to_txt<'a>(
        &self,
        on_base_insert: impl Fn() -> &'a str + 'a,
        on_insert: impl Fn(Range) -> Cow<'a, str> + 'a,
    ) -> DiagnosticMessage {
        match *self {
            ParsingErrorKind::Unmatched(unmatched) => unmatched.to_txt(on_base_insert, on_insert),
            ParsingErrorKind::Invalid(invalid) => invalid.to_txt(on_base_insert, on_insert),
            ParsingErrorKind::Missing(missing) => missing.to_txt(on_base_insert, on_insert),
            ParsingErrorKind::IsNot(is_not) => is_not.to_txt(on_base_insert, on_insert),
            ParsingErrorKind::NoEos(not_eos) => not_eos.to_txt(on_base_insert, on_insert),
            ParsingErrorKind::DuplicateArguments => format!(
                "No duplicate argument names allowed.\nDuplicate name: {}",
                on_base_insert()
            )
            .into(),
            ParsingErrorKind::NoScopeForContinue => {
                "Continue clause `continue` must be contained within a scope".into()
            }
            ParsingErrorKind::NoScopeForBreak => {
                "Break clause `break` must be contained within a scope".into()
            }
            ParsingErrorKind::NonCaseAfterMatch => {
                "A case clause must follow after a match clause".into()
            }
            ParsingErrorKind::TabNotAllowed => "Tab is not allowed here".into(),
            ParsingErrorKind::WhiteSpaceNotAllowed => "White space is not allowed here".into(),
            ParsingErrorKind::NewLineNotAllowed => "A new line is not allowed here".into(),
            ParsingErrorKind::DuplicateIf => "No second subsequent if clause allowed".into(),
            ParsingErrorKind::DuplicateElse => "No second subsequent else clause allowed".into(),
            ParsingErrorKind::TooManyValuesForAssignment => format!(
                "There is no variable name to assign the value ({}) to",
                on_base_insert()
            )
            .into(),
            ParsingErrorKind::Builtin(error) => format!(
                "Error in calling builtin: `{}`\n\
                {}",
                on_insert(error.builtin_range()),
                error.kind().to_txt(on_base_insert, on_insert)
            )
            .into(),
        }
    }
}
