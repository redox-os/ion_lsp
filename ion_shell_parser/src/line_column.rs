use crate::Range;

use derive_more::Display;
#[derive(Debug, Clone, Copy, Display)]
#[display(fmt = "({}:{})", line, column)]
pub struct LineColumn {
    line: usize,
    column: usize,
}

impl LineColumn {
    pub fn new(line: usize, column: usize) -> Self {
        Self { line, column }
    }

    pub fn line(&self) -> usize {
        self.line
    }

    pub fn column(&self) -> usize {
        self.column
    }
}

#[derive(Debug, Default)]
pub struct LineColumnMap {
    /// Offset (bytes) the the beginning of each line, zero-based
    line_offsets: Vec<usize>,
}

impl LineColumnMap {
    pub fn new(input: &str) -> Self {
        let mut line_offsets: Vec<usize> = vec![0];

        let mut offset = 0;

        for c in input.chars() {
            offset += c.len_utf8();
            if c == '\n' {
                line_offsets.push(offset);
            }
        }

        Self { line_offsets }
    }

    pub fn get_line_column(&self, input: &str, byte: usize) -> Option<LineColumn> {
        if input.len() < byte {
            return None;
        }

        let line = Self::partitioned_index(&self.line_offsets, byte);
        let first_offset = self.line_offsets.get(line)?;

        // Get line str from original input, then we can get column offset
        let line_str = &input[*first_offset..byte];
        let col = line_str.chars().count();

        Some(LineColumn::new(line, col))
    }

    pub fn get_byte(&self, input: &str, line_col: LineColumn) -> Option<usize> {
        let line = line_col.line();
        let first_offset = self.line_offsets.get(line)?;
        let line_str = &input[*first_offset..];

        let mut char_offset = 0;
        let mut left_chars = line_col.column;
        let mut previous_index = 0;
        for (index, _) in line_str.char_indices() {
            let current_offset = index - previous_index;
            char_offset += current_offset;
            previous_index = index;

            match left_chars.checked_sub(1) {
                Some(new) => left_chars = new,
                None => {
                    let total = first_offset + char_offset;
                    return Some(total);
                }
            }
        }

        const REACHED_EOI_OFFSET: usize = 1;
        let total = first_offset + char_offset + REACHED_EOI_OFFSET;
        Some(total)
    }

    pub fn get_byte_range(
        &self,
        input: &str,
        (start, end): (LineColumn, LineColumn),
    ) -> Option<Range> {
        let (start, end) = (self.get_byte(input, start)?, self.get_byte(input, end)?);
        Some(Range::new_internal(start, end))
    }

    pub fn range_line_column(&self, input: &str, range: Range) -> Option<(LineColumn, LineColumn)> {
        let (start, end) = (range.start_pos(), range.end_pos());
        let (start, end) = (
            self.get_line_column(input, start)?,
            self.get_line_column(input, end)?,
        );
        Some((start, end))
    }

    fn partitioned_index(seq: &[usize], value: usize) -> usize {
        seq.binary_search(&value).unwrap_or_else(|not_exact| {
            let max_index = seq.len().saturating_sub(1);
            let not_exact = not_exact.min(max_index);
            match seq[not_exact].cmp(&value) {
                std::cmp::Ordering::Equal => not_exact,
                std::cmp::Ordering::Less => (not_exact + 1).min(max_index),
                std::cmp::Ordering::Greater => not_exact.saturating_sub(1),
            }
        })
    }
}

#[cfg(test)]
mod testing {
    use crate::{IonTokenizer, TokenKind};

    use super::*;

    #[test]
    fn returns_line_column_numbers_from_byte() {
        const INPUT: &str = "let a = 2; export a = 2\nif true end";
        let tokens = IonTokenizer::new(INPUT);
        let map = LineColumnMap::new(INPUT);

        let actual: Vec<(TokenKind, LineColumn, LineColumn)> = tokens
            .map(|element| {
                let location = element.location();

                let start = map.get_line_column(INPUT, location.start_pos()).unwrap();
                let end = map.get_line_column(INPUT, location.end_pos()).unwrap();

                let (start_b, end_b) = (
                    map.get_byte(INPUT, start).unwrap(),
                    map.get_byte(INPUT, end).unwrap(),
                );

                assert!(start_b == location.start_pos());
                assert!(end_b == location.end_pos());

                (element.kind(), start, end)
            })
            .collect();
        insta::assert_debug_snapshot!(actual);
    }
}
