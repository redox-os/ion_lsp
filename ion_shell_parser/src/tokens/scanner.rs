use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "../rules_for_tokens.pest"]
/// Scanner whichs provides the token from text input.
/// It used the parsing crate `pest` under the hood.
pub struct IonScanner;
