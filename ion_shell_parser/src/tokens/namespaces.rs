#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Namespace {
    Color,
    Env,
    Global,
    Super,
}
