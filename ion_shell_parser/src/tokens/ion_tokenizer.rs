use pest::{
    iterators::{Pair, Pairs},
    Parser,
};

use crate::{
    tokens::scanner::{IonScanner, Rule},
    Range,
};

use super::IonToken;

#[derive(Debug)]
/// Used as input for the syntax analysis/parsing.
///
/// # Rules for tokenisation
///
/// See file called `rules_for_tokens.pest` under the root of this package
///
/// # Purpose
///
/// * Turns pest tokens yielded by the scanner into tokens of this crate.
/// This way all subsequent phases do not depend on the pest crate.
/// * Tokens have the range value to get their text content by slicing the input anyway.
/// * Ensures that every yielded tokens has its start and end position
/// into the given input.
pub struct IonTokenizer<'a>(Pairs<'a, Rule>);

impl<'a> Iterator for IonTokenizer<'a> {
    type Item = IonToken;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(pair_to_ion_token)
    }
}

impl<'a> IonTokenizer<'a> {
    pub fn new(input: &'a str) -> Self {
        let scanned = IonScanner::parse(Rule::program, input)
            .unwrap_or_else(|e| panic!("Unmatched token: {:#?}", e));
        Self(scanned)
    }
}

fn pair_to_ion_token(token: Pair<'_, Rule>) -> IonToken {
    let span = token.as_span();

    let (start, end) = (span.start(), span.end());
    let range = Range::new_internal(start, end);
    IonToken::new(token.into(), range)
}

#[cfg(test)]
mod testing {
    use super::*;
    use crate::{tokens::IonToken, ImmutableClonableText};

    use quickcheck_macros::quickcheck;

    #[quickcheck]
    #[ignore = "Fuzing ion tokenizer for panicking"]
    fn fuzzy_tokenizer_for_panic(xs: Vec<String>) -> bool {
        for name in xs {
            let input = ImmutableClonableText::from(name);
            let tokenizer = IonTokenizer::new(&input);
            let _: Vec<IonToken> = tokenizer.collect();
        }
        true
    }
}
