use pest::iterators::Pair;

use crate::{
    parsing::{
        language_items::command_operators::{
            DetachOperator, FileOutputRedirection, FileRedirect, LogicalChainOperator,
            OutputChannels, PipeOperator,
        },
        SigilKind,
    },
    NumberValue,
};

use super::{
    scanner::Rule, type_annotation::TypePrimitiveAnnotation, AssignmentOpKind, BinaryOp, Namespace,
    TypeAnnotation,
};

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TokenKind {
    Continue,
    Break,
    Namespace(Namespace),
    MatchExprBegin,
    MatchExprEnd,
    Number(NumberValue),
    ElseIf,
    Bool,
    Redirect(FileRedirect),
    Pipe(PipeOperator),
    LogicalChain(LogicalChainOperator),
    Detach(DetachOperator),
    Hash,
    StringSigil,
    ArraySigil,
    SingleQuotation,
    Quotation,
    NewLine,
    EscapedQuotation,
    EscapedSingleQuotation,
    WhiteSpace(usize),
    Word,
    Tab(usize),
    Point,
    Comma,
    DoublePoint,
    TriplePoint,
    Eoi,
    TypeIdent(TypeAnnotation),
    BinaryOp(BinaryOp),
    StartDocstring,
    AssignmentOp(AssignmentOpKind),
    OpenSquareBracket,
    ClosedSquareBracket,
    OpenCurlyBracket,
    ClosedCurlyBracket,
    OpenParanteseCurlyBracket,
    ClosedParanteseCurlyBracket,
    Return,
    Let,
    Or,
    Not,
    Drop,
    Fn,
    If,
    Else,
    In,
    For,
    While,
    Match,
    Case,
    End,
    Export,
    Colon,
    Compare,
    SemiColon,
    CommandArrayExpansionBegin,
    CommandStringExpansionBegin,
    Escaped,
}

impl From<Pair<'_, Rule>> for TokenKind {
    fn from(pair: Pair<'_, Rule>) -> Self {
        match pair.as_rule() {
            Rule::Continue => Self::Continue,
            Rule::Break => Self::Break,
            Rule::NamespaceGlobal => Self::Namespace(Namespace::Global),
            Rule::NamespaceSuper => Self::Namespace(Namespace::Super),
            Rule::NamespaceEnv => Self::Namespace(Namespace::Env),
            Rule::NamespaceColor => Self::Namespace(Namespace::Color),
            Rule::Escaped => Self::Escaped,
            Rule::CommandStrExpandBegin => TokenKind::CommandStringExpansionBegin,
            Rule::CommandArrayExpandBegin => TokenKind::CommandArrayExpansionBegin,
            Rule::MathExprBegin => TokenKind::MatchExprBegin,
            Rule::MathExprEnd => TokenKind::MatchExprEnd,
            Rule::ElseIf => TokenKind::ElseIf,
            Rule::Semicolon => TokenKind::SemiColon,
            Rule::int => TokenKind::Number(NumberValue::Int),
            Rule::Hash => TokenKind::Hash,
            Rule::white_space => TokenKind::WhiteSpace(pair.as_str().len()),
            Rule::new_line => TokenKind::NewLine,
            Rule::Float => TokenKind::Number(NumberValue::Float),
            Rule::DoublePoint => TokenKind::DoublePoint,
            Rule::TriplePoint => TokenKind::TriplePoint,
            Rule::tab => TokenKind::Tab(pair.as_str().len()),
            Rule::bool => TokenKind::Bool,
            Rule::Point => TokenKind::Point,
            Rule::Quotation => TokenKind::Quotation,
            Rule::SingleQuotation => TokenKind::SingleQuotation,
            Rule::EOI => TokenKind::Eoi,
            Rule::StringSigil => TokenKind::StringSigil,
            Rule::Add => TokenKind::BinaryOp(BinaryOp::Add),
            Rule::Subtract => TokenKind::BinaryOp(BinaryOp::Subtract),
            Rule::Divide => TokenKind::BinaryOp(BinaryOp::Divide),
            Rule::Multiply => TokenKind::BinaryOp(BinaryOp::Multiply),
            Rule::Power => TokenKind::BinaryOp(BinaryOp::Power),
            Rule::IntegerDivide => TokenKind::BinaryOp(BinaryOp::IntegerDivide),
            Rule::Equal => TokenKind::AssignmentOp(AssignmentOpKind::Equal),
            Rule::AddAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Add),
            Rule::MinusAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Minus),
            Rule::DivAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Div),
            Rule::MultAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Mult),
            Rule::PowerAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Power),
            Rule::InterDivAssignment => TokenKind::AssignmentOp(AssignmentOpKind::InterDiv),
            Rule::AppendAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Append),
            Rule::PrependAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Prepend),
            Rule::StripAssignment => TokenKind::AssignmentOp(AssignmentOpKind::Strip),
            Rule::Return => TokenKind::Return,
            Rule::Comma => TokenKind::Comma,
            Rule::StartDocstring => TokenKind::StartDocstring,
            Rule::OpenSquareBracket => TokenKind::OpenSquareBracket,
            Rule::ClosedSquareBracket => TokenKind::ClosedSquareBracket,
            Rule::OpenCurlyBracket => TokenKind::OpenCurlyBracket,
            Rule::ClosedCurlyBracket => TokenKind::ClosedCurlyBracket,
            Rule::OpenParanteseCurlyBracket => TokenKind::OpenParanteseCurlyBracket,
            Rule::ClosedParanteseCurlyBracket => TokenKind::ClosedParanteseCurlyBracket,
            Rule::ArraySigil => TokenKind::ArraySigil,
            Rule::Let => TokenKind::Let,
            Rule::Drop => TokenKind::Drop,
            Rule::Fn => TokenKind::Fn,
            Rule::If => TokenKind::If,
            Rule::Else => TokenKind::Else,
            Rule::In => TokenKind::In,
            Rule::For => TokenKind::For,
            Rule::While => TokenKind::While,
            Rule::Match => TokenKind::Match,
            Rule::Case => TokenKind::Case,
            Rule::End => TokenKind::End,
            Rule::Export => TokenKind::Export,
            Rule::AndOperator => TokenKind::LogicalChain(LogicalChainOperator::And),
            Rule::OrOperator => TokenKind::LogicalChain(LogicalChainOperator::Or),
            Rule::Pipe => TokenKind::Pipe(OutputChannels::StdOut.into()),
            Rule::PipeStdErr => TokenKind::Pipe(OutputChannels::Err.into()),
            Rule::PipeBoth => TokenKind::Pipe(OutputChannels::Both.into()),
            Rule::Ampersand => TokenKind::Detach(DetachOperator::ToBackGround),
            Rule::Disown => TokenKind::Detach(DetachOperator::Disown),
            Rule::ToStdOut => TokenKind::Redirect(FileRedirect::Output(
                FileOutputRedirection::new_no_append(OutputChannels::StdOut),
            )),
            Rule::ToStdErr => TokenKind::Redirect(FileRedirect::Output(
                FileOutputRedirection::new_no_append(OutputChannels::Err),
            )),
            Rule::ToStdBoth => TokenKind::Redirect(FileRedirect::Output(
                FileOutputRedirection::new_no_append(OutputChannels::Both),
            )),
            Rule::AppendToStdOut => TokenKind::Redirect(FileRedirect::Output(
                FileOutputRedirection::new_append(OutputChannels::StdOut),
            )),
            Rule::AppendToStdErr => TokenKind::Redirect(FileRedirect::Output(
                FileOutputRedirection::new_append(OutputChannels::Err),
            )),
            Rule::AppendToStdBoth => TokenKind::Redirect(FileRedirect::Output(
                FileOutputRedirection::new_append(OutputChannels::Both),
            )),
            Rule::FileInputOpreator => TokenKind::Redirect(FileRedirect::Input),
            Rule::EscapedQuotation => TokenKind::EscapedQuotation,
            Rule::EscapedSingleQuotation => TokenKind::EscapedSingleQuotation,
            Rule::Colon => TokenKind::Colon,
            Rule::Compare => TokenKind::Compare,
            Rule::Word | Rule::WordStartWithKeyWord => TokenKind::Word,
            Rule::TypeIdent => {
                let mut inner = pair.into_inner();
                let (_colon, rest) = (
                    inner
                        .next()
                        .expect("Pest: Colon must be encountered for this rule"),
                    inner
                        .next()
                        .expect("Pest: Rule must contain a type annotation"),
                );
                match rest.as_rule() {
                    Rule::TypeHashMap => map_type(rest, |primitive| {
                        TokenKind::TypeIdent(TypeAnnotation::Hmap(primitive))
                    }),
                    Rule::TypeBeeTreeMap => map_type(rest, |primitive| {
                        TokenKind::TypeIdent(TypeAnnotation::Bmap(primitive))
                    }),
                    Rule::TypeArray => {
                        let mut primitive = rest.into_inner();
                        let (_open_brack, primitive, _closed_bracket) = (
                            primitive
                                .next()
                                .expect("Pest: open square bracket is there according to the tokeniser rules"),
                            primitive
                                .next()
                                .expect("Pest: tokeniser rule contains a primitive type annotation"),
                            primitive
                                .next()
                                .expect("Pest: close square bracket is there according to the tokeniser rules"),
                        );
                        let primitive = expect_primitive_type(primitive.as_rule());
                        TokenKind::TypeIdent(TypeAnnotation::Array(primitive))
                    }
                    p => TokenKind::TypeIdent(TypeAnnotation::Primitive(expect_primitive_type(p))),
                }
            }

            rule => unreachable!("Rule {:?} could not be matched to a token kind: ", rule),
        }
    }
}

fn map_type(
    rest: Pair<'_, Rule>,
    on_primitive: impl Fn(TypePrimitiveAnnotation) -> TokenKind,
) -> TokenKind {
    let mut primitive = rest.into_inner();
    let mut primitive = primitive
        .next()
        .expect("Pest: is type according to this rule")
        .into_inner();
    let (_open_brack, primitive, _closed_bracket) = (
        primitive
            .next()
            .expect("Pest: open square bracket is there according to the tokeniser rules"),
        primitive
            .next()
            .expect("Pest: tokeniser rule contains a primitive type annotation"),
        primitive
            .next()
            .expect("Pest: close square bracket is there according to the tokeniser rules"),
    );
    let primitive = expect_primitive_type(primitive.as_rule());
    on_primitive(primitive)
}
fn expect_primitive_type(rule: Rule) -> TypePrimitiveAnnotation {
    match rule {
        Rule::TypeInt => TypePrimitiveAnnotation::Int,
        Rule::TypeBool => TypePrimitiveAnnotation::Bool,
        Rule::TypeFloat => TypePrimitiveAnnotation::Float,
        Rule::TypeStr => TypePrimitiveAnnotation::Str,
        _ => unreachable!(
            "Only tokens for annotation of primitive types should be encountered here\n\
            But found rule: {:?}",
            rule
        ),
    }
}

impl TokenKind {
    pub fn match_on_sigil(self) -> Option<SigilKind> {
        let value = match self {
            TokenKind::StringSigil => SigilKind::String,
            TokenKind::ArraySigil => SigilKind::Array,
            _ => return None,
        };
        Some(value)
    }
    pub fn is_sigil(self) -> bool {
        matches!(self, TokenKind::ArraySigil | TokenKind::StringSigil)
    }
    pub fn is_quotation(self) -> bool {
        matches!(self, TokenKind::Quotation | TokenKind::SingleQuotation)
    }
    pub fn is_valid_file_name(self) -> bool {
        self.is_word_like() || matches!(self, TokenKind::Number(_))
    }

    pub fn is_word_like(self) -> bool {
        matches!(
            self,
            TokenKind::AssignmentOp(_)
                | TokenKind::Word
                | TokenKind::Point
                | TokenKind::Compare
                | TokenKind::Colon
                | TokenKind::BinaryOp(_)
                | TokenKind::DoublePoint
                | TokenKind::StartDocstring
        )
    }

    pub fn is_new_line(self) -> bool {
        matches!(self, TokenKind::NewLine)
    }
    pub fn is_eoi(self) -> bool {
        self == TokenKind::Eoi
    }
    pub fn is_comment(self) -> bool {
        self == TokenKind::Hash
    }
    pub fn is_end_of_statement(&self) -> bool {
        matches!(
            self,
            TokenKind::NewLine | TokenKind::Eoi | TokenKind::SemiColon
        )
    }
    pub fn is_visible_end_of_statement(&self) -> bool {
        matches!(self, TokenKind::SemiColon | TokenKind::NewLine)
    }
    pub fn is_noise(self) -> bool {
        self.is_horizontal_noise() || matches!(self, TokenKind::NewLine)
    }

    pub fn is_eos_or_noise(self) -> bool {
        self.is_noise() || self.is_end_of_statement()
    }
    pub fn is_horizontal_noise(self) -> bool {
        matches!(self, TokenKind::WhiteSpace(_) | TokenKind::Tab(_))
    }
}
