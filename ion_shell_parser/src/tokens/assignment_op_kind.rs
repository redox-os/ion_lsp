#[derive(Debug, PartialEq, Clone, Copy)]
pub enum AssignmentOpKind {
    Equal,
    Add,
    Minus,
    Mult,
    InterDiv,
    Div,
    Power,
    Concat,
    Append,
    Strip,
    Prepend,
}
