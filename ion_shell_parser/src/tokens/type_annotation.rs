#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TypeAnnotation {
    Primitive(TypePrimitiveAnnotation),
    Array(TypePrimitiveAnnotation),
    Hmap(TypePrimitiveAnnotation),
    Bmap(TypePrimitiveAnnotation),
}

impl std::fmt::Display for TypeAnnotation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TypeAnnotation::Primitive(primitive) => write!(f, "{}", primitive),
            TypeAnnotation::Array(primitive) => write!(f, "[{}]", primitive),
            TypeAnnotation::Hmap(primitive) => write!(f, "hmap:[{}]", primitive),
            TypeAnnotation::Bmap(primitive) => write!(f, "bmap:[{}]", primitive),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TypePrimitiveAnnotation {
    Int,
    Float,
    Bool,
    Str,
}

impl std::fmt::Display for TypePrimitiveAnnotation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            TypePrimitiveAnnotation::Int => "int",
            TypePrimitiveAnnotation::Float => "float",
            TypePrimitiveAnnotation::Bool => "bool",
            TypePrimitiveAnnotation::Str => "str",
        };
        f.write_str(text)
    }
}
