use super::TokenKind;
use crate::{parsing::HasLocation, range::Range};

#[derive(Debug, PartialEq, Clone)]
pub struct IonToken {
    kind: TokenKind,
    location: Range,
}

impl HasLocation for IonToken {
    fn location(&self) -> Range {
        self.location
    }
}

impl From<IonToken> for TokenKind {
    fn from(value: IonToken) -> Self {
        value.kind
    }
}

impl IonToken {
    pub fn new(kind: TokenKind, location: Range) -> Self {
        Self { kind, location }
    }
    pub fn location(&self) -> Range {
        self.location
    }

    pub fn kind(&self) -> TokenKind {
        self.kind
    }
}
