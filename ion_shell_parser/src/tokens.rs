pub use assignment_op_kind::AssignmentOpKind;
pub use binary_op::BinaryOp;
pub use ion_tokenizer::IonTokenizer;
pub use namespaces::Namespace;
pub use token::IonToken;
pub use token_kind::TokenKind;
pub use type_annotation::TypeAnnotation;

mod assignment_op_kind;
mod binary_op;
mod ion_tokenizer;
mod namespaces;
mod scanner;
mod token;
mod token_kind;
mod type_annotation;

#[cfg(test)]
mod tests {

    use crate::tokens::TokenKind;

    use super::*;
    #[test]
    fn tokens_strip_assignment() {
        let input = "let array \\\\= [2 3] # remove variables 2 and 3";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }

    #[test]
    fn tokens_single_semicolon() {
        // Found panic by quick check
        let input = ";";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }

    #[test]
    fn tokens_with_escaped_quotation() {
        let input = "\"hello \\\" word\"";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }

    #[test]
    fn token_from_several_empty_lines_without_trailing_space_end() {
        let input = " \n \n";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn token_from_several_empty_lines() {
        let input = " \n ";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }

    #[test]
    fn token_from_empty() {
        let input = "";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn token_from_empty_lines() {
        let input = "  ";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn parse_int() {
        let input = "234";
        let result: Vec<_> = IonTokenizer::new(input).collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn parse_line_with_bool_and_int() {
        let input = "234 false -45 true   234";
        let result: Vec<TokenKind> = IonTokenizer::new(input).map(TokenKind::from).collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn token_arithmetic_vars() {
        let input = include_str!("input_files_for_tests/test_assignment_tokens.txt");

        let result: Vec<TokenKind> = IonTokenizer::new(input).map(TokenKind::from).collect();

        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn tokens_float_and_integer() {
        let input = "let number = 2.25 +\t  22";
        let result: Vec<TokenKind> = IonTokenizer::new(input).map(TokenKind::from).collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn parse_lines_of_let_statements() {
        let input = "let name = \"Max Schmidt\" 
echo $name";
        let result: Vec<TokenKind> = ion_tokenizer::IonTokenizer::new(input)
            .map(TokenKind::from)
            .collect();
        insta::assert_debug_snapshot!(result);
    }

    #[test]
    fn tokens_string_variables() {
        let input = include_str!("input_files_for_tests/string_variables_tokens.txt");

        let result: Vec<TokenKind> = ion_tokenizer::IonTokenizer::new(input)
            .map(TokenKind::from)
            .collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn tokens_function_with_docstring() {
        let input = include_str!("input_files_for_tests/function_with_docstring.txt");

        let result: Vec<TokenKind> = ion_tokenizer::IonTokenizer::new(input)
            .map(TokenKind::from)
            .collect();

        insta::assert_debug_snapshot!(result);
    }

    #[test]
    fn tokens_slicing_string() {
        let input = include_str!("input_files_for_tests/slicing_string.txt");

        let result: Vec<TokenKind> = ion_tokenizer::IonTokenizer::new(input)
            .map(TokenKind::from)
            .collect();
        insta::assert_debug_snapshot!(result);
    }
    #[test]
    fn tokens_escaped_sigils() {
        const INPUT: &str = "let a = '\\$aa\\@aaccc'";
        let result: Vec<TokenKind> = ion_tokenizer::IonTokenizer::new(INPUT)
            .map(TokenKind::from)
            .collect();
        insta::assert_debug_snapshot!(result);
    }
}
