#![deny(clippy::dbg_macro, clippy::todo)]

use std::{borrow::Cow, num::NonZeroUsize, rc::Rc};

pub use problem_severity::ProblemSeverity;
pub use range::Range;
pub(crate) use range::ToRange;
pub use range_input_cursor::TextAccessor;
pub use tokens::{IonToken, IonTokenizer, TokenKind};
pub use validation_error_context::ValidationErrorMeta;

pub use line_column::{LineColumn, LineColumnMap};
pub mod parsing;
pub mod prelude;
pub mod semantic_analysis;

mod line_column;
mod macros;
mod problem_severity;
mod range;
mod range_input_cursor;
mod tokens;
mod validation_error_context;

pub type DiagnosticMessage = Cow<'static, str>;
pub type LineNumber = NonZeroUsize;
pub type ColumnCount = NonZeroUsize;
pub type BytePos = usize;
pub type ImmutableClonableText = Rc<str>;
pub type OptImmutableClonableText = Option<ImmutableClonableText>;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum NumberValue {
    Int,
    Float,
}

pub fn get_string_from_input(input: &str, range: Range) -> &str {
    let (start, end) = (range.start_pos(), range.end_pos());
    &input[start..end]
}
