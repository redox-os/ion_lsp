use crate::range::Range;

#[derive(Debug)]
pub struct TextAccessor<'a> {
    range: Range,
    input: &'a str,
}

impl<'a> TextAccessor<'a> {
    pub fn new(range: Range, input: &'a str) -> Self {
        Self { range, input }
    }
    pub fn get_content(&self) -> &str {
        crate::get_string_from_input(self.input, self.range)
    }

    pub fn get_another_content(&self, range: Range) -> &str {
        crate::get_string_from_input(self.input, range)
    }
}
