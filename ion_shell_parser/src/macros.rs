macro_rules! gen_impl_dbg_txt {
    ($name:ident) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                self.0.go_through_dbg_txt(on_found)
            }
        }
    };
    ($name:ident, $($y:ident),+) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                $(
                    self.$y.go_through_dbg_txt(on_found);
                )*
            }
        }
    };
    ($name:ident ?) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                if let Some(e)  = self.0.as_mut() {
                    e.go_through_dbg_txt(on_found)
                }
            }
        }
    };
    ($name:ident ? $($y:ident),+ ! $($z:ident),+) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                $(
                    if let Some(a) = self.$y.as_mut() {
                        a.go_through_dbg_txt(on_found);
                    }
                )*
                $(
                    self.$z.go_through_dbg_txt(on_found);
                )*
            }
        }
    };
    ($name:ident @ $($y:ident),+) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                $(
                    for next in self.$y.iter_mut() {
                        next.go_through_dbg_txt(on_found);
                    }
                )*
            }
        }
    };
    ($name:ident # $($z:ident),+ @ $($y:ident),+ ) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                $(
                    self.$z.go_through_dbg_txt(on_found);
                )*

                $(
                    for next in self.$y.iter_mut() {
                        next.go_through_dbg_txt(on_found);
                    }
                )*
            }
        }
    };
    ($name:ident, $($a:ident => $b:ident),+ ) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                match self {
                    $(
                        $name::$a($b) => $b.go_through_dbg_txt(on_found),
                    )*
                    #[allow(unreachable_patterns)]
                    _ => (),
                }
            }
        }
    };
    ($name:ident $(<$type:ty>)?, Ok => $a:ident, Err => $b:ident) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for $name {
            fn go_through_dbg_txt(
                &mut self,
                on_found: &impl Fn($crate::Range) -> $crate::ImmutableClonableText,
            ) {
                match self {
                    Ok($a) => $a.go_through_dbg_txt(on_found),
                    Err($b) => $b.go_through_dbg_txt(on_found),
                }
            }
        }
    };
    ($name:ident <= $field:ident) => {
        #[cfg(all(debug_assertions, test))]
        impl $crate::parsing::HasOptAllocatedDebugText for RangeDebugText {
            fn go_through_dbg_txt(&mut self, on_found: &dyn Fn($crate::Range) -> $crate::ImmutableClonableText) {
                use $crate::parsing::HasLocation;
                let location = self.location();
                let text = on_found(location);
                self.$field = Some(text);
            }
        }
    }
}

macro_rules! gen_impl_has_location {
    ($name:ident) => {
        impl $crate::parsing::HasLocation for $name {
            fn location(&self) -> $crate::Range {
                self.0.location()
            }
        }
    };
    ($name:ident, $y:ident) => {
        impl $crate::parsing::HasLocation for $name {
            fn location(&self) -> $crate::Range {
                self.$y.location()
            }
        }
    };
    ($name:ident, $x:ident <=> $y:ident) => {
        impl $crate::parsing::HasLocation for $name {
            fn location(&self) -> $crate::Range {
                let start = self.$x.location();
                let end = self.$y.location();
                $crate::ToRange::new((start, end)).into()
            }
        }
    };
    ($name:ident, $x:ident ? $y:ident) => {
        impl $crate::parsing::HasLocation for $name {
            fn location(&self) -> $crate::Range {
                let start = self.$x.location();
                let end = self.$y.as_ref().map(|e| e.location()).unwrap_or(start);
                $crate::ToRange::new((start, end)).into()
            }
        }
    };
    ($name:ident, $x:ident @ $y:ident) => {
        impl $crate::parsing::HasLocation for $name {
            fn location(&self) -> $crate::Range {
                $crate::ToRange::new((&self.$x, self.$y.as_slice())).into()
            }
        }
    };
    ($name:ident, $($a:ident => $b:ident),+ ) => {
        impl $crate::parsing::HasLocation for $name {
            fn location(&self) -> $crate::Range {
                match self {
                    $(
                        $name::$a($b) => $b.location(),
                    )*
                }
            }
        }
    };
}

pub(crate) use gen_impl_dbg_txt;
pub(crate) use gen_impl_has_location;
