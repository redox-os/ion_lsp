use crate::{
    parsing::{HasLocation, ParsingError},
    DiagnosticMessage, ProblemSeverity,
};

use super::SemanticError;

#[derive(Debug, PartialEq, Clone)]
pub enum ValidationError {
    Syntax(ParsingError),
    Semantic(SemanticError),
}

impl ValidationError {
    pub fn severity(&self) -> ProblemSeverity {
        match self {
            ValidationError::Syntax(error) => error.meta_context().severity(),
            ValidationError::Semantic(_) => ProblemSeverity::Error,
        }
    }
}

impl ValidationError {
    pub fn message(&self, input: &str) -> DiagnosticMessage {
        match self {
            ValidationError::Syntax(error) => error.message(input),
            ValidationError::Semantic(error) => error.message(input),
        }
    }
}

impl HasLocation for ValidationError {
    fn location(&self) -> crate::range::Range {
        match self {
            ValidationError::Syntax(a) => a.location(),
            ValidationError::Semantic(a) => a.location(),
        }
    }
}

impl From<ParsingError> for ValidationError {
    fn from(value: ParsingError) -> Self {
        Self::Syntax(value)
    }
}
