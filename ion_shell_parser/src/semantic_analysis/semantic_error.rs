use crate::{parsing::HasLocation, DiagnosticMessage, TextAccessor, ValidationErrorMeta};

#[derive(Debug, PartialEq, Clone)]
pub struct SemanticError {
    context: ValidationErrorMeta,
    kind: SemanticKindError,
}

impl SemanticError {
    pub fn message(&self, input: &str) -> DiagnosticMessage {
        let cursor = TextAccessor::new(self.location(), input);
        self.kind.message(&cursor)
    }
}

impl HasLocation for SemanticError {
    fn location(&self) -> crate::range::Range {
        self.context.location()
    }
}

impl SemanticError {
    pub fn new(context: ValidationErrorMeta, kind: SemanticKindError) -> Self {
        Self { context, kind }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum SemanticKindError {
    VariableNotFound,
}

impl SemanticKindError {
    fn message(&self, cursor: &crate::TextAccessor) -> DiagnosticMessage {
        match self {
            SemanticKindError::VariableNotFound => format!(
                "No variable with name `{}` is declared",
                cursor.get_content()
            )
            .into(),
        }
    }
}
