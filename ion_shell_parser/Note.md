Builtin = { "bg" | "cd" | "contains" | "dir-depth" | "dirs" | "disown" | "echo" | "ends-with" 
            | "eval" | "exec" | "exits" | "exit" | "eq" | "fg" | "help" | "history" | "isatty" 
            | "jobs" | "matches" | "popd" | "pushd" | "random" | "read" | "set" | "source" 
            | "starts-with" | "status" | "suspend" | "test" | "wait" | "which" }
