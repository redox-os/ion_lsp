# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

## [0.2.0] - 2024-04-07 

### Added 

- Validates syntax of ion shell script. 
- Error tolerant. Returns more than more error.
- Errors and language items are locatable, having a start and end byte

[unreleased]: https://gitlab.redox-os.org/redox-os/ion_lsp/-/commits/main?ref_type=heads
[0.2.0]: https://gitlab.redox-os.org/redox-os/ion_lsp/-/tags/0.2.0v
