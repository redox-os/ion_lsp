# Ion LSP server

This project aims to provide a Language Server adhering to the [LSP protocol](https://microsoft.github.io/language-server-protocol/).
This server provides language smarts for the scripting language of [Ion Shell](https://gitlab.redox-os.org/redox-os/ion).
Language smarts are certain pieces of information about the source code. 
These can be used by a code editor or IDE to provide 
code navigation, refactoring and code comprehension to an editor.

Code navigation is for example jumping to the definition of a function.
Changing the name of a variable where it is referenced, is a form of refactoring 
and showing all spots where a variable is used, falls into the category of code comprehension.

## Purpose of workspace members

- [ion_parser][ion_parser]: Parser which is different from the currently used parser of Ion Shell. 
  This parser is written for the needs of a Language Server in mind.
- [ion_lsp][ion_lsp]: Language server which provides language smarts to a code editor or an IDE.
- [Visual Code extension][vs_code_ext]: Extension which provides syntax highlighting 
  for the scripting language of Ion shell.
  It is mainly used for testing the behavior of this [LSP server][ion_lsp] 
  towards an editor.

## Crates.io

- [Ion Shell LSP server][ion_shell_lsp_server_crates_io]
- [Ion Shell parser][ion_shell_parser_crates_io]

## Contributing

See the following [guide lines][contributing] 

## Installation

First you must install the language server for Ion shell.
You can install it by following the instructions within the following [Readme][ion_lsp_readme].
Your editor must adhere to LSP protocol as a client to make use of this language server. 
Additionally the editor must know where to start language server as an executable.
Every editor has its own way of specifying the path to a language server.
The previously mentioned [Visual Studio Code extension][vs_code_ext] is set up for the Ion language server.
You can install it by following the instructions within [this README][vs_code_ext_readme].
The language server is mainly tested against this extension.

## License 

This project, all workspace members and the visual code extension are licensed under [MIT License][LICENSE]


[ion_shell_lsp_server_crates_io]: https://crates.io/crates/ion_shell_lsp_server 
[ion_shell_parser_crates_io]: https://crates.io/crates/ion_shell_parser
[License]: LICENSE
[ion_lsp]: ./ion_shell_lsp_server 
[ion_lsp_readme]: ./ion_shell_lsp_server/README.md
[ion_parser]: ./ion_shell_parser 
[vs_code_ext]: ./vs_code_extension 
[vs_code_ext_readme]: ./vs_code_extension/README.md
[contributing]: CONTRIBUTING.md 
