#![deny(clippy::dbg_macro, clippy::todo)]
use std::{error::Error, process::ExitCode};

use clap::Parser;
use ion_shell_parser::{
    parsing::{HasLocation, IonParser},
    IonTokenizer, LineColumnMap,
};

use crate::cli::AppCli;

mod cli;
fn main() -> Result<ExitCode, Box<dyn Error>> {
    let args = AppCli::parse();
    let content = std::fs::read_to_string(args.file())?;
    let tokens = IonTokenizer::new(&content);
    let lines = LineColumnMap::new(&content);
    let mut has_errors = false;
    for next in IonParser::new(tokens, &content) {
        if let Err(error) = next {
            if !has_errors {
                println!(
                    "(<Start Line>:<Start Column>) -> (<End Line>:<End Column>) | <Error Message>\n"
                );
            }
            has_errors = true;
            let message = error.message(&content);
            let (start, end) = lines.range_line_column(&content, error.location()).unwrap();
            println!("{} -> {} | {}", start, end, message);
        }
    }

    let exit_code = if has_errors {
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    };
    Ok(exit_code)
}
