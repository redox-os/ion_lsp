use std::path::{Path, PathBuf};

use clap::Parser;

#[derive(Debug, Parser)]
pub struct AppCli {
    /// File to parse
    file: PathBuf,
}

impl AppCli {
    pub fn file(&self) -> &Path {
        &self.file
    }
}
